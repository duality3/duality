/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#define _GNU_SOURCE
#define _CRT_SECURE_NO_WARNINGS

#include "src/lib.c"

#include <stdio.h>
#include <string.h>

/*
static inline void print_core_expr(FILE *file, dy_core_expr_t expr);

static inline void print_core_expr_with_newline(FILE *file, dy_core_expr_t expr);
*/

static inline FILE *dy_get_stream(int argc, const char *argv[]);

typedef struct dy_stream_env {
    FILE *stream;
    int current_byte;
} dy_stream_env_t;

static inline
bool dy_utf8_get_byte_from_stream(void *env, uint8_t *b);

static inline
void dy_utf8_consume_byte_from_stream(void *env);

//static inline int start_lsp(FILE *in, FILE *out);

/*
static void print_error_fragment(struct dy_range range, const char *text, size_t text_size);

static void calculate_line_and_column(struct dy_range range, const char *text, size_t text_size, size_t *start_line, size_t *start_column, size_t *end_line, size_t *end_column);
*/

//static inline void dy_core_expr_from_syntax_to_utf8(const dy_core_expr_t *expr, dy_put_byte_t put_byte, void *env);


int main(int argc, const char *argv[])
{
    FILE *stream = dy_get_stream(argc, argv);

    dy_stream_env_t stream_env = {
        .stream = stream
    };

    dy_utf8_consume_byte_from_stream(&stream_env);

    dy_utf8_bytes_cb_t parsing_cb = {
        .byte = dy_utf8_get_byte_from_stream,
        .consume = dy_utf8_consume_byte_from_stream,
        .env = &stream_env
    };

    uint8_t **ast_names = dy_array_create(sizeof *ast_names, 8);

    dy_core_expr_t core;
    if (!dy_utf8_to_core_expr(parsing_cb, &ast_names, &core)) {
        fprintf(stderr, "Couldn't parse anything.\n");
        return -1;
    }

    uint8_t *core_as_utf8 = dy_array_create(sizeof *core_as_utf8, 64);
    dy_core_expr_to_utf8(core, &core_as_utf8);
    dy_array_add(&core_as_utf8, &(uint8_t){'\0'});

    printf("Before checking: %s\n", core_as_utf8);

    dy_binder_t *binders = dy_array_create(sizeof *binders, 64);
    dy_constraint_t *constraints = dy_array_create(sizeof *constraints, 64);

    size_t float_offset;
    dy_check_expr(&binders, &constraints, &core, &float_offset);

    dy_array_set_end(core_as_utf8, core_as_utf8);
    dy_core_expr_to_utf8(core, &core_as_utf8);
    dy_array_add(&core_as_utf8, &(uint8_t){'\0'});

    printf("After checking: %s\n", core_as_utf8);

    return 0;
}
/*
void print_core_expr(FILE *file, dy_core_expr_t expr)
{
    uint8_t *s = dy_array_create(sizeof *s, 64);
    dy_core_expr_to_string(expr, &s, 0);

    const uint8_t *end = dy_array_past_end(s);
    for (; s != end; ++s) {
        fprintf(file, "%c", *s);
    }
}

void print_core_expr_with_newline(FILE *file, dy_core_expr_t expr)
{
    print_core_expr(file, expr);
    fprintf(file, "\n");
}
*/
/*
void print_error_fragment(struct dy_range range, const char *text, size_t text_size)
{
    assert(range.start < text_size);
    assert(range.end <= text_size);

    size_t start_line, start_col, end_line, end_col;
    calculate_line_and_column(range, text, text_size, &start_line, &start_col, &end_line, &end_col);

    if (start_line == end_line) {
        // Single-line error.

        int chars_written = 0;
        fprintf(stderr, "Error at %zu:%zu-%zu: %n", start_line, start_col, end_col,     &chars_written);

        for (size_t i = range.start - (start_col - 1); text[i] != '\n'; ++i) {
            fprintf(stderr, "%c", text[i]);
        }

        fprintf(stderr, "\n");

        for (size_t i = 0; i < (size_t)chars_written + start_col - 1; ++i) {
            fprintf(stderr, " ");
        }
        for (size_t i = range.start; i < range.end; ++i) {
            fprintf(stderr, "~");
        }
        fprintf(stderr, "\n");
    } else {
        // Multi-line error.
        fprintf(stderr, "* Error at %zu:%zu *\n", start_line, start_col);

        for (size_t i = range.start; i < range.end; ++i) {
            fprintf(stderr, "%c", text[i]);
        }

        fprintf(stderr, "\n");
    }
}

void calculate_line_and_column(struct dy_range range, const char *text, size_t text_size, size_t *s_line, size_t *s_column, size_t *e_line, size_t *e_column)
{
    size_t l = 1;
    size_t c = 1;

    size_t i = 0;

    for (; i < range.start; ++i) {
        if (text[i] == '\n') {
            l++;
            c = 1;
            continue;
        }

        ++c;
    }

    *s_line = l;
    *s_column = c;

    for (; i < range.end; ++i) {
        if (text[i] == '\n') {
            l++;
            c = 1;
            continue;
        }

        ++c;
    }

    *e_line = l;
    *e_column = c;
}
*/

FILE *dy_get_stream(int argc, const char *argv[])
{
    if (argc > 1) {
        FILE *stream = fopen(argv[1], "r");
        if (stream == NULL) {
            fprintf(stderr, "[LOG] Opening %s failed.", argv[1]);
        }
        return stream;
    } else {
        return stdin;
    }
}

bool dy_utf8_get_byte_from_stream(void *env, uint8_t *b)
{
    dy_stream_env_t *stream_env = env;

    if (stream_env->current_byte < 0 || stream_env->current_byte > UCHAR_MAX) {
        return false;
    }

    *b = (uint8_t)stream_env->current_byte;
    return true;
}

void dy_utf8_consume_byte_from_stream(void *env)
{
    dy_stream_env_t *stream_env = env;

    stream_env->current_byte = getc(stream_env->stream);
}

/*
int start_lsp(FILE *in, FILE *out)
{
    const char content_length[] = "Content-Length: ";
    const char sep[] = "\r\n";

    for (;;) {
        char x[(sizeof content_length) - 1];
        if (fread(x, sizeof x, 1, in) == 0) {
            return -1;
        }

        if (memcmp(x, content_length, sizeof x) != 0) {
            return -1;
        }


    }
}*/
/*
void dy_core_expr_from_syntax_to_utf8(const dy_core_expr_t *expr, dy_put_byte_t put_byte, void *env)
{
    const dy_ast_t *syntax = dy_core_get_aux_data(expr);

    if (syntax != NULL && syntax->tag == DY_AST_PARENTHESIZED) {
        put_byte('(', env);

        for (size_t i = 0; i < syntax->parenthesized.num_spaces; ++i) {
            put_byte(' ', env);
        }

        dy_core_set_aux_data((dy_core_expr_t *)expr, syntax->parenthesized.syntax);
        dy_core_expr_from_syntax_to_utf8(expr, put_byte, env);
        dy_core_set_aux_data((dy_core_expr_t *)expr, syntax);

        for (size_t i = 0; i < syntax->parenthesized.num_spaces2; ++i) {
            put_byte(' ', env);
        }

        if (syntax->parenthesized.have_closing_paren) {
            put_byte(')', env);
        }
        return;
    }

    switch (expr->tag) {
    case DY_CORE_EXPR_TRAP: {
        if (syntax != NULL && syntax->tag != DY_AST_EMPTY) {
            put_byte('-', env);
            dy_ast_to_utf8(syntax, put_byte, env);
            put_byte('-', env);
            put_byte(' ', env);
        }
        put_byte('t', env);
        put_byte('r', env);
        put_byte('a', env);
        put_byte('p', env);
        return;
    }
    case DY_CORE_EXPR_INTRO:
        if (expr->intro.is_complex) {

        } else {
            switch (expr->intro.simple.tag) {
            case DY_CORE_MAPPING:
                if (expr->intro.simple.mapping.is_computationally_irrelevant) {
                    put_byte('[', env);
                    for (size_t i = 0; i < syntax->map.bracket_expr.num_spaces; ++i) {
                        put_byte(' ', env);
                    }
                }

                dy_core_expr_from_syntax_to_utf8(expr->intro.simple.mapping.expr, put_byte, env);

                if (expr->intro.simple.mapping.is_computationally_irrelevant) {
                    for (size_t i = 0; i < syntax->map.bracket_expr.num_spaces2; ++i) {
                        put_byte(' ', env);
                    }

                    put_byte(']', env);
                }

                for (size_t i = 0; i < syntax->map.num_spaces; ++i) {
                    put_byte(' ', env);
                }

                switch (syntax->map.type) {
                case DY_AST_MAP_TYPE_MINUS:
                    put_byte('-', env);
                    break;
                case DY_AST_MAP_TYPE_TILDE:
                    put_byte('~', env);
                    break;
                }

                if (!syntax->map.have_greater_than) {
                    return;
                }

                put_byte('>', env);

                for (size_t i = 0; i < syntax->map.num_spaces2; ++i) {
                    put_byte(' ', env);
                }

                dy_core_expr_from_syntax_to_utf8(expr->intro.simple.mapping.sequent, put_byte, env);

                return;
            case DY_CORE_SELECTION:
                return;
            case DY_CORE_UNFOLDING:
                return;
            }

            abort();
        }
    case DY_CORE_EXPR_ELIM:
    case DY_CORE_EXPR_REF:
    case DY_CORE_EXPR_SEQUENCE:
    case DY_CORE_EXPR_COMP:
    case DY_CORE_EXPR_INFERENCE_CTX:
    case DY_CORE_EXPR_INFERENCE_VAR:;
    }
}
*/
