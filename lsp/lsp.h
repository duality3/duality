#pragma once

#include "../json/ast.h"

#include "../core/core.h"

/**
 * This file implements LSP support.
 */

typedef struct dy_document dy_document_t;

typedef void (*dy_lsp_send_fn)(const dy_json_t *json, void *env);

typedef struct dy_lsp_ctx {
    dy_lsp_send_fn send;
    void *env;

    bool is_initialized;
    bool received_shutdown_request;
    int exit_code;
    dy_document_t *documents;
} dy_lsp_ctx_t;

/**
 * Represents an open document.
 */
typedef struct dy_document {
    const uint32_t *uri; /** The URI is used as the identifier for a document. */
    const uint32_t *text;

    dy_core_ctx_t core_ctx;

    dy_core_expr_t core;
    bool core_is_present;
} dy_document_t;

typedef struct dy_error {
    //struct dy_range text_loc;
    bool have_text_loc;
} dy_lsp_error_t;

static inline bool dy_lsp_handle_message(dy_lsp_ctx_t *ctx, const dy_json_t *message);

static inline dy_json_t dy_lsp_initialize(dy_lsp_ctx_t *ctx, const dy_json_t *id);

static inline void dy_lsp_initialized(dy_lsp_ctx_t *ctx);

static inline void dy_lsp_shutdown(dy_lsp_ctx_t *ctx);

static inline void dy_lsp_did_open(dy_lsp_ctx_t *ctx, const uint32_t *uri, const uint32_t *text);

static inline void dy_lsp_did_close(dy_lsp_ctx_t *ctx, const uint32_t *uri);

static inline void dy_lsp_did_change(dy_lsp_ctx_t *ctx, const uint32_t *uri, const dy_json_t *content_changes);

static inline dy_json_t dy_lsp_hover(dy_lsp_ctx_t *ctx, const dy_json_t *id, const uint32_t *uri, int64_t line_number, int64_t utf16_char_offset);

static inline void dy_lsp_exit(dy_lsp_ctx_t *ctx);

static inline int dy_lsp_exit_code(dy_lsp_ctx_t *ctx);

static inline dy_json_t result_response(const dy_json_t *id, const dy_json_t *result);
static inline dy_json_t error_response(const dy_json_t *id, const dy_json_t *error);
static inline dy_json_t invalid_request(const dy_json_t *id, const uint32_t *message, size_t message_len);
static inline dy_json_t server_capabilities(void);
static inline dy_json_t server_info(void);
static inline dy_json_t text_document_sync_options(void);
static inline dy_json_t initialize_data(const dy_json_t *id);
static inline dy_json_t error_response_data(int64_t code, const uint32_t *message, size_t message_len);
static inline dy_json_t null_success_response(const dy_json_t *id);
static inline dy_json_t hover_result(const uint32_t *contents);
static inline dy_json_t make_position(long line, long character);
static inline dy_json_t make_range(long start_line, long start_character, long end_line, long end_character);
static inline dy_json_t publish_diagnostics(dy_core_ctx_t *ctx, const uint32_t *uri, const uint32_t *text, dy_core_expr_t expr);
//static inline dy_json_t compute_lsp_range(const uint8_t *text, struct dy_range range);

static inline void process_document(struct dy_lsp_ctx *ctx, dy_document_t *doc);
static inline bool compute_utf8_offset(const uint32_t *text, int64_t line_offset, int64_t utf16_offset, size_t *byte_offset);
static inline dy_json_t produce_diagnostics(dy_core_ctx_t *ctx, const uint32_t *text, dy_core_expr_t expr);
//static inline dy_json_t compute_lsp_range(const uint8_t *text, struct dy_range range);
//static inline dy_json_t make_diagnostic(const uint8_t *text, struct dy_range range, long severity, const uint8_t *message);
static inline dy_json_t diagnostics_params(dy_core_ctx_t *ctx, const uint32_t *uri, const uint32_t *text, dy_core_expr_t expr);

static inline bool scan_for_errors(dy_core_expr_t expr, bool convert_errors_into_traps, dy_core_expr_t *new_expr, dy_lsp_error_t *errors);

//static inline bool dy_find_smallest_range(dy_core_ctx_t *ctx, dy_core_expr_t expr, size_t byte_offset, struct dy_range *range, uint32_t **message);
