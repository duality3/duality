#include "lsp.h"

#include "../syntax/syntax.h"

#define DY_STR_LIT(s) s, ((sizeof s) / (sizeof *s) - 1)

static inline bool dy_utf32_strings_are_equal(const uint32_t *array, const uint32_t *s, size_t len_s);
static inline bool dy_utf32_arrays_are_equal(const uint32_t *ar1, const uint32_t *ar2);

static inline size_t dy_num_utf8_bytes_for_utf32_codepoint(uint32_t code_point);

static inline void dy_utf32_to_utf8(uint32_t code_point, uint8_t bytes[static 4]);

bool dy_lsp_handle_message(dy_lsp_ctx_t *ctx, const dy_json_t *message)
{
    if (message->tag != DY_JSON_OBJECT) {
        return true;
    }

    const dy_json_object_t *obj = &message->object;

    const dy_json_t *id = dy_json_get_member(obj, DY_STR_LIT(U"id"));

    const dy_json_t *method = dy_json_get_member(obj, DY_STR_LIT(U"method"));
    if (method == NULL) {
        if (id != NULL) {
            dy_json_t answer = invalid_request(id, DY_STR_LIT(U"Missing 'method' field."));
            ctx->send(&answer, ctx->env);
        }

        return true;
    }

    const dy_json_t *params = dy_json_get_member(obj, DY_STR_LIT(U"params"));

    if (method->tag != DY_JSON_STRING || method->string.code_points == NULL) {
        if (id != NULL) {
            dy_json_t answer = invalid_request(id, DY_STR_LIT(U"'method' field is not a string."));
            ctx->send(&answer, ctx->env);
        }

        return true;
    }

    const uint32_t *method_string = method->string.code_points;

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"exit"))) {
        dy_lsp_exit(ctx);
        return false;
    }

    if (ctx->received_shutdown_request) {
        if (id != NULL) {
            dy_json_t answer = invalid_request(id, DY_STR_LIT(U"Non-exit request after shutdown."));
            ctx->send(&answer, ctx->env);
        }

        return true;
    }

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"initialize"))) {
        if (id == NULL) {
            return true;
        }

        if (params == NULL) {
            dy_json_t answer = invalid_request(id, DY_STR_LIT(U"Missing 'params' field."));
            ctx->send(&answer, ctx->env);
            return true;
        }

        dy_json_t answer = dy_lsp_initialize(ctx, id);

        ctx->send(&answer, ctx->env);

        return true;
    }

    if (!ctx->is_initialized) {
        if (id != NULL) {
            dy_json_t error = error_response_data(-32002, DY_STR_LIT(U"Not yet initialized."));

            dy_json_t answer = error_response(id, &error);

            ctx->send(&answer, ctx->env);
        }

        return true;
    }

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"shutdown"))) {
        if (id == NULL) {
            return true;
        }

        dy_lsp_shutdown(ctx);

        dy_json_t answer = null_success_response(id);
        ctx->send(&answer, ctx->env);

        return true;
    }

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"initialized"))) {
        dy_lsp_initialized(ctx);
        return true;
    }

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"textDocument/didOpen"))) {
        if (params == NULL || params->tag != DY_JSON_OBJECT || params->object.members == NULL) {
            return true;
        }

        const dy_json_t *text_document = dy_json_get_member(&params->object, DY_STR_LIT(U"textDocument"));
        if (text_document == NULL || text_document->tag != DY_JSON_OBJECT || text_document->object.members == NULL) {
            return true;
        }

        const dy_json_t *uri = dy_json_get_member(&text_document->object, DY_STR_LIT(U"uri"));
        if (uri == NULL || uri->tag != DY_JSON_STRING || uri->string.code_points == NULL) {
            return true;
        }

        const dy_json_t *text = dy_json_get_member(&text_document->object, DY_STR_LIT(U"text"));
        if (text == NULL || text->tag != DY_JSON_STRING || text->string.code_points == NULL) {
            return true;
        }

        dy_lsp_did_open(ctx, uri->string.code_points, text_document->string.code_points);

        return true;
    }

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"textDocument/didChange"))) {
        if (params == NULL || params->tag != DY_JSON_OBJECT || params->object.members == NULL) {
            return true;
        }

        const dy_json_t *text_document = dy_json_get_member(&params->object, DY_STR_LIT(U"textDocument"));
        if (text_document == NULL || text_document->tag != DY_JSON_OBJECT || text_document->object.members == NULL) {
            return true;
        }

        const dy_json_t *uri = dy_json_get_member(&text_document->object, DY_STR_LIT(U"uri"));
        if (uri == NULL || uri->tag != DY_JSON_STRING || uri->string.code_points == NULL) {
            return true;
        }

        const dy_json_t *content_changes = dy_json_get_member(&params->object, DY_STR_LIT(U"contentChanges"));
        if (content_changes == NULL || content_changes->tag != DY_JSON_ARRAY || content_changes->array.values == NULL) {
            return true;
        }

        dy_lsp_did_change(ctx, uri->string.code_points, content_changes->array.values);

        return true;
    }

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"textDocument/didClose"))) {
        if (params == NULL || params->tag != DY_JSON_OBJECT || params->object.members == NULL) {
            return true;
        }

        const dy_json_t *text_document = dy_json_get_member(&params->object, DY_STR_LIT(U"textDocument"));
        if (text_document == NULL || text_document->tag != DY_JSON_OBJECT || text_document->object.members == NULL) {
            return true;
        }

        const dy_json_t *uri = dy_json_get_member(&text_document->object, DY_STR_LIT(U"uri"));
        if (uri == NULL || uri->tag != DY_JSON_STRING || uri->string.code_points == NULL)  {
            return true;
        }

        dy_lsp_did_close(ctx, uri->string.code_points);

        return true;
    }

    if (dy_utf32_strings_are_equal(method_string, DY_STR_LIT(U"textDocument/hover"))) {
        if (id == NULL) {
            return true;
        }

        if (params == NULL || params->tag != DY_JSON_OBJECT || params->object.members == NULL) {
            return true;
        }

        const dy_json_t *text_document = dy_json_get_member(&params->object, DY_STR_LIT(U"textDocument"));
        if (text_document == NULL || text_document->tag != DY_JSON_OBJECT || text_document->object.members == NULL) {
            return true;
        }

        const dy_json_t *uri = dy_json_get_member(&text_document->object, DY_STR_LIT(U"uri"));
        if (uri == NULL || uri->tag != DY_JSON_STRING || uri->string.code_points == NULL) {
            return true;
        }

        const dy_json_t *position = dy_json_get_member(&params->object, DY_STR_LIT(U"position"));
        if (position == NULL || position->tag != DY_JSON_OBJECT || position->object.members == NULL) {
            return true;
        }

        const dy_json_t *line = dy_json_get_member(&position->object, DY_STR_LIT(U"line"));
        if (line == NULL || line->tag != DY_JSON_NUMBER || !line->number.have_value) {
            return true;
        }

        const dy_json_t *character = dy_json_get_member(&position->object, DY_STR_LIT(U"character"));
        if (character == NULL || character->tag != DY_JSON_NUMBER || !character->number.have_value) {
            return true;
        }

        dy_json_t answer = dy_lsp_hover(ctx, id, uri->string.code_points, line->number.value, character->number.value);

        ctx->send(&answer, ctx->env);

        return true;
    }

    if (id != NULL) {
        dy_json_t data = error_response_data(-32601, DY_STR_LIT(U"Unknown method name."));
        dy_json_t answer = error_response(id, &data);
        ctx->send(&answer, ctx->env);
    }

    return true;
}

dy_json_t dy_lsp_initialize(dy_lsp_ctx_t *ctx, const dy_json_t *id)
{
    if (ctx->is_initialized) {
        return invalid_request(id, DY_STR_LIT(U"Already initialized."));
    } else {
        ctx->is_initialized = true;
        dy_json_t data = initialize_data(id);
        return result_response(id, &data);
    }
}

void dy_lsp_initialized(dy_lsp_ctx_t *ctx)
{
    (void)ctx;
    return;
}

void dy_lsp_shutdown(dy_lsp_ctx_t *ctx)
{
    ctx->received_shutdown_request = true;
}

void dy_lsp_did_open(dy_lsp_ctx_t *ctx, const uint32_t *uri, const uint32_t *text)
{
    dy_document_t doc = {
        .uri = uri,
        .text = text,
        .core_ctx = {
            .running_id = 0,
            .variables_in_scope = dy_array_create(sizeof(dy_free_var_t), 64),
            .captured_inference_vars = dy_array_create(sizeof(dy_captured_inference_var_t), 64),
            .past_subtype_checks = dy_array_create(sizeof(struct dy_core_past_subtype_check), 64),
            .equal_variables = dy_array_create(sizeof(dy_equal_variables_t), 64),
            .free_ids_arrays = dy_array_create(sizeof(size_t *), 8),
            .free_expr_arrays = dy_array_create(sizeof(dy_core_expr_t *), 8),
            .constraints = dy_array_create(sizeof(dy_constraint_t), 64),
            .inverse_subtype_recursion_ids = dy_array_create(sizeof(size_t), 8),
            .potential_transform_recursion_ids = dy_array_create(sizeof(size_t), 8),
            .no_transformation_recursion_ids = dy_array_create(sizeof(size_t), 8),
            .sequence_steps = dy_array_create(sizeof(dy_core_compose_step_t), 80)
        },
        .core_is_present = false
    };

    process_document(ctx, &doc);

    dy_array_add(&ctx->documents, &doc);
}

void dy_lsp_did_close(dy_lsp_ctx_t *ctx, const uint32_t *uri)
{
    const dy_document_t *end = dy_array_past_end(ctx->documents);
    for (dy_document_t *doc = ctx->documents; doc != end; ++doc) {
        if (dy_utf32_arrays_are_equal(doc->uri, uri)) {
            dy_array_remove(&ctx->documents, doc);
            break;
        }
    }
}

void dy_lsp_did_change(dy_lsp_ctx_t *ctx, const uint32_t *uri, const dy_json_t *content_changes)
{
    const dy_document_t *docs_end = dy_array_past_end(ctx->documents);
    for (dy_document_t *doc = ctx->documents; doc != docs_end; ++doc) {
        if (!dy_utf32_arrays_are_equal(doc->uri, uri)) {
            continue;
        }

        const dy_json_t *changes_end = dy_array_past_end(content_changes);
        for (const dy_json_t *change = content_changes; change != changes_end; ++change) {
            if (change->tag != DY_JSON_OBJECT || change->object.members == NULL) {
                continue;
            }

            const dy_json_t *text = dy_json_get_member(&change->object, DY_STR_LIT(U"text"));
            if (text == NULL || text->tag != DY_JSON_STRING || text->string.code_points == NULL) {
                continue;
            }

            doc->text = text->string.code_points;

            process_document(ctx, doc);
        }

        break;
    }
}

dy_json_t dy_lsp_hover(dy_lsp_ctx_t *ctx, const dy_json_t *id, const uint32_t *uri, int64_t line_number, int64_t utf16_char_offset)
{
    const dy_document_t *docs_end = dy_array_past_end(ctx->documents);
    dy_document_t *doc = NULL;
    for (doc = ctx->documents; doc != docs_end; ++doc) {
        if (dy_utf32_arrays_are_equal(doc->uri, uri)) {
            break;
        }
    }

    if (doc == NULL) {
        return invalid_request(id, DY_STR_LIT(U"Could not find the document."));
    }

    size_t utf8_offset;
    if (!compute_utf8_offset(doc->text, line_number, utf16_char_offset, &utf8_offset)) {
        return invalid_request(id, DY_STR_LIT(U"Invalid line/col."));
    }

    if (!doc->core_is_present) {
        return null_success_response(id);
    }

    return null_success_response(id);

/*
    struct dy_range range;
    uint32_t *message = dy_array_create(sizeof(char), 16);
    if (!dy_find_smallest_range(&doc->core_ctx, doc->core, utf8_offset, &range, &message)) {
        return null_success_response(id);
    }

    dy_json_t hover_data = hover_result(message);

    return result_response(id, &hover_data);
*/
}

void dy_lsp_exit(dy_lsp_ctx_t *ctx)
{
    if (ctx->received_shutdown_request) {
        ctx->exit_code = 0;
    }
}

int dy_lsp_exit_code(dy_lsp_ctx_t *ctx)
{
    return ctx->exit_code;
}

dy_json_t initialize_data(const dy_json_t *id)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 2);

    dy_json_member_t member = dy_json_make_member(
        DY_STR_LIT(U"capabilities"),
        server_capabilities()
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"serverInfo"),
        server_info()
    );
    dy_array_add(&members, &member);

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t result_response(const dy_json_t *id, const dy_json_t *result)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 3);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"jsonrpc"),
        dy_json_make_string(DY_STR_LIT(U"2.0"))
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"id"),
        *id
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"result"),
        *result
    );
    dy_array_add(&members, &member);

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t error_response(const dy_json_t *id, const dy_json_t *error)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 3);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"jsonrpc"),
        dy_json_make_string(DY_STR_LIT(U"2.0"))
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"id"),
        *id
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"error"),
        *error
    );
    dy_array_add(&members, &member);

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t invalid_request(const dy_json_t *id, const uint32_t *message, size_t message_len)
{
    dy_json_t error = error_response_data(-32600, message, message_len);

    return error_response(id, &error);
}

dy_json_t error_response_data(int64_t code, const uint32_t *message, size_t message_len)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 2);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"code"),
        dy_json_make_number(code)
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"message"),
        dy_json_make_string(message, message_len)
    );

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t server_capabilities(void)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 2);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"textDocumentSync"),
        text_document_sync_options()
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"hoverProvider"),
        (dy_json_t){ .tag = DY_JSON_TRUE, .true_literal = DY_JSON_TRUE_TRUE  }
    );

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t server_info(void)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 2);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"name"),
        dy_json_make_string(DY_STR_LIT(U"Duality"))
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"version"),
        dy_json_make_string(DY_STR_LIT(U"0.0.1"))
    );

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t text_document_sync_options(void)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 2);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"openClose"),
        (dy_json_t){ .tag = DY_JSON_TRUE, .true_literal = DY_JSON_TRUE_TRUE  }
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"change"),
        dy_json_make_number(1)
    );

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t null_success_response(const dy_json_t *id)
{
    dy_json_t null = {
        .tag = DY_JSON_NULL,
        .null_literal = DY_JSON_NULL_NULL
    };

    return result_response(id, &null);
}

dy_json_t hover_result(const uint32_t *contents)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 1);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"contents"),
        dy_json_make_string(contents, (size_t)((const uint32_t *)dy_array_past_end(contents) - contents))
    );
    dy_array_add(&members, &member);

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

dy_json_t publish_diagnostics(dy_core_ctx_t *ctx, const uint32_t *uri, const uint32_t *text, dy_core_expr_t expr)
{
    dy_json_member_t *members = dy_array_create(sizeof *members, 3);
    dy_json_member_t member;

    member = dy_json_make_member(
        DY_STR_LIT(U"jsonrpc"),
        dy_json_make_string(DY_STR_LIT(U"2.0"))
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"method"),
        dy_json_make_string(DY_STR_LIT(U"textDocument/publishDiagnostics"))
    );
    dy_array_add(&members, &member);

    member = dy_json_make_member(
        DY_STR_LIT(U"params"),
        diagnostics_params(ctx, uri, text, expr)
    );
    dy_array_add(&members, &member);

    return (dy_json_t){
        .tag = DY_JSON_OBJECT,
        .object = dy_json_make_object(members)
    };
}

void process_document(struct dy_lsp_ctx *ctx, dy_document_t *doc)
{
    if (doc->core_is_present) {
        doc->core_is_present = false;
    }

    dy_ast_t ast;
    bool have_ast = false;

    const uint32_t *text_end = dy_array_past_end(doc->text);
    for (const uint32_t *s = doc->text; s != text_end; ++s) {
        uint8_t bytes[4];
        dy_utf32_to_utf8(*s, bytes);

        if (have_ast) {
            have_ast = dy_char_to_ast(bytes[0], &ast);
        } else {
            dy_ast_parse(&ast, bytes[0]);
        }

        if ((bytes[0] & 0b01000000) == 0) {
            continue;
        }

        if (have_ast) {
            have_ast = dy_char_to_ast(bytes[1], &ast);
        } else {
            dy_ast_parse(&ast, bytes[1]);
        }

        if ((bytes[0] & 0b00100000) == 0) {
            continue;
        }

        if (have_ast) {
            have_ast = dy_char_to_ast(bytes[2], &ast);
        } else {
            dy_ast_parse(&ast, bytes[2]);
        }

        if ((bytes[0] & 0b00010000) == 0) {
            continue;
        }

        if (have_ast) {
            have_ast = dy_char_to_ast(bytes[3], &ast);
        } else {
            dy_ast_parse(&ast, bytes[3]);
        }
    }

    if (!have_ast) {
        return;
    }

    dy_ast_to_core_ctx_t ast_to_core_ctx = {
        .running_id = 0,
        .variable_replacements = dy_array_create(sizeof (dy_var_replacement_t), 64)
    };

    dy_core_expr_t core = dy_ast_to_core(&ast_to_core_ctx, &ast);

    doc->core_ctx.running_id = ast_to_core_ctx.running_id;

    dy_check_expr(&doc->core_ctx, &core);

    doc->core_is_present = true;
    doc->core = core;

    dy_json_t msg = publish_diagnostics(&doc->core_ctx, doc->uri, doc->text, doc->core);

    ctx->send(&msg, ctx->env);
}

size_t dy_num_utf8_bytes_for_utf32_codepoint(uint32_t code_point)
{
    if (code_point <= 0x7F) {
        return 1;
    } else if (code_point <= 0x7FF) {
        return 2;
    } else if (code_point <= 0xFFFF) {
        return 3;
    } else {
        return 4;
    }
}

void dy_utf32_to_utf8(uint32_t code_point, uint8_t bytes[static 4])
{
    if (code_point <= 0x7F) {
        bytes[0] = (uint8_t)code_point;
        return;
    }

    if (code_point <= 0x7FF) {
        bytes[0] = 0b11000000 | (uint8_t)(code_point >> 6);
        bytes[1] = 0b10000000 | (uint8_t)(code_point & 0x3F);
        return;
    }

    if (code_point <= 0xFFFF) {
        bytes[0] = 0b11100000 | (uint8_t)(code_point >> 12);
        bytes[1] = 0b10000000 | (uint8_t)((code_point >> 6) & 0x3F);
        bytes[2] = 0b10000000 | (uint8_t)(code_point & 0x3F);
        return;
    }

    bytes[0] = 0b11110000 | (uint8_t)(code_point >> 18);
    bytes[1] = 0b10000000 | (uint8_t)((code_point >> 12) & 0x3F);
    bytes[2] = 0b10000000 | (uint8_t)((code_point >> 6) & 0x3F);
    bytes[2] = 0b10000000 | (uint8_t)(code_point & 0x3F);
}

bool compute_utf8_offset(const uint32_t *text, int64_t line_offset, int64_t utf16_offset, size_t *utf8_offset)
{
    if (line_offset < 0 || utf16_offset < 0) {
        return false;
    }

    size_t running_utf8_offset = 0;

    const uint32_t *text_end = dy_array_past_end(text);
    const uint32_t *s = text;
    for (int64_t i = 0; i < line_offset; ++i) {
        if (s == text_end) {
            return false;
        }

        for (;;) {
            if (*s == '\n') {
                ++s;
                running_utf8_offset += dy_num_utf8_bytes_for_utf32_codepoint(*s);
                break;
            }

            ++s;
            running_utf8_offset += dy_num_utf8_bytes_for_utf32_codepoint(*s);
        }
    }

    for (int64_t i = 0; i < utf16_offset; ++i) {
        if (s == text_end) {
            return false;
        }

        if (*s == '\n') {
            return false;
        }

        if (*s > 0xFFFF) {
            // Would need two utf-16 code points.
            ++i;

            if (i >= utf16_offset) {
                // Offset between surrogate pair.
                break;
            }
        }

        running_utf8_offset += dy_num_utf8_bytes_for_utf32_codepoint(*s);

        ++s;
    }

    *utf8_offset = running_utf8_offset;

    return true;
}
/*
dy_json_t produce_diagnostics(dy_core_ctx_t *ctx, const uint32_t *text, dy_core_expr_t expr)
{
    dy_array_t errors = dy_array_create(sizeof(struct dy_error), 8);

    dy_core_expr_t e;
    scan_for_errors(expr, false, &e, &errors);

    for (size_t i = 0; i < errors->len; ++i) {
        const struct dy_error *err = dy_array_pos(&errors, i);

        if (err->have_text_loc) {
            make_diagnostic(text, err->text_loc, 1, DY_STR_LIT("Error"), json);
        }
    }

    return true;
}

void compute_lsp_range(dy_string_t text, struct dy_range range, dy_array_t *json)
{
    long line_start = 0;
    long character_start = 0;

    for (size_t i = 0; i < range.start; ++i) {
        if (text.first[i] == '\n') {
            ++line_start;
            character_start = 0;
        } else {
            ++character_start;
        }
    }

    long line_end = line_start;
    long character_end = character_start;

    for (size_t i = range.start; i < range.end; ++i) {
        if (text.first[i] == '\n') {
            ++line_end;
            character_end = 0;
        } else {
            ++character_end;
        }
    }

    make_range(line_start, character_start, line_end, character_end, json);
}

void make_diagnostic(dy_string_t text, struct dy_range range, long severity, dy_string_t message, dy_array_t *json)
{
    dy_array_add(json, &(uint8_t){ DY_JSON_OBJECT });

    put_string_literal(DY_STR_LIT("range"), json);
    compute_lsp_range(text, range, json);

    put_string_literal(DY_STR_LIT("severity"), json);
    put_number(severity, json);

    put_string_literal(DY_STR_LIT("message"), json);
    put_string_literal(message, json);

    dy_array_add(json, &(uint8_t){ DY_JSON_END });
}

void diagnostics_params(dy_core_ctx_t *ctx, dy_string_t uri, dy_string_t text, dy_core_expr_t expr, dy_array_t *json)
{
    dy_array_add(json, &(uint8_t){ DY_JSON_OBJECT });

    put_string_literal(DY_STR_LIT("uri"), json);
    put_string_literal(uri, json);

    put_string_literal(DY_STR_LIT("diagnostics"), json);
    dy_array_add(json, &(uint8_t){ DY_JSON_ARRAY });
    produce_diagnostics(ctx, text, expr, json);
    dy_array_add(json, &(uint8_t){ DY_JSON_END });

    dy_array_add(json, &(uint8_t){ DY_JSON_END });
}

void make_position(long line, long character, dy_array_t *json)
{
    dy_array_add(json, &(uint8_t){ DY_JSON_OBJECT });

    put_string_literal(DY_STR_LIT("line"), json);
    put_number(line, json);

    put_string_literal(DY_STR_LIT("character"), json);
    put_number(character, json);

    dy_array_add(json, &(uint8_t){ DY_JSON_END });
}

void make_range(long start_line, long start_character, long end_line, long end_character, dy_array_t *json)
{
    dy_array_add(json, &(uint8_t){ DY_JSON_OBJECT });

    put_string_literal(DY_STR_LIT("start"), json);
    make_position(start_line, start_character, json);

    put_string_literal(DY_STR_LIT("end"), json);
    make_position(end_line, end_character, json);

    dy_array_add(json, &(uint8_t){ DY_JSON_END });
}

bool scan_for_errors(dy_core_expr_t expr, bool convert_errors_into_traps, dy_core_expr_t *new_expr, dy_array_t *errors)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        if (expr.intro.is_complex) {
            switch (expr.intro.complex.tag) {
            case DY_CORE_FUNCTION: {
                bool have_new_type = false;
                dy_core_expr_t new_type;
                if (expr.intro.complex.function.type != NULL) {
                    have_new_type = scan_for_errors(*expr.intro.complex.function.type, convert_errors_into_traps, &new_type, errors);
                }

                dy_core_expr_t new_e;
                bool have_new_e = scan_for_errors(*expr.intro.complex.function.sequent, convert_errors_into_traps, &new_e, errors);

                if (!have_new_type && !have_new_e) {
                    return false;
                }

                if (have_new_type) {
                    expr.intro.complex.function.type = dy_core_expr_new(new_type);
                } else if (expr.intro.complex.function.type != NULL) {
                    dy_rc_retain(expr.intro.complex.function.type);
                }

                if (have_new_e) {
                    expr.intro.complex.function.sequent = dy_core_expr_new(new_e);
                } else {
                    dy_rc_retain(expr.intro.complex.function.sequent);
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_TUPLE: {
                const dy_core_expr_t *src = expr.intro.complex.tuple.sequents;
                const dy_core_expr_t *src_end = dy_rc_array_end(src);

                dy_core_expr_t *tuple = dy_rc_array_alloc(sizeof *tuple, src_end - src);
                dy_core_expr_t *dst = tuple;

                bool have_anything_new = false;

                for (; src != src_end; ++src, ++dst) {
                    if (scan_for_errors(*src, convert_errors_into_traps, dst, errors)) {
                        have_anything_new = true;
                    } else {
                        *dst = dy_core_expr_retain(*src);
                    }
                }

                if (!have_anything_new) {


                    return false;
                }

                expr.intro.complex.tuple.sequents = tuple;

                *new_expr = expr;

                return true;
            }
            case DY_CORE_RECURSION: {
                dy_core_expr_t new_e;
                if (!scan_for_errors(*expr.intro.complex.recursion.sequent, convert_errors_into_traps, &new_e, errors)) {
                    return false;
                }

                expr.intro.complex.recursion.sequent = dy_core_expr_new(new_e);

                *new_expr = expr;

                return true;
            }
            }

            g_error("impossible");
        } else {
            switch (expr.intro.simple.tag) {
            case DY_CORE_MAPPING: {
                dy_core_expr_t new_mapping;
                bool have_new_mapping = scan_for_errors(*expr.intro.simple.mapping.expr, convert_errors_into_traps, &new_mapping, errors);

                dy_core_expr_t new_out;
                bool have_new_out = scan_for_errors(*expr.intro.simple.mapping.sequent, convert_errors_into_traps, &new_out, errors);

                if (!have_new_mapping && !have_new_out) {
                    return false;
                }

                if (have_new_mapping) {
                    expr.intro.simple.mapping.expr = dy_core_expr_new(new_mapping);
                } else {
                    dy_rc_retain(expr.intro.simple.mapping.expr);
                }

                if (have_new_out) {
                    expr.intro.simple.mapping.sequent = dy_core_expr_new(new_out);
                } else {
                    dy_rc_retain(expr.intro.simple.mapping.sequent);
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_SELECTION: {
                dy_core_expr_t new_out;
                bool have_new_out = scan_for_errors(*expr.intro.simple.selection.sequent, convert_errors_into_traps, &new_out, errors);

                if (!have_new_out) {
                    return false;
                }

                if (have_new_out) {
                    expr.intro.simple.selection.sequent = dy_core_expr_new(new_out);
                } else {
                    dy_rc_retain(expr.intro.simple.selection.sequent);
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_UNFOLDING: {
                dy_core_expr_t new_out;
                bool have_new_out = scan_for_errors(*expr.intro.simple.unfolding.sequent, convert_errors_into_traps, &new_out, errors);

                if (!have_new_out) {
                    return false;
                }

                if (have_new_out) {
                    expr.intro.simple.unfolding.sequent = dy_core_expr_new(new_out);
                } else {
                    dy_rc_retain(expr.intro.simple.unfolding.sequent);
                }

                *new_expr = expr;

                return true;
            }
            }

            g_error("impossible");
        }
    case DY_CORE_EXPR_ELIM: {
        dy_core_expr_t new_e;
        bool have_new_e = scan_for_errors(*expr.elim.expr, convert_errors_into_traps, &new_e, errors);

        if (expr.elim.is_complex) {
            dy_core_expr_t new_result_type;
            bool have_new_result_type = scan_for_errors(*expr.elim.complex.result_type, convert_errors_into_traps, &new_result_type, errors);

            switch (expr.elim.complex.tag) {
            case DY_CORE_FUNCTION: {
                dy_core_expr_t new_type;
                bool have_new_type = scan_for_errors(*expr.elim.complex.function.type, convert_errors_into_traps, &new_type, errors);

                dy_core_expr_t new_cont_type;
                bool have_new_cont_type = scan_for_errors(*expr.elim.complex.function.cont.type, convert_errors_into_traps, &new_cont_type, errors);

                dy_core_expr_t new_cont_expr;
                bool have_new_cont_expr = scan_for_errors(*expr.elim.complex.function.cont.sequent, convert_errors_into_traps, &new_cont_expr, errors);

                if (expr.elim.check_result == DY_CHECK_STATUS_FAIL || expr.elim.complex.function.check_result == DY_CHECK_STATUS_FAIL) {
                    dy_array_add(errors, &(struct dy_error){
                        .text_loc = expr.elim.text_loc,
                        .have_text_loc = expr.elim.have_text_loc
                    });

                    if (convert_errors_into_traps) {
                        *new_expr = (dy_core_expr_t){
                            .tag = DY_CORE_EXPR_TRAP
                        };

                        return true;
                    }
                }

                if (!have_new_e && !have_new_type && !have_new_cont_type && !have_new_cont_expr && !have_new_result_type) {
                    return false;
                }

                if (have_new_e) {
                    expr.elim.expr = dy_core_expr_new(new_e);
                } else {
                    dy_rc_retain(expr.elim.expr);
                }

                if (have_new_type) {
                    expr.elim.complex.function.type = dy_core_expr_new(new_type);
                } else {
                    dy_rc_retain(expr.elim.complex.function.type);
                }

                if (have_new_cont_type) {
                    expr.elim.complex.function.cont.type = dy_core_expr_new(new_cont_type);
                } else {
                    dy_rc_retain(expr.elim.complex.function.cont.type);
                }

                if (have_new_cont_expr) {
                    expr.elim.complex.function.cont.sequent = dy_core_expr_new(new_cont_expr);
                } else {
                    dy_rc_retain(expr.elim.complex.function.cont.sequent);
                }

                if (have_new_result_type) {
                    expr.elim.complex.result_type = dy_core_expr_new(new_type);
                } else {
                    dy_rc_retain(expr.elim.complex.result_type);
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_TUPLE: {
                const dy_core_function_t *src = expr.elim.complex.tuple.conts;
                const dy_core_function_t *src_end = dy_rc_array_end(src);

                dy_core_function_t *conts = dy_rc_array_alloc(sizeof *conts, src_end - src);
                dy_core_function_t *dst = conts;

                bool have_anything_new = false;

                for (; src != src_end; ++src, ++dst) {
                    dst->var = src->var;

                    dy_core_expr_t type;
                    if (scan_for_errors(*src->type, convert_errors_into_traps, &type, errors)) {
                        dst->type = dy_core_expr_new(type);
                        have_anything_new = true;
                    } else {
                        dst->type = dy_rc_retain(src->type);
                    }

                    dy_core_expr_t e;
                    if (scan_for_errors(*src->sequent, convert_errors_into_traps, &e, errors)) {
                        have_anything_new = true;
                        dst->sequent = dy_core_expr_new(e);
                    } else {
                        dst->sequent = dy_rc_retain(src->sequent);
                    }
                }

                if (expr.elim.check_result == DY_CHECK_STATUS_FAIL) {
                    dy_array_add(errors, &(struct dy_error){
                        .text_loc = expr.elim.text_loc,
                        .have_text_loc = expr.elim.have_text_loc
                    });

                    if (convert_errors_into_traps) {
                        *new_expr = (dy_core_expr_t){
                            .tag = DY_CORE_EXPR_TRAP
                        };

                        return true;
                    }
                }

                for (const DyCheckStatus *status = expr.elim.complex.tuple.check_results, *end = dy_rc_array_end(status); status != end; ++status) {
                    if (*status == DY_CHECK_STATUS_FAIL) {
                        dy_array_add(errors, &(struct dy_error){
                            .text_loc = expr.elim.text_loc,
                            .have_text_loc = expr.elim.have_text_loc
                        });

                        if (convert_errors_into_traps) {
                            *new_expr = (dy_core_expr_t){
                                .tag = DY_CORE_EXPR_TRAP
                            };

                            return true;
                        }
                    }
                }

                if (!have_new_e && !have_anything_new && !have_new_result_type) {
                    for (const dy_core_function_t *f = conts; f != dst; ++f) {
                        dy_core_function_release(*f);
                    }

                    dy_rc_array_release(conts);

                    return false;
                }

                if (have_new_e) {
                    expr.elim.expr = dy_core_expr_new(new_e);
                } else {
                    dy_rc_retain(expr.elim.expr);
                }

                if (have_new_result_type) {
                    expr.elim.complex.result_type = dy_core_expr_new(new_result_type);
                } else {
                    dy_rc_retain(expr.elim.complex.result_type);
                }

                expr.elim.complex.tuple.conts = conts;

                *new_expr = expr;

                return true;
            }
            case DY_CORE_RECURSION: {
                if (expr.elim.complex.recursion.is_only_id) {
                    if (!have_new_e) {
                        return false;
                    }

                    expr.elim.expr = dy_core_expr_new(new_e);

                    *new_expr = expr;

                    return true;
                }

                dy_core_expr_t new_cont_type;
                bool have_new_cont_type = scan_for_errors(*expr.elim.complex.recursion.cont_with_inference_ctx.cont.type, convert_errors_into_traps, &new_cont_type, errors);

                dy_core_expr_t new_cont_expr;
                bool have_new_cont_expr = scan_for_errors(*expr.elim.complex.recursion.cont_with_inference_ctx.cont.sequent, convert_errors_into_traps, &new_cont_expr, errors);

                if (expr.elim.check_result == DY_CHECK_STATUS_FAIL || expr.elim.complex.recursion.check_result == DY_CHECK_STATUS_FAIL) {
                    dy_array_add(errors, &(struct dy_error){
                        .text_loc = expr.elim.text_loc,
                        .have_text_loc = expr.elim.have_text_loc
                    });

                    if (convert_errors_into_traps) {
                        *new_expr = (dy_core_expr_t){
                            .tag = DY_CORE_EXPR_TRAP
                        };

                        return true;
                    }
                }

                if (!have_new_e && !have_new_cont_type && !have_new_cont_expr && !have_new_result_type) {
                    return false;
                }

                if (have_new_e) {
                    expr.elim.expr = dy_core_expr_new(new_e);
                } else {
                    dy_rc_retain(expr.elim.expr);
                }

                if (have_new_cont_type) {
                    expr.elim.complex.recursion.cont_with_inference_ctx.cont.type = dy_core_expr_new(new_cont_type);
                } else {
                    dy_rc_retain(expr.elim.complex.recursion.cont_with_inference_ctx.cont.type);
                }

                if (have_new_cont_expr) {
                    expr.elim.complex.recursion.cont_with_inference_ctx.cont.sequent = dy_core_expr_new(new_cont_expr);
                } else {
                    dy_rc_retain(expr.elim.complex.recursion.cont_with_inference_ctx.cont.sequent);
                }

                if (have_new_result_type) {
                    expr.elim.complex.result_type = dy_core_expr_new(new_result_type);
                } else {
                    dy_rc_retain(expr.elim.complex.result_type);
                }

                *new_expr = expr;

                return true;
            }
            }

            g_error("impossible");
        } else {
            switch (expr.elim.simple.tag) {
            case DY_CORE_MAPPING: {
                dy_core_expr_t new_mapping_expr;
                bool have_new_mapping_expr = scan_for_errors(*expr.elim.simple.mapping.expr, convert_errors_into_traps, &new_mapping_expr, errors);

                dy_core_expr_t new_sequent;
                bool have_new_sequent = scan_for_errors(*expr.elim.simple.mapping.sequent, convert_errors_into_traps, &new_sequent, errors);

                if (expr.elim.check_result == DY_CHECK_STATUS_FAIL) {
                    dy_array_add(errors, &(struct dy_error){
                        .text_loc = expr.elim.text_loc,
                        .have_text_loc = expr.elim.have_text_loc
                    });

                    if (convert_errors_into_traps) {
                        *new_expr = (dy_core_expr_t){
                            .tag = DY_CORE_EXPR_TRAP
                        };

                        return true;
                    }
                }

                if (!have_new_e && !have_new_mapping_expr && !have_new_sequent) {
                    return false;
                }

                if (have_new_e) {
                    expr.elim.expr = dy_core_expr_new(new_e);
                } else {
                    dy_rc_retain(expr.elim.expr);
                }

                if (have_new_mapping_expr) {
                    expr.elim.simple.mapping.expr = dy_core_expr_new(new_mapping_expr);
                } else {
                    dy_rc_retain(expr.elim.simple.mapping.expr);
                }

                if (have_new_sequent) {
                    expr.elim.simple.mapping.sequent = dy_core_expr_new(new_sequent);
                } else {
                    dy_rc_retain(expr.elim.simple.mapping.sequent);
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_SELECTION: {
                dy_core_expr_t new_sequent;
                bool have_new_sequent = scan_for_errors(*expr.elim.simple.selection.sequent, convert_errors_into_traps, &new_sequent, errors);

                if (expr.elim.check_result == DY_CHECK_STATUS_FAIL) {
                    dy_array_add(errors, &(struct dy_error){
                        .text_loc = expr.elim.text_loc,
                        .have_text_loc = expr.elim.have_text_loc
                    });

                    if (convert_errors_into_traps) {
                        *new_expr = (dy_core_expr_t){
                            .tag = DY_CORE_EXPR_TRAP
                        };

                        return true;
                    }
                }

                if (!have_new_e && !have_new_sequent) {
                    return false;
                }

                if (have_new_e) {
                    expr.elim.expr = dy_core_expr_new(new_e);
                } else {
                    dy_rc_retain(expr.elim.expr);
                }

                if (have_new_sequent) {
                    expr.elim.simple.selection.sequent = dy_core_expr_new(new_sequent);
                } else {
                    dy_rc_retain(expr.elim.simple.selection.sequent);
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_UNFOLDING: {
                dy_core_expr_t new_sequent;
                bool have_new_sequent = scan_for_errors(*expr.elim.simple.unfolding.sequent, convert_errors_into_traps, &new_sequent, errors);

                if (expr.elim.check_result == DY_CHECK_STATUS_FAIL) {
                    dy_array_add(errors, &(struct dy_error){
                        .text_loc = expr.elim.text_loc,
                        .have_text_loc = expr.elim.have_text_loc
                    });

                    if (convert_errors_into_traps) {
                        *new_expr = (dy_core_expr_t){
                            .tag = DY_CORE_EXPR_TRAP
                        };

                        return true;
                    }
                }

                if (!have_new_e && !have_new_sequent) {
                    return false;
                }

                if (have_new_e) {
                    expr.elim.expr = dy_core_expr_new(new_e);
                } else {
                    dy_rc_retain(expr.elim.expr);
                }

                if (have_new_sequent) {
                    expr.elim.simple.unfolding.sequent = dy_core_expr_new(new_sequent);
                } else {
                    dy_rc_retain(expr.elim.simple.unfolding.sequent);
                }

                *new_expr = expr;

                return true;
            }
            }

            g_error("impossible");
        }
    }
    case DY_CORE_EXPR_REF:
        if (expr.variable.is_unbound) {
            // TODO: Error
        }

        return false;
    case DY_CORE_EXPR_INFERENCE_VAR:
    case DY_CORE_EXPR_INFERENCE_CTX:
        g_error("impossible");
    case DY_CORE_EXPR_IMPORT:
        if (expr.import.fopen_errno != 0 || expr.import.fstat_errno != 0 || expr.import.opening_file_would_loop || expr.import.parsing_failed) {
            dy_array_add(errors, &(struct dy_error){
                .text_loc = expr.import.text_loc,
                .have_text_loc = expr.import.have_text_loc
            });

            if (convert_errors_into_traps) {
                *new_expr = (dy_core_expr_t){
                    .tag = DY_CORE_EXPR_TRAP
                };

                return true;
            }
        }

        return false;
    case DY_CORE_EXPR_TRAP:
        if (!convert_errors_into_traps) {
            dy_array_add(errors, &(struct dy_error){
                .text_loc = {
                    .start = expr.trap.text_loc_start_index,
                    .end = expr.trap.text_loc_start_index + sizeof ("trap") - 1
                },
                .have_text_loc = expr.trap.have_text_loc_start_index
            });

            return true;
        } else {
            return false;
        }
    case DY_CORE_EXPR_ALIAS:
        g_error("impossible");
    case DY_CORE_EXPR_COMP: {
        dy_core_expr_t new_comp_type;
        if (scan_for_errors(*expr.comp.expr, convert_errors_into_traps, &new_comp_type, errors)) {
            expr.comp.expr = dy_core_expr_new(new_comp_type);

            *new_expr = expr;

            return true;
        } else {
            return false;
        }
    }
    case DY_CORE_EXPR_SEQUENCE: {
        dy_core_expr_t new_first;
        bool have_new_first = scan_for_errors(*expr.compose.first, convert_errors_into_traps, &new_first, errors);

        dy_core_expr_t new_first_type;
        bool have_new_first_type = scan_for_errors(*expr.compose.first_type, convert_errors_into_traps, &new_first_type, errors);

        dy_core_expr_t new_second;
        bool have_new_second = scan_for_errors(*expr.compose.second, convert_errors_into_traps, &new_second, errors);

        dy_core_expr_t new_second_type;
        bool have_new_second_type = scan_for_errors(*expr.compose.second_type, convert_errors_into_traps, &new_second_type, errors);

        if (expr.compose.first_result == DY_CHECK_STATUS_FAIL || expr.compose.second_result == DY_CHECK_STATUS_FAIL) {
            dy_array_add(errors, &(struct dy_error){
                .text_loc = expr.compose.text_loc,
                .have_text_loc = expr.compose.have_text_loc
            });

            if (convert_errors_into_traps) {
                *new_expr = (dy_core_expr_t){
                    .tag = DY_CORE_EXPR_TRAP
                };

                return true;
            }
        }

        if (!have_new_first && !have_new_first_type && !have_new_second && !have_new_second_type) {
            return false;
        }

        if (have_new_first) {
            expr.compose.first = dy_core_expr_new(new_first);
        } else {
            dy_rc_retain(expr.compose.first);
        }

        if (have_new_first_type) {
            expr.compose.first_type = dy_core_expr_new(new_first_type);
        } else {
            dy_rc_retain(expr.compose.first_type);
        }

        if (have_new_second) {
            expr.compose.second = dy_core_expr_new(new_second);
        } else {
            dy_rc_retain(expr.compose.second);
        }

        if (have_new_second_type) {
            expr.compose.second_type = dy_core_expr_new(new_second_type);
        } else {
            dy_rc_retain(expr.compose.second_type);
        }

        *new_expr = expr;

        return true;
    }
    }

    g_error("impossible");

    return false;
}

bool dy_find_smallest_range(dy_core_ctx_t *ctx, dy_core_expr_t expr, size_t byte_offset, struct dy_range *range, uint32_t **message)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        if (expr.intro.is_complex) {
            switch (expr.intro.complex.tag) {
            case DY_CORE_FUNCTION: {
                if (expr.intro.complex.function.var.have_text_loc && expr.intro.complex.function.var.text_loc.start <= byte_offset && byte_offset < expr.intro.complex.function.var.text_loc.end) {
                    *range = expr.intro.complex.function.text_loc;
                    dy_core_add_string(message, DY_STR_LIT("It's a variable, mario!"));
                    return true;
                }

                return false;
            }
            case DY_CORE_TUPLE:
            case DY_CORE_RECURSION:
                return false;
            }

            g_error("impossible");
        } else {
            g_error("impossible");
        }
    case DY_CORE_EXPR_ELIM:
    case DY_CORE_EXPR_REF:
    case DY_CORE_EXPR_TRAP:
    case DY_CORE_EXPR_SEQUENCE:
    case DY_CORE_EXPR_COMP:
        return false;
    case DY_CORE_EXPR_INFERENCE_CTX:
    case DY_CORE_EXPR_INFERENCE_VAR:
        g_error("impossible");
    }
}
*/
bool dy_utf32_strings_are_equal(const uint32_t *array, const uint32_t *s, size_t len_s)
{
    const uint32_t *end = dy_array_past_end(array);

    size_t array_size = (size_t)(end - array);

    if (array_size != len_s) {
        return false;
    }

    for (size_t i = 0; i < len_s; ++i) {
        if (array[i] != s[i]) {
            return false;
        }
    }

    return true;
}

bool dy_utf32_arrays_are_equal(const uint32_t *ar1, const uint32_t *ar2)
{
    const uint32_t *ar1_end = dy_array_past_end(ar1);
    const uint32_t *ar2_end = dy_array_past_end(ar2);

    size_t ar1_len = (size_t)(ar1_end - ar1);
    size_t ar2_len = (size_t)(ar2_end - ar2);

    if (ar1_len != ar2_len) {
        return false;
    }

    for (size_t i = 0; i < ar1_len; ++i) {
        if (ar1[i] != ar2[i]) {
            return false;
        }
    }

    return true;
}
