#pragma once

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

typedef enum dy_content_length_lit {
    DY_C,
    DY_CO,
    DY_CON,
    DY_CONT,
    DY_CONTE,
    DY_CONTEN,
    DY_CONTENT,
    DY_CONTENT_,
    DY_CONTENT_L,
    DY_CONTENT_LE,
    DY_CONTENT_LEN,
    DY_CONTENT_LENG,
    DY_CONTENT_LENGT,
    DY_CONTENT_LENGTH
} dy_content_length_lit_t;

typedef struct dy_content_length {
    dy_content_length_lit_t lit;
    bool have_colon;
    bool have_value;
    size_t value;
} dy_content_length_t;

typedef enum dy_http_header_tag {
    DY_HTTP_HEADER_CONTENT_LENGTH
} dy_http_header_tag_t;

typedef struct dy_http_header {
    union {
        dy_content_length_t content_length;
    };

    dy_http_header_tag_t tag;
} dy_http_header_t;

typedef struct dy_http_headers {
    dy_http_header_t *headers;
    dy_http_header_t *current_header; // Can be NULL.
    bool have_carriage;
    bool have_newline;
} dy_http_headers_t;

static inline bool dy_char_to_http_headers(uint8_t c, dy_http_headers_t *headers);

static inline bool dy_http_headers_parse(dy_http_headers_t *headers, uint8_t c);

static inline bool dy_char_to_http_header(uint8_t c, dy_http_header_t *header);

static inline bool dy_http_header_parse(dy_http_header_t *header, uint8_t c);

static inline bool dy_http_header_content_length_parse(dy_content_length_t *cl, char c);
