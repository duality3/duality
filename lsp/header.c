#include "header.h"

#include "../array/array.h"

bool dy_char_to_http_headers(uint8_t c, dy_http_headers_t *headers)
{
    dy_http_header_t header;
    if (!dy_char_to_http_header(c, &header)) {
        return false;
    }

    headers->headers = dy_array_create(sizeof *headers->headers, 2);
    headers->current_header = dy_array_add(&headers->headers, &header);

    return true;
}

bool dy_http_headers_parse(dy_http_headers_t *headers, uint8_t c)
{
    if (headers->have_newline) {
        return false;
    }

    if (headers->current_header != NULL) {
        if (dy_http_header_parse(headers->current_header, c)) {
            return true;
        }

        if (!headers->have_carriage) {
            if (c == '\r') {
                headers->have_carriage = true;
                return true;
            }

            return false;
        }

        if (c == '\n') {
            headers->have_carriage = false;
            headers->current_header = NULL;
            return true;
        }

        return false;
    }

    if (!headers->have_carriage) {
        dy_http_header_t hdr;
        if (dy_char_to_http_header(c, &hdr)) {
            headers->headers = dy_array_add(&headers->headers, &hdr);
            return true;
        }

        if (c == '\r') {
            headers->have_carriage = true;
            return true;
        }

        return false;
    }

    if (c == '\n') {
        headers->have_newline = true;
        return true;
    }

    return false;
}

bool dy_char_to_http_header(uint8_t c, dy_http_header_t *header)
{
    if (c == 'c' || c == 'C') {
        *header = (dy_http_header_t){
            .tag = DY_HTTP_HEADER_CONTENT_LENGTH,
            .content_length = {
                .lit = DY_C
            }
        };

        return true;
    }

    return false;
}

bool dy_http_header_parse(dy_http_header_t *header, uint8_t c)
{
    switch (header->tag) {
    case DY_HTTP_HEADER_CONTENT_LENGTH:
        return dy_http_header_content_length_parse(&header->content_length, c);
    }
}

bool dy_http_header_content_length_parse(dy_content_length_t *cl, char c)
{
    switch (cl->lit) {
    case DY_C:
    }
}
