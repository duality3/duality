/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <duality/core.h>

#include <stdlib.h>
#include <assert.h>

#include <duality/array.h>

#define DY_STR_LIT_PTR_PARAMS(s) ((unsigned char *)s), ((sizeof s) - 1)

const void *dy_core_get_aux_data(const dy_core_expr_t *expr)
{
    return *(const void * const*)expr;
}

void dy_core_set_aux_data(dy_core_expr_t *expr, const void *aux_data)
{
    *(const void **)expr = aux_data;
}

dy_core_expr_t *dy_core_expr_new(dy_core_expr_t expr)
{
    dy_core_expr_t *p = malloc(sizeof *p);
    assert(p != NULL);
    *p = expr;
    return p;
}

void dy_core_add_string(uint8_t **string, const uint8_t *s, size_t len_s)
{
    for (size_t i = 0; i < len_s; ++i) {
        dy_array_add(string, &s[i]);
    }
}

void dy_core_add_size_t_decimal(uint8_t **string, size_t x)
{
    size_t start_size = (size_t)(((const uint8_t *)dy_array_past_end(*string) - *string));

    // Add digits in reverse order.
    for (;;) {
        unsigned char digit = x % 10 + '0';

        dy_array_add(string, &digit);

        x /= 10;

        if (x == 0) {
            break;
        }
    }

    uint8_t *start = &(*string)[start_size];
    uint8_t *past_end = (uint8_t *)dy_array_last(*string) + 1;
    if (start == past_end) {
        return;
    }

    uint8_t *end = past_end - 1;

    // Reverse digits in buffer.
    for (; start < end; ++start, --end) {
        unsigned char c = *start;
        *start = *end;
        *end = c;
    }
}
