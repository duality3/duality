/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "is_subtype.h"

#include "adjust_free_refs.h"

#include <duality/array.h>

dy_subtype_res_t dy_exprs_are_subtypes_and_transform(
    dy_binder_t **binders,
    dy_constraint_t **constraints,

    dy_core_expr_t subtype,
    dy_core_expr_t supertype,

    dy_core_expr_t *subtype_expr,
    dy_core_expr_t *enclosing_comp_expr
)
{
    dy_sequence_step_t *sequence_steps = dy_array_create(sizeof *sequence_steps, 4);

    bool have_new_subtype_expr;
    dy_subtype_res_t res = dy_exprs_are_subtypes(binders, constraints, &sequence_steps, subtype, supertype, *subtype_expr, subtype_expr, &have_new_subtype_expr);

    if (have_new_subtype_expr) {
        size_t num_steps = (size_t)((const dy_sequence_step_t *)dy_array_past_end(sequence_steps) - sequence_steps);

        if (!dy_adjust_free_refs_of_expr(*enclosing_comp_expr, 0, num_steps, DY_ADJUST_FREE_REFS_DIRECTION_DOWN, enclosing_comp_expr, &(bool){ 0 })) {
            return DY_SUBTYPE_RES_FAIL;
        }

        for (size_t i = num_steps; i-- > 0;) {
            *enclosing_comp_expr = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_SEQUENCE,
                .sequence = {
                    .expr = &sequence_steps[i].expr,
                    .cont = {
                        .type = &sequence_steps[i].type,
                        .sequent = dy_core_expr_new(*enclosing_comp_expr)
                    },
                    .type = dy_core_expr_new(supertype),
                    .expr_result = DY_CHECK_STATUS_OK,
                    .type_result = DY_CHECK_STATUS_OK
                }
            };
        }
    }

    return res;
}

dy_subtype_res_t dy_exprs_are_subtypes(
    dy_binder_t **binders,
    dy_constraint_t **constraints,
    dy_sequence_step_t **sequence_steps,

    dy_core_expr_t subtype,
    dy_core_expr_t supertype,

    dy_core_expr_t subtype_expr,
    dy_core_expr_t *new_subtype_expr,
    bool *have_new_subtype_expr
)
{
    return DY_SUBTYPE_RES_FAIL;
}
