/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "adjust_free_refs.c"
#include "are_equal.c"
#include "array.c"
#include "check.c"
#include "checked_arith.c"
#include "constraint.c"
#include "core_to_utf8.c"
#include "core.c"
#include "is_subtype.c"
#include "ref_appears_in_polarity.c"
#include "ref_occurs_in.c"
#include "substitute_in_place.c"
#include "substitute.c"
#include "type_of.c"
#include "utf8_to_core.c"
