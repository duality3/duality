/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

/**
 * The functions here define equality for all objects of Core.
 */

static inline
bool dy_exprs_are_equal(dy_core_expr_t e1, dy_core_expr_t e2);

static inline
bool dy_intros_are_equal(dy_core_intro_t intro1, dy_core_intro_t intro2);

static inline
bool dy_complexes_are_equal(dy_core_complex_t complex1, dy_core_complex_t complex2);

static inline
bool dy_functions_are_equal(dy_core_function_t fun1, dy_core_function_t fun2);

static inline
bool dy_pairs_are_equal(dy_core_tuple_t pair1, dy_core_tuple_t pair2);

static inline
bool dy_recursions_are_equal(dy_core_recursion_t rec1, dy_core_recursion_t rec2);

static inline
bool dy_simple_prefixes_are_equal(dy_core_simple_prefix_t prefix1, dy_core_simple_prefix_t prefix2);

static inline
bool dy_simples_are_equal(dy_core_simple_t simple1, dy_core_simple_t simple2);

static inline
bool dy_elims_are_equal(dy_core_elim_t elim1, dy_core_elim_t elim2);

static inline
bool dy_complex_eliminators_are_equal(dy_core_complex_eliminator_t complex1, dy_core_complex_eliminator_t complex2);

static inline
bool dy_function_eliminators_are_equal(dy_core_function_eliminator_t fun1, dy_core_function_eliminator_t fun2);

static inline
bool dy_pair_eliminators_are_equal(dy_core_tuple_eliminator_t pair1, dy_core_tuple_eliminator_t pair2);

static inline
bool dy_recursion_eliminators_are_equal(dy_core_recursion_eliminator_t rec1, dy_core_recursion_eliminator_t rec2);

static inline
bool dy_elim_refs_are_equal(dy_core_elim_ref_t elim_ref1, dy_core_elim_ref_t elim_ref2);

static inline
bool dy_composes_are_equal(dy_core_sequence_t c1, dy_core_sequence_t c2);
