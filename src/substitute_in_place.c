/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "substitute_in_place.h"

#include "adjust_free_refs.h"

#include <duality/array.h>

bool dy_substitute_expr_in_place(dy_core_expr_t *expr, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    switch (expr->tag) {
    case DY_CORE_EXPR_INTRO:
        return dy_substitute_intro_in_place(&expr->intro, index, sub, did_substitute);
    case DY_CORE_EXPR_ELIM:
        return dy_substitute_elim_in_place(&expr->elim, index, sub, did_substitute);
    case DY_CORE_EXPR_REF:
        return dy_substitute_variable_in_place(&expr->ref, expr, index, sub, did_substitute);
    case DY_CORE_EXPR_ELIM_REF:
        return dy_substitute_elim_ref_in_place(&expr->elim_ref, index, sub, did_substitute);
    case DY_CORE_EXPR_SEQUENCE:
        return dy_substitute_sequence_in_place(&expr->sequence, index, sub, did_substitute);
    case DY_CORE_EXPR_COMP:
        return dy_substitute_comp_in_place(&expr->comp, index, sub, did_substitute);
    case DY_CORE_EXPR_TRAP:
        *did_substitute = false;
        return true;
    case DY_CORE_EXPR_INFERENCE_CTX:
        return false;
    }
}

bool dy_substitute_intro_in_place(dy_core_intro_t *intro, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    switch (intro->tag) {
    case DY_INTRO_COMPLEX:
        return dy_substitute_complex_in_place(&intro->complex, index, sub, did_substitute);
    case DY_INTRO_SIMPLE:
        return dy_substitute_simple_in_place(&intro->simple, index, sub, did_substitute);
    }
}

bool dy_substitute_complex_in_place(dy_core_complex_t *complex, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    switch (complex->tag) {
    case DY_CORE_FUNCTION:
        return dy_substitute_function_in_place(&complex->function, index, sub, did_substitute);
    case DY_CORE_TUPLE:
        return dy_substitute_tuple_in_place(&complex->tuple, index, sub, did_substitute);
    case DY_CORE_RECURSION:
        return dy_substitute_recursion_in_place(&complex->recursion, index, sub, did_substitute);
    }
}

bool dy_substitute_function_in_place(dy_core_function_t *function, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    bool have_type = false;
    if (function->type != NULL) {
        if (!dy_substitute_expr_in_place(function->type, index, sub, &have_type)) {
            return false;
        }
    }

    bool have_sequent;
    if (!dy_substitute_expr_in_place(function->sequent, index + 1, sub, &have_sequent)) {
        return false;
    }

    *did_substitute = have_type || have_sequent;
    return true;
}

bool dy_substitute_tuple_in_place(dy_core_tuple_t *tuple, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    const dy_core_expr_t *end = dy_array_past_end(tuple->sequents);

    bool have_anything_new = false;

    for (dy_core_expr_t *e = tuple->sequents; e != end; ++e) {
        bool have_new_e;
        if (!dy_substitute_expr_in_place(e, index, sub, &have_new_e)) {
            return false;
        }

        if (have_new_e) {
            have_anything_new = true;
        }
    }

    *did_substitute = have_anything_new;
    return true;
}

bool dy_substitute_recursion_in_place(dy_core_recursion_t *recursion, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    return dy_substitute_expr_in_place(recursion->sequent, index + 1, sub, did_substitute);
}

bool dy_substitute_simple_prefix_in_place(dy_core_simple_prefix_t *simple, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    if (simple->tag != DY_CORE_MAPPING) {
        *did_substitute = false;
        return true;
    }

    return dy_substitute_expr_in_place(simple->expr, index, sub, did_substitute);
}

bool dy_substitute_simple_in_place(dy_core_simple_t *simple, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    bool have_simple_prefix;
    if (!dy_substitute_simple_prefix_in_place(&simple->prefix, index, sub, &have_simple_prefix)) {
        return false;
    }

    bool have_sequent;
    if (!dy_substitute_expr_in_place(simple->sequent, index, sub, &have_sequent)) {
        return false;
    }

    *did_substitute = have_simple_prefix || have_sequent;
    return true;
}

bool dy_substitute_elim_in_place(dy_core_elim_t *elim, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    bool have_expr;
    if (!dy_substitute_expr_in_place(elim->expr, index, sub, &have_expr)) {
        return false;
    }

    bool did_substitute_type;
    if (!dy_substitute_expr_in_place(elim->result_type, index, sub, &did_substitute_type)) {
        return false;
    }

    bool have_simple_or_complex;
    switch (elim->tag) {
    case DY_INTRO_COMPLEX:
        if (!dy_substitute_complex_eliminator_in_place(&elim->complex, index, sub, &have_simple_or_complex)) {
            return false;
        }

        break;
    case DY_INTRO_SIMPLE:
        if (!dy_substitute_simple_prefix_in_place(&elim->simple_prefix, index, sub, &have_simple_or_complex)) {
            return false;
        }

        break;
    }

    *did_substitute = have_expr || did_substitute_type || have_simple_or_complex;

    return true;
}

bool dy_substitute_complex_eliminator_in_place(dy_core_complex_eliminator_t *complex_elim, size_t index, dy_core_expr_t sub,  bool *did_substitute)
{
    switch (complex_elim->tag) {
    case DY_CORE_FUNCTION:
        return dy_substitute_function_eliminator_in_place(&complex_elim->function, index, sub, did_substitute);
    case DY_CORE_TUPLE:
        return dy_substitute_tuple_eliminator_in_place(&complex_elim->tuple, index, sub, did_substitute);
    case DY_CORE_RECURSION:
        return dy_substitute_recursion_eliminator_in_place(&complex_elim->recursion, index, sub, did_substitute);
    }
}

bool dy_substitute_function_eliminator_in_place(dy_core_function_eliminator_t *function_elim, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    bool have_type = false;
    if (function_elim->type != NULL) {
        if (!dy_substitute_expr_in_place(function_elim->type, index, sub, &have_type)) {
            return false;
        }
    }

    bool have_cont;
    if (!dy_substitute_function_in_place(&function_elim->cont, index + 1, sub, &have_cont)) {
        return false;
    }

    *did_substitute = have_type || have_cont;

    return true;
}

bool dy_substitute_tuple_eliminator_in_place(dy_core_tuple_eliminator_t *tuple_elim, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    dy_core_cont_with_check_status_t *cont = tuple_elim->conts_with_check_status;
    const dy_core_cont_with_check_status_t *end = dy_array_past_end(cont);

    bool have_anything_new = false;

    for (; cont != end; ++cont) {
        bool have_cont;
        if (!dy_substitute_function_in_place(&cont->cont, index, sub, &have_cont)) {
            return false;
        }

        if (have_cont) {
            have_anything_new = true;
        }
    }

    *did_substitute = have_anything_new;

    return true;
}

bool dy_substitute_recursion_eliminator_in_place(dy_core_recursion_eliminator_t *recursion_elim, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    return dy_substitute_function_in_place(&recursion_elim->cont, index + 1, sub, did_substitute);
}

bool dy_substitute_variable_in_place(size_t *variable_index, dy_core_expr_t *expr, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    if (*variable_index != index) {
        *did_substitute = false;
        return true;
    }

    if (!dy_adjust_free_refs_of_expr(sub, 0, index, DY_ADJUST_FREE_REFS_DIRECTION_DOWN, expr, did_substitute)) {
        return false;
    }

    if (!*did_substitute) {
        *did_substitute = true;
        *expr = sub;
    }

    return true;
}

bool dy_substitute_elim_ref_in_place(dy_core_elim_ref_t *elim_ref, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    return dy_substitute_expr_in_place(elim_ref->expr, index, sub, did_substitute);
}

bool dy_substitute_sequence_in_place(dy_core_sequence_t *sequence, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    bool have_expr;
    if (!dy_substitute_expr_in_place(sequence->expr, index, sub, &have_expr)) {
        return false;
    }

    bool have_cont;
    if (!dy_substitute_function_in_place(&sequence->cont, index, sub, &have_cont)) {
        return false;
    }

    bool have_type;
    if (!dy_substitute_expr_in_place(sequence->type, index, sub, &have_type)) {
        return false;
    }

    *did_substitute = have_expr || have_cont || have_type;

    return true;
}

bool dy_substitute_comp_in_place(dy_core_comp_t *comp, size_t index, dy_core_expr_t sub, bool *did_substitute)
{
    return dy_substitute_expr_in_place(comp->expr, index, sub, did_substitute);
}
