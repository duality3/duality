# Core - The Duality Calculus

The files in this folder implement the Duality Calculus (DC),
and depend on the files in the 'support' folder.

While DC has an informal syntactic representation (just to be able to write it in text documents),
the actual syntax of Duality is implemented and described in the 'syntax' folder.

## Main Characteristics

 - Every construct in DC is an expression, and (almost) every expression has a type.
 
 - DC is purely statically typed. Type annotations are irrelevant during reduction.
 
 - The language of expressions in type annotations is DC itself; type and term language are unified.
 
 - DC is referentially transparent in that equal eliminations always reduce to the same value.
 
 - However, the reduction of eliminations in DC is not generally assumed to be free of side effects. 
   Side effects are allowed as long as they don't affect the resulting value.
 
 - The fundamental relation in DC is subtyping; bifurcating many constructs in DC into positive and negative versions.
 
 - DC features full type inference; further bifurcating many constructs into explicit and implicit versions. 
   Type annotations are always optional.
 
 - Every DC expression is checked for correctness by marking every faulty elimination.
 
 - No checked-correct DC expression ever produces an error, loops infinitely or crashes due to malformed program logic.
 
 - DC is not Turing Complete, but nevertheless meant to be (the core of) a general-purpose programming language.
 
## Constructs
 
There are three fundamental constructs in DC, and each of them 

Function: <var> : <expr> => <expr>
