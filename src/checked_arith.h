/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * Signed/unsigned integer add/mul operations with overflow detection.
 */

static inline bool dy_sadd_overflow(int a, int b, int *c);
static inline bool dy_saddl_overflow(long a, long b, long *c);
static inline bool dy_saddll_overflow(long long a, long long b, long long *c);
static inline bool dy_uadd_overflow(unsigned a, unsigned b, unsigned *c);
static inline bool dy_uaddl_overflow(unsigned long a, unsigned long b, unsigned long *c);
static inline bool dy_uaddll_overflow(unsigned long long a, unsigned long long b, unsigned long long *c);

static inline bool dy_size_t_add_overflow(size_t a, size_t b, size_t *c);

static inline bool dy_intmax_t_add_overflow(intmax_t a, intmax_t b, intmax_t *c);

static inline bool dy_smul_overflow(int a, int b, int *c);
static inline bool dy_smull_overflow(long a, long b, long *c);
static inline bool dy_smulll_overflow(long long a, long long b, long long *c);
static inline bool dy_umul_overflow(unsigned a, unsigned b, unsigned *c);
static inline bool dy_umull_overflow(unsigned long a, unsigned long b, unsigned long *c);
static inline bool dy_umulll_overflow(unsigned long long a, unsigned long long b, unsigned long long *c);

static inline bool dy_size_t_mul_overflow(size_t a, size_t b, size_t *c);

static inline bool dy_intmax_t_mul_overflow(intmax_t a, intmax_t b, intmax_t *c);

static inline
bool dy_size_t_sub_overflow(size_t a, size_t b, size_t *c);
