/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

static inline
void dy_ref_appears_in_polarity(size_t ref, dy_core_expr_t expr, dy_polarity_t current_polarity, bool *positive, bool *negative);
