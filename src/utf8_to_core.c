/*
 * SPDX-FileCopyrightText: 2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <duality/utf8_to_core.h>

#include <duality/array.h>

#include "checked_arith.h"

#define DY_STR_LIT(x) ((uint8_t *)(x)), (((uint8_t *)(x)) + sizeof (x) - 1)

static inline
bool dy_utf8_bang_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t left, dy_core_expr_t *expr);

static inline
bool dy_get_var_index(uint8_t *const *vars_in_scope, const uint8_t *var, size_t *index);

static inline
bool dy_char_arrays_are_equal(const uint8_t *v1, const uint8_t *v2);

static inline
bool dy_char_array_is_equal_to_literal(const uint8_t *s1, const uint8_t *s2_begin, const uint8_t *s2_end);

static inline
bool dy_parse_first_var_char(dy_utf8_bytes_cb_t cb, uint8_t *b);

static inline
bool dy_parse_var_char(dy_utf8_bytes_cb_t cb, uint8_t *b);

static inline
uint8_t *dy_parse_name(dy_utf8_bytes_cb_t cb);

static inline
void dy_skip_whitespace(dy_utf8_bytes_cb_t cb);

static inline
void dy_skip_line_whitespace(dy_utf8_bytes_cb_t cb);

static inline
bool dy_expect(dy_utf8_bytes_cb_t cb, uint8_t b);

static inline
dy_core_expr_t dy_utf8_fun_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_polarity_t polarity);

static inline
dy_core_expr_t dy_utf8_inf_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_polarity_t polarity);

static inline
dy_core_expr_t dy_utf8_trap_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope);

static inline
dy_core_expr_t dy_utf8_comp_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope);

static inline
dy_core_expr_t dy_utf8_list_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_polarity_t polarity);

static inline
dy_core_expr_t dy_utf8_parse_var_further(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, uint8_t *var);

static inline
dy_core_expr_t dy_utf8_map_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope);

static inline
bool dy_utf8_parse_simple_prefix(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_simple_prefix_t *prefix);

typedef enum dy_complex_elim_tag {
    DY_COMPLEX_ELIM_VAR,
    DY_COMPLEX_ELIM_SOME,
    DY_COMPLEX_ELIM_EITHER,
    DY_COMPLEX_ELIM_FIN
} dy_complex_elim_tag_t;

static inline
bool dy_parse_complex_elim_tag(dy_utf8_bytes_cb_t cb, dy_complex_elim_tag_t *tag, uint8_t **var);

static inline
dy_core_expr_t dy_parse_elim_complex_some(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t left);

bool dy_utf8_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr)
{
    dy_core_expr_t left;
    if (!dy_utf8_non_left_recursive_to_core_expr(cb, vars_in_scope, &left)) {
        return false;
    }

    dy_skip_whitespace(cb);

    if (dy_utf8_bang_to_core_expr(cb, vars_in_scope, left, expr)) {
        return true;
    }

    *expr = left;
    return true;
}

bool dy_utf8_non_left_recursive_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr)
{
    if (dy_utf8_paren_to_core_expr(cb, vars_in_scope, expr)) {
        return true;
    }

    if (dy_utf8_var_to_core_expr(cb, vars_in_scope, expr)) {
        return true;
    }

    return false;
}

bool dy_utf8_paren_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr)
{
    if (!dy_expect(cb, '(')) {
        return false;
    }

    dy_skip_whitespace(cb);

    dy_core_expr_t inner_expr;
    if (!dy_utf8_to_core_expr(cb, vars_in_scope, &inner_expr)) {
        *expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_TRAP
        };

        return true;
    }

    dy_skip_whitespace(cb);

    if (!dy_expect(cb, ')')) {
        *expr = inner_expr;
        return true;
    }

    *expr = inner_expr;
    return true;
}

bool dy_utf8_var_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr)
{
    uint8_t *var = dy_array_create(sizeof *var, 4);

    uint8_t b;
    if (!dy_parse_first_var_char(cb, &b)) {
        return false;
    }

    dy_array_add(&var, &b);

    *expr = dy_utf8_parse_var_further(cb, vars_in_scope, var);
    return true;
}

dy_core_expr_t dy_utf8_parse_var_further(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, uint8_t *var)
{
    uint8_t b;
    if (dy_parse_var_char(cb, &b)) {
        dy_array_add(&var, &b);

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("fun"))) {
            dy_array_destroy(var);
            return dy_utf8_fun_to_core_expr(cb, vars_in_scope, DY_POLARITY_POSITIVE);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("some"))) {
            dy_array_destroy(var);
            return dy_utf8_fun_to_core_expr(cb, vars_in_scope, DY_POLARITY_NEGATIVE);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("list"))) {
            dy_array_destroy(var);
            return dy_utf8_list_to_core_expr(cb, vars_in_scope, DY_POLARITY_POSITIVE);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("either"))) {
            dy_array_destroy(var);
            return dy_utf8_list_to_core_expr(cb, vars_in_scope, DY_POLARITY_NEGATIVE);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("inf"))) {
            dy_array_destroy(var);
            return dy_utf8_inf_to_core_expr(cb, vars_in_scope, DY_POLARITY_POSITIVE);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("fin"))) {
            dy_array_destroy(var);
            return dy_utf8_inf_to_core_expr(cb, vars_in_scope, DY_POLARITY_NEGATIVE);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("comp"))) {
            dy_array_destroy(var);
            return dy_utf8_comp_to_core_expr(cb, vars_in_scope);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("trap"))) {
            dy_array_destroy(var);
            return dy_utf8_trap_to_core_expr(cb, vars_in_scope);
        }

        if (dy_char_array_is_equal_to_literal(var, DY_STR_LIT("map"))) {
            dy_array_destroy(var);
            return dy_utf8_map_to_core_expr(cb, vars_in_scope);
        }

        return dy_utf8_parse_var_further(cb, vars_in_scope, var);
    } else {
        size_t ref;
        if (!dy_get_var_index(*vars_in_scope, var, &ref)) {
            return (dy_core_expr_t){
                .tag = DY_CORE_EXPR_TRAP
            };
        }

        return (dy_core_expr_t){
            .tag = DY_CORE_EXPR_REF,
            .ref = ref
        };
    }
}

dy_core_expr_t dy_utf8_fun_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_polarity_t polarity)
{
    dy_core_expr_t function = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .is_implicit = false,
            .polarity = polarity,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_FUNCTION,
                .function = {
                    .type = dy_core_expr_new((dy_core_expr_t){
                        .tag = DY_CORE_EXPR_TRAP
                    }),
                    .sequent = dy_core_expr_new((dy_core_expr_t){
                        .tag = DY_CORE_EXPR_TRAP
                    })
                }
            }
        }
    };

    uint8_t b;
    if (dy_parse_var_char(cb, &b)) {
        uint8_t *var = dy_array_create(sizeof *var, 4);

        switch (polarity) {
        case DY_POLARITY_POSITIVE:
            dy_array_add(&var, &(uint8_t){ 'f' });
            dy_array_add(&var, &(uint8_t){ 'u' });
            dy_array_add(&var, &(uint8_t){ 'n' });
            break;
        case DY_POLARITY_NEGATIVE:
            dy_array_add(&var, &(uint8_t){ 's' });
            dy_array_add(&var, &(uint8_t){ 'o' });
            dy_array_add(&var, &(uint8_t){ 'm' });
            dy_array_add(&var, &(uint8_t){ 'e' });
            break;
        }

        dy_array_add(&var, &b);

        return dy_utf8_parse_var_further(cb, vars_in_scope, var);
    }

    dy_skip_whitespace(cb);

    if (dy_expect(cb, '@')) {
        function.intro.is_implicit = true;

        dy_skip_whitespace(cb);
    }

    uint8_t *name;
    if (dy_expect(cb, '_')) {
        name = NULL;
    } else {
        name = dy_parse_name(cb);
        if (name == NULL) {
            return function;
        }
    }

    dy_skip_whitespace(cb);

    bool needs_inference_ctx = false;
    if (dy_expect(cb, ':')) {
        dy_skip_whitespace(cb);

        if (dy_expect(cb, '_')) {
            // free
            function.intro.complex.function.type = NULL;
        } else {
            dy_core_expr_t type;
            if (dy_utf8_to_core_expr(cb, vars_in_scope, &type)) {
                // free
                function.intro.complex.function.type = dy_core_expr_new(type);
            }
        }
    } else {
        function.intro.complex.function.type = dy_core_expr_new((dy_core_expr_t){
            .tag = DY_CORE_EXPR_REF,
            .ref = 0
        });
        needs_inference_ctx = true;
    }

    if (!dy_expect(cb, '=') || !dy_expect(cb, '>')) {
        if (needs_inference_ctx) {
            function = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_INFERENCE_CTX,
                .inference_ctx = {
                    .num_dependent_ctxs = 0,
                    .expr = dy_core_expr_new(function)
                }
            };
        }

        return function;
    }

    dy_skip_whitespace(cb);

    dy_array_add(vars_in_scope, &name);

    dy_core_expr_t sequent;
    if (dy_utf8_to_core_expr(cb, vars_in_scope, &sequent)) {
        // free
        function.intro.complex.function.sequent = dy_core_expr_new(sequent);
    }

    dy_array_remove_last(*vars_in_scope);

    if (needs_inference_ctx) {
        function = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INFERENCE_CTX,
            .inference_ctx = {
                .num_dependent_ctxs = 0,
                .expr = dy_core_expr_new(function)
            }
        };
    }

    return function;
}

dy_core_expr_t dy_utf8_inf_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_polarity_t polarity)
{
    dy_core_expr_t inf = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .is_implicit = false,
            .polarity = polarity,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = {
                    .sequent = dy_core_expr_new((dy_core_expr_t){
                        .tag = DY_CORE_EXPR_TRAP
                    })
                }
            }
        }
    };

    uint8_t b;
    if (dy_parse_var_char(cb, &b)) {
        uint8_t *var = dy_array_create(sizeof *var, 4);

        switch (polarity) {
        case DY_POLARITY_POSITIVE:
            dy_array_add(&var, &(uint8_t){ 'i' });
            dy_array_add(&var, &(uint8_t){ 'n' });
            dy_array_add(&var, &(uint8_t){ 'f' });
            break;
        case DY_POLARITY_NEGATIVE:
            dy_array_add(&var, &(uint8_t){ 'f' });
            dy_array_add(&var, &(uint8_t){ 'i' });
            dy_array_add(&var, &(uint8_t){ 'n' });
            break;
        }

        dy_array_add(&var, &b);

        return dy_utf8_parse_var_further(cb, vars_in_scope, var);
    }

    dy_skip_whitespace(cb);

    if (dy_expect(cb, '@')) {
        inf.intro.is_implicit = true;

        dy_skip_whitespace(cb);
    }

    uint8_t *name;
    if (dy_expect(cb, '_')) {
        name = NULL;
    } else {
        name = dy_parse_name(cb);
        if (name == NULL) {
            return inf;
        }
    }

    dy_skip_whitespace(cb);

    if (!dy_expect(cb, '=')) {
        return inf;
    }

    dy_skip_whitespace(cb);

    dy_array_add(vars_in_scope, &name);

    dy_core_expr_t sequent;
    if (dy_utf8_to_core_expr(cb, vars_in_scope, &sequent)) {
        // free
        inf.intro.complex.function.sequent = dy_core_expr_new(sequent);
    }

    dy_array_remove_last(*vars_in_scope);

    return inf;
}

dy_core_expr_t dy_utf8_trap_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope)
{
    uint8_t b;
    if (dy_parse_var_char(cb, &b)) {
        uint8_t *var = dy_array_create(sizeof *var, 4);
        dy_array_add(&var, &(uint8_t){ 't' });
        dy_array_add(&var, &(uint8_t){ 'r' });
        dy_array_add(&var, &(uint8_t){ 'a' });
        dy_array_add(&var, &(uint8_t){ 'p' });
        dy_array_add(&var, &b);

        return dy_utf8_parse_var_further(cb, vars_in_scope, var);
    } else {
        return (dy_core_expr_t){
            .tag = DY_CORE_EXPR_TRAP
        };
    }
}

dy_core_expr_t dy_utf8_comp_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope)
{
    uint8_t b;
    if (dy_parse_var_char(cb, &b)) {
        uint8_t *var = dy_array_create(sizeof *var, 4);
        dy_array_add(&var, &(uint8_t){ 'c' });
        dy_array_add(&var, &(uint8_t){ 'o' });
        dy_array_add(&var, &(uint8_t){ 'm' });
        dy_array_add(&var, &(uint8_t){ 'p' });
        dy_array_add(&var, &b);

        return dy_utf8_parse_var_further(cb, vars_in_scope, var);
    }

    dy_skip_whitespace(cb);

    dy_core_expr_t expr;
    if (dy_utf8_to_core_expr(cb, vars_in_scope, &expr)) {
        return (dy_core_expr_t){
            .tag = DY_CORE_EXPR_COMP,
            .comp = {
                .expr = dy_core_expr_new(expr)
            }
        };
    } else {
        return (dy_core_expr_t){
            .tag = DY_CORE_EXPR_COMP,
            .comp = {
                .expr = dy_core_expr_new((dy_core_expr_t){
                    .tag = DY_CORE_EXPR_TRAP
                })
            }
        };
    }
}

dy_core_expr_t dy_utf8_list_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_polarity_t polarity)
{
    dy_core_expr_t list = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .is_implicit = false,
            .polarity = polarity,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_TUPLE,
                .tuple = {
                    .sequents = dy_array_create(sizeof *list.intro.complex.tuple.sequents, 2)
                }
            }
        }
    };

    uint8_t b;
    if (dy_parse_var_char(cb, &b)) {
        uint8_t *var = dy_array_create(sizeof *var, 4);

        switch (polarity) {
        case DY_POLARITY_POSITIVE:
            dy_array_add(&var, &(uint8_t){ 'l' });
            dy_array_add(&var, &(uint8_t){ 'i' });
            dy_array_add(&var, &(uint8_t){ 's' });
            dy_array_add(&var, &(uint8_t){ 't' });
            break;
        case DY_POLARITY_NEGATIVE:
            dy_array_add(&var, &(uint8_t){ 'e' });
            dy_array_add(&var, &(uint8_t){ 'i' });
            dy_array_add(&var, &(uint8_t){ 't' });
            dy_array_add(&var, &(uint8_t){ 'h' });
            dy_array_add(&var, &(uint8_t){ 'e' });
            dy_array_add(&var, &(uint8_t){ 'r' });
            break;
        }

        dy_array_add(&var, &b);

        return dy_utf8_parse_var_further(cb, vars_in_scope, var);
    }

    dy_skip_whitespace(cb);

    if (dy_expect(cb, '@')) {
        list.intro.is_implicit = true;

        dy_skip_whitespace(cb);
    }

    if (!dy_expect(cb, '{')) {
        return list;
    }

    for (;;) {
        dy_skip_whitespace(cb);

        dy_core_expr_t expr;
        if (!dy_utf8_to_core_expr(cb, vars_in_scope, &expr)) {
            expr = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_TRAP
            };
        }

        dy_array_add(&list.intro.complex.tuple.sequents, &expr);

        dy_skip_line_whitespace(cb);

        if (dy_expect(cb, '}')) {
            return list;
        }

        if (!dy_expect(cb, ',')) {
            if (dy_expect(cb, '\n')) {
                dy_skip_whitespace(cb);

                if (dy_expect(cb, '}')) {
                    return list;
                }
            } else {
                return list;
            }
        }
    }
}

dy_core_expr_t dy_utf8_map_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope)
{
    dy_core_expr_t map = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = false,
            .tag = DY_INTRO_SIMPLE,
            .simple = {
                .sequent = dy_core_expr_new((dy_core_expr_t){
                    .tag = DY_CORE_EXPR_TRAP
                })
            }
        }
    };

    uint8_t b;
    if (dy_parse_var_char(cb, &b)) {
        uint8_t *var = dy_array_create(sizeof *var, 4);
        dy_array_add(&var, &(uint8_t){ 'm' });
        dy_array_add(&var, &(uint8_t){ 'a' });
        dy_array_add(&var, &(uint8_t){ 'p' });
        dy_array_add(&var, &b);

        return dy_utf8_parse_var_further(cb, vars_in_scope, var);
    }

    dy_skip_whitespace(cb);

    if (dy_expect(cb, '@')) {
        map.intro.is_implicit = true;

        dy_skip_whitespace(cb);
    }

    if (!dy_utf8_parse_simple_prefix(cb, vars_in_scope, &map.intro.simple.prefix)) {
        return (dy_core_expr_t){
            .tag = DY_CORE_EXPR_TRAP
        };
    }

    dy_skip_whitespace(cb);

    if (!dy_expect(cb, '-')) {
        if (dy_expect(cb, '~')) {
            map.intro.polarity = DY_POLARITY_NEGATIVE;
        } else {
            map.intro.simple.sequent = dy_core_expr_new((dy_core_expr_t){ .tag = DY_CORE_EXPR_TRAP });
            return map;
        }
    }

    if (!dy_expect(cb, '>')) {
        map.intro.simple.sequent = dy_core_expr_new((dy_core_expr_t){ .tag = DY_CORE_EXPR_TRAP });
        return map;
    }

    dy_skip_whitespace(cb);

    dy_core_expr_t expr;
    if (dy_utf8_to_core_expr(cb, vars_in_scope, &expr)) {
        map.intro.simple.sequent = dy_core_expr_new(expr);
    }

    return map;
}

bool dy_utf8_parse_simple_prefix(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_simple_prefix_t *prefix)
{
    if (dy_expect(cb, '0')) {
        prefix->tag = DY_CORE_UNFOLDING;
        return true;
    }

    unsigned char b;
    if (cb.byte(cb.env, &b) && ('0' <= b && b <= '9')) {
        prefix->tag = DY_CORE_SELECTION;
        prefix->index = b - '0';

        cb.consume(cb.env);

        for (;;) {
            if (!cb.byte(cb.env, &b) || !('0' <= b && b <= '9')) {
                break;
            }

            if (dy_size_t_mul_overflow(prefix->index, 10, &prefix->index)) {
                break;
            }

            if (dy_size_t_add_overflow(prefix->index, b - '0', &prefix->index)) {
                break;
            }

            cb.consume(cb.env);
        }

        return true;
    }

    dy_core_expr_t expr;
    if (dy_utf8_to_core_expr(cb, vars_in_scope, &expr)) {
        prefix->tag = DY_CORE_MAPPING;
        prefix->expr = dy_core_expr_new(expr);
        return true;
    }

    return false;
}

uint8_t *dy_parse_name(dy_utf8_bytes_cb_t cb)
{
    uint8_t *name = dy_array_create(sizeof *name, 4);

    uint8_t b;
    if (!dy_parse_first_var_char(cb, &b)) {
        return NULL;
    }

    dy_array_add(&name, &b);

    for (;;) {
        if (!dy_parse_var_char(cb, &b)) {
            break;
        }

        dy_array_add(&name, &b);
    }

    return name;
}

void dy_skip_whitespace(dy_utf8_bytes_cb_t cb)
{
    for (;;) {
        uint8_t b;
        if (!cb.byte(cb.env, &b)) {
            return;
        }

        if (b != ' ' && b != '\t' && b != '\n') {
            break;
        }

        cb.consume(cb.env);
    }
}

void dy_skip_line_whitespace(dy_utf8_bytes_cb_t cb)
{
    for (;;) {
        uint8_t b;
        if (!cb.byte(cb.env, &b)) {
            return;
        }

        if (b != ' ' && b != '\t') {
            break;
        }

        cb.consume(cb.env);
    }
}

bool dy_expect(dy_utf8_bytes_cb_t cb, uint8_t wanted)
{
    uint8_t b;
    if (!cb.byte(cb.env, &b)) {
        return false;
    }

    if (b != wanted) {
        return false;
    }

    cb.consume(cb.env);
    return true;
}

bool dy_utf8_bang_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t left, dy_core_expr_t *expr)
{
    if (!dy_expect(cb, '!')) {
        return false;
    }

    if (dy_expect(cb, '!')) {
        dy_skip_whitespace(cb);

        uint8_t *var;
        dy_complex_elim_tag_t tag;
        if (!dy_parse_complex_elim_tag(cb, &tag, &var)) {
            *expr = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_TRAP
            };
            return true;
        }

        switch (tag) {
        case DY_COMPLEX_ELIM_VAR: {
            size_t index;
            if (!dy_get_var_index(*vars_in_scope, var, &index)) {
                *expr = (dy_core_expr_t){
                    .tag = DY_CORE_EXPR_TRAP
                };
                return true;
            }

            *expr = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_ELIM_REF,
                .elim_ref = {
                    .expr = dy_core_expr_new(left),
                    .index = index,
                    .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                }
            };
            return true;
        }
        case DY_COMPLEX_ELIM_SOME:
            *expr = dy_parse_elim_complex_some(cb, vars_in_scope, left);
            return true;
        case DY_COMPLEX_ELIM_EITHER:
        case DY_COMPLEX_ELIM_FIN:
        }
    } else {
        dy_skip_whitespace(cb);

        bool is_implicit = dy_expect(cb, '@');

        if (is_implicit) {
            dy_skip_whitespace(cb);
        }

        dy_core_simple_prefix_t prefix;
        if (!dy_utf8_parse_simple_prefix(cb, vars_in_scope, &prefix)) {
            *expr = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_TRAP
            };

            return true;
        }

        dy_skip_whitespace(cb);

        if (!dy_expect(cb, ':')) {
            *expr = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_INFERENCE_CTX,
                .inference_ctx = {
                    .num_dependent_ctxs = 0,
                    .expr = dy_core_expr_new((dy_core_expr_t){
                        .tag = DY_CORE_EXPR_ELIM,
                        .elim = {
                            .expr = dy_core_expr_new(left),
                            .tag = DY_INTRO_SIMPLE,
                            .simple_prefix = prefix,
                            .is_implicit = is_implicit,
                            .result_type = dy_core_expr_new((dy_core_expr_t){
                                .tag = DY_CORE_EXPR_REF,
                                .ref = 0
                            }),
                            .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                        }
                    })
                }
            };

            return true;
        }

        dy_skip_whitespace(cb);

        dy_core_expr_t type;
        if (!dy_utf8_to_core_expr(cb, vars_in_scope, &type)) {
            type = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_TRAP
            };
        }

        *expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .expr = dy_core_expr_new(left),
                .tag = DY_INTRO_SIMPLE,
                .simple_prefix = prefix,
                .is_implicit = is_implicit,
                .result_type = dy_core_expr_new(type),
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        };

        return true;
    }
}

bool dy_parse_complex_elim_tag(dy_utf8_bytes_cb_t cb, dy_complex_elim_tag_t *tag, uint8_t **var)
{
    unsigned char b;
    if (!dy_parse_first_var_char(cb, &b)) {
        return false;
    }

    unsigned char *array = dy_array_create(sizeof *array, 4);
    dy_array_add(&array, &b);

    for (;;) {
        if (!dy_parse_var_char(cb, &b)) {
            if (dy_char_array_is_equal_to_literal(array, DY_STR_LIT("some"))) {
                *tag = DY_COMPLEX_ELIM_SOME;
                return true;
            }

            if (dy_char_array_is_equal_to_literal(array, DY_STR_LIT("either"))) {
                *tag = DY_COMPLEX_ELIM_EITHER;
                return true;
            }

            if (dy_char_array_is_equal_to_literal(array, DY_STR_LIT("fin"))) {
                *tag = DY_COMPLEX_ELIM_FIN;
                return true;
            }

            *tag = DY_COMPLEX_ELIM_VAR;
            *var = array;
            return true;
        }
    }
}

dy_core_expr_t dy_parse_elim_complex_some(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t left)
{
    dy_core_expr_t some = {
        .tag = DY_CORE_EXPR_ELIM,
        .elim = {
            .expr = dy_core_expr_new(left),
            .is_implicit = false,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_FUNCTION,
                .function = {
                    .type = dy_core_expr_new((dy_core_expr_t){ .tag = DY_CORE_EXPR_TRAP }),
                    .cont = {
                        .type = dy_core_expr_new((dy_core_expr_t){ .tag = DY_CORE_EXPR_TRAP }),
                        .sequent = dy_core_expr_new((dy_core_expr_t){ .tag = DY_CORE_EXPR_TRAP })
                    },
                    .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                }
            }
        }
    };

    dy_skip_whitespace(cb);

    if (dy_expect(cb, '@')) {
        some.elim.is_implicit = true;

        dy_skip_whitespace(cb);
    }

    uint8_t *var = NULL;
    if (!dy_expect(cb, '_')) {
        var = dy_parse_name(cb);
        if (var == NULL) {
            return some;
        }
    }

    dy_skip_whitespace(cb);

    bool inference_ctx;
    dy_core_expr_t type;
    if (dy_expect(cb, ':')) {
        inference_ctx = false;

        dy_skip_whitespace(cb);

        if (!dy_utf8_to_core_expr(cb, vars_in_scope, &type)) {
            return some;
        }

        dy_skip_whitespace(cb);
    } else {
        inference_ctx = true;

        type = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_REF,
            .ref = 0
        };
    }

    if (!dy_expect(cb, '=') || !dy_expect(cb, '>')) {
        if (inference_ctx) {
            some = (dy_core_expr_t){
                .inference_ctx = {
                    .num_dependent_ctxs = 0,
                    .expr = dy_core_expr_new(some)
                }
            };
        }

        return some;
    }

    dy_skip_whitespace(cb);


}

bool dy_get_var_index(uint8_t *const *vars_in_scope, const uint8_t *var, size_t *index)
{
    uint8_t *const *end = (uint8_t *const *)dy_array_past_end(vars_in_scope);

    for (uint8_t * const *p = end; p-- != vars_in_scope;) {
        if (p == NULL) {
            continue;
        }

        if (dy_char_arrays_are_equal(*p, var)) {
            *index = (size_t)(end - p) - 1;
            return true;
        }
    }

    return false;
}

bool dy_char_arrays_are_equal(const uint8_t *v1, const uint8_t *v2)
{
    const uint8_t *v1_end = dy_array_past_end(v1);
    const uint8_t *v2_end = dy_array_past_end(v2);

    size_t v1_size = (size_t)(v1_end - v1);
    size_t v2_size = (size_t)(v2_end - v2);

    if (v1_size != v2_size) {
        return false;
    }

    for (size_t i = 0; i < v1_size; ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }

    return true;
}

bool dy_char_array_is_equal_to_literal(const uint8_t *s1, const uint8_t *s2_begin, const uint8_t *s2_end)
{
    const uint8_t *s1_end = dy_array_past_end(s1);

    size_t s1_size = (size_t)(s1_end - s1);
    size_t s2_size = (size_t)(s2_end - s2_begin);

    if (s1_size != s2_size) {
        return false;
    }

    for (size_t i = 0; i < s1_size; ++i) {
        if (s1[i] != s2_begin[i]) {
            return false;
        }
    }

    return true;
}

bool dy_parse_var_char(dy_utf8_bytes_cb_t cb, uint8_t *b)
{
    uint8_t local_b;
    if (!cb.byte(cb.env, &local_b)) {
        return false;
    }

    if (('a' <= local_b && local_b <= 'z') || ('0' <= local_b && local_b <= '9') || (local_b == '-')) {
        *b = local_b;
        cb.consume(cb.env);
        return true;
    }

    return false;
}

bool dy_parse_first_var_char(dy_utf8_bytes_cb_t cb, uint8_t *b)
{
    uint8_t local_b;
    if (!cb.byte(cb.env, &local_b)) {
        return false;
    }

    if ('a' <= local_b && local_b <= 'z') {
        *b = local_b;
        cb.consume(cb.env);
        return true;
    }

    return false;
}
