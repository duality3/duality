/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>
#include <duality/constraint.h>

typedef enum dy_subtype_res {
    DY_SUBTYPE_RES_OK,
    DY_SUBTYPE_RES_FAIL,
    DY_SUBTYPE_RES_INFERENCE
} dy_subtype_res_t;

typedef struct dy_sequence_step {
    dy_core_expr_t expr;
    dy_core_expr_t type;
} dy_sequence_step_t;

static inline
dy_subtype_res_t dy_exprs_are_subtypes_and_transform(
    dy_binder_t **binders,
    dy_constraint_t **constraints,

    dy_core_expr_t subtype,
    dy_core_expr_t supertype,

    dy_core_expr_t *subtype_expr,
    dy_core_expr_t *enclosing_comp_expr
);

static inline
dy_subtype_res_t dy_exprs_are_subtypes(
    dy_binder_t **binders,
    dy_constraint_t **constraints,
    dy_sequence_step_t **sequence_steps,

    dy_core_expr_t subtype,
    dy_core_expr_t supertype,

    dy_core_expr_t subtype_expr,
    dy_core_expr_t *new_subtype_expr,
    bool *have_new_subtype_expr
);
