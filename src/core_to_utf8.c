/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <duality/core_to_utf8.h>

#include <duality/array.h>

static inline
void dy_size_t_to_utf8(size_t x, uint8_t base, uint8_t **utf8);

void dy_core_expr_to_utf8(dy_core_expr_t expr, uint8_t **utf8)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        switch (expr.intro.tag) {
        case DY_INTRO_COMPLEX:
            switch (expr.intro.complex.tag) {
            case DY_CORE_FUNCTION:
                switch (expr.intro.polarity) {
                case DY_POLARITY_POSITIVE:
                    dy_array_add(utf8, &(uint8_t){ 'f' });
                    dy_array_add(utf8, &(uint8_t){ 'u' });
                    dy_array_add(utf8, &(uint8_t){ 'n' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                    break;
                case DY_POLARITY_NEGATIVE:
                    dy_array_add(utf8, &(uint8_t){ 's' });
                    dy_array_add(utf8, &(uint8_t){ 'o' });
                    dy_array_add(utf8, &(uint8_t){ 'm' });
                    dy_array_add(utf8, &(uint8_t){ 'e' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                    break;
                }

                if (expr.intro.is_implicit) {
                    dy_array_add(utf8, &(uint8_t){ '@' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                }

                if (expr.intro.complex.function.type == NULL) {
                    dy_array_add(utf8, &(uint8_t){ '_' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                } else {
                    dy_core_expr_to_utf8(*expr.intro.complex.function.type, utf8);
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                }

                dy_array_add(utf8, &(uint8_t){ '=' });
                dy_array_add(utf8, &(uint8_t){ '>' });
                dy_array_add(utf8, &(uint8_t){ ' ' });

                dy_core_expr_to_utf8(*expr.intro.complex.function.sequent, utf8);
                return;
            case DY_CORE_TUPLE:
                switch (expr.intro.polarity) {
                case DY_POLARITY_POSITIVE:
                    dy_array_add(utf8, &(uint8_t){ 'l' });
                    dy_array_add(utf8, &(uint8_t){ 'i' });
                    dy_array_add(utf8, &(uint8_t){ 's' });
                    dy_array_add(utf8, &(uint8_t){ 't' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                    break;
                case DY_POLARITY_NEGATIVE:
                    dy_array_add(utf8, &(uint8_t){ 'e' });
                    dy_array_add(utf8, &(uint8_t){ 'i' });
                    dy_array_add(utf8, &(uint8_t){ 't' });
                    dy_array_add(utf8, &(uint8_t){ 'h' });
                    dy_array_add(utf8, &(uint8_t){ 'e' });
                    dy_array_add(utf8, &(uint8_t){ 'r' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                    break;
                }

                if (expr.intro.is_implicit) {
                    dy_array_add(utf8, &(uint8_t){ '@' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                }

                dy_array_add(utf8, &(uint8_t){ '{' });
                dy_array_add(utf8, &(uint8_t){ ' ' });

                for (const dy_core_expr_t *e = expr.intro.complex.tuple.sequents, *end = dy_array_past_end(e); e != end; ++e) {
                    dy_core_expr_to_utf8(*e, utf8);
                    if (e == end - 1) {
                        dy_array_add(utf8, &(uint8_t){ ' ' });
                        dy_array_add(utf8, &(uint8_t){ '}' });
                    } else {
                        dy_array_add(utf8, &(uint8_t){ ',' });
                        dy_array_add(utf8, &(uint8_t){ ' ' });
                    }
                }

                return;
            case DY_CORE_RECURSION:
                switch (expr.intro.polarity) {
                case DY_POLARITY_POSITIVE:
                    dy_array_add(utf8, &(uint8_t){ 'i' });
                    dy_array_add(utf8, &(uint8_t){ 'n' });
                    dy_array_add(utf8, &(uint8_t){ 'f' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                    break;
                case DY_POLARITY_NEGATIVE:
                    dy_array_add(utf8, &(uint8_t){ 'f' });
                    dy_array_add(utf8, &(uint8_t){ 'i' });
                    dy_array_add(utf8, &(uint8_t){ 'n' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                    break;
                }

                if (expr.intro.is_implicit) {
                    dy_array_add(utf8, &(uint8_t){ '@' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                }

                dy_core_expr_to_utf8(*expr.intro.complex.recursion.sequent, utf8);

                return;
            }

            return;
        case DY_INTRO_SIMPLE:
            switch (expr.intro.simple.prefix.tag) {
            case DY_CORE_MAPPING:
                if (expr.intro.simple.prefix.is_computationally_irrelevant) {
                    dy_array_add(utf8, &(uint8_t){ '[' });
                } else {
                    dy_array_add(utf8, &(uint8_t){ '(' });
                }
                dy_core_expr_to_utf8(*expr.intro.simple.prefix.expr, utf8);
                if (expr.intro.simple.prefix.is_computationally_irrelevant) {
                    dy_array_add(utf8, &(uint8_t){ ']' });
                } else {
                    dy_array_add(utf8, &(uint8_t){ ')' });
                }

                dy_array_add(utf8, &(uint8_t){ ' ' });

                if (expr.intro.is_implicit) {
                    dy_array_add(utf8, &(uint8_t){ '@' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                }

                switch (expr.intro.polarity) {
                case DY_POLARITY_POSITIVE:
                    dy_array_add(utf8, &(uint8_t){ '-' });
                    break;
                case DY_POLARITY_NEGATIVE:
                    dy_array_add(utf8, &(uint8_t){ '~' });
                    break;
                }

                dy_array_add(utf8, &(uint8_t){ '>' });
                dy_array_add(utf8, &(uint8_t){ ' ' });

                dy_core_expr_to_utf8(*expr.intro.simple.sequent, utf8);

                return;
            case DY_CORE_SELECTION:
                dy_size_t_to_utf8(expr.intro.simple.prefix.index, 10, utf8);

                dy_array_add(utf8, &(uint8_t){ ' ' });

                if (expr.intro.is_implicit) {
                    dy_array_add(utf8, &(uint8_t){ '@' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                }

                switch (expr.intro.polarity) {
                case DY_POLARITY_POSITIVE:
                    dy_array_add(utf8, &(uint8_t){ '-' });
                    break;
                case DY_POLARITY_NEGATIVE:
                    dy_array_add(utf8, &(uint8_t){ '~' });
                    break;
                }

                dy_array_add(utf8, &(uint8_t){ '>' });
                dy_array_add(utf8, &(uint8_t){ ' ' });

                dy_core_expr_to_utf8(*expr.intro.simple.sequent, utf8);

                return;
            case DY_CORE_UNFOLDING:
                dy_size_t_to_utf8(0, 10, utf8);

                dy_array_add(utf8, &(uint8_t){ ' ' });

                if (expr.intro.is_implicit) {
                    dy_array_add(utf8, &(uint8_t){ '@' });
                    dy_array_add(utf8, &(uint8_t){ ' ' });
                }

                switch (expr.intro.polarity) {
                case DY_POLARITY_POSITIVE:
                    dy_array_add(utf8, &(uint8_t){ '-' });
                    break;
                case DY_POLARITY_NEGATIVE:
                    dy_array_add(utf8, &(uint8_t){ '~' });
                    break;
                }

                dy_array_add(utf8, &(uint8_t){ '>' });
                dy_array_add(utf8, &(uint8_t){ ' ' });

                dy_core_expr_to_utf8(*expr.intro.simple.sequent, utf8);

                return;
            }
            return;
        }

        return;
    case DY_CORE_EXPR_ELIM:
        dy_array_add(utf8, &(uint8_t){ '(' });
        dy_core_expr_to_utf8(*expr.elim.expr, utf8);
        dy_array_add(utf8, &(uint8_t){ ')' });

        dy_array_add(utf8, &(uint8_t){ ' ' });
        dy_array_add(utf8, &(uint8_t){ '!' });

        switch (expr.elim.tag) {
        case DY_INTRO_SIMPLE:
            dy_array_add(utf8, &(uint8_t){ ' ' });

            if (expr.elim.is_implicit) {
                dy_array_add(utf8, &(uint8_t){ '@' });
                dy_array_add(utf8, &(uint8_t){ ' ' });
            }

            switch (expr.elim.simple_prefix.tag) {
            case DY_CORE_MAPPING:
                dy_core_expr_to_utf8(*expr.elim.simple_prefix.expr, utf8);
                break;
            case DY_CORE_SELECTION:
                dy_size_t_to_utf8(expr.elim.simple_prefix.index, 10, utf8);
                break;
            case DY_CORE_UNFOLDING:
                dy_array_add(utf8, &(uint8_t){ '0' });
                break;
            }

            break;
        case DY_INTRO_COMPLEX:
            dy_array_add(utf8, &(uint8_t){ '!' });
            dy_array_add(utf8, &(uint8_t){ ' ' });
            break;
        }

        dy_array_add(utf8, &(uint8_t){ ' ' });
        dy_array_add(utf8, &(uint8_t){ ':' });
        dy_array_add(utf8, &(uint8_t){ ' ' });

        dy_core_expr_to_utf8(*expr.elim.result_type, utf8);

        return;
    case DY_CORE_EXPR_REF:
        dy_size_t_to_utf8(expr.ref, 10, utf8);
        return;
    case DY_CORE_EXPR_ELIM_REF:
    case DY_CORE_EXPR_SEQUENCE:
        return;
    case DY_CORE_EXPR_COMP:
        dy_array_add(utf8, &(uint8_t){ 'c' });
        dy_array_add(utf8, &(uint8_t){ 'o' });
        dy_array_add(utf8, &(uint8_t){ 'm' });
        dy_array_add(utf8, &(uint8_t){ 'p' });
        dy_array_add(utf8, &(uint8_t){ ' ' });
        dy_core_expr_to_utf8(*expr.comp.expr, utf8);
        return;
    case DY_CORE_EXPR_TRAP:
        dy_core_trap_to_utf8(utf8);
        return;
    case DY_CORE_EXPR_INFERENCE_CTX:
        dy_array_add(utf8, &(uint8_t){ '?' });
        dy_array_add(utf8, &(uint8_t){ ' ' });
        dy_core_expr_to_utf8(*expr.inference_ctx.expr, utf8);
        return;
    }
}

void dy_core_trap_to_utf8(uint8_t **utf8)
{
    dy_array_add(utf8, &(uint8_t){ 't' });
    dy_array_add(utf8, &(uint8_t){ 'r' });
    dy_array_add(utf8, &(uint8_t){ 'a' });
    dy_array_add(utf8, &(uint8_t){ 'p' });
}

void dy_size_t_to_utf8(size_t x, uint8_t base, uint8_t **utf8)
{
    uint8_t *p1 = (uint8_t *)dy_array_last(*utf8) + 1;

    do {
        uint8_t digit = x % base;
        dy_array_add(utf8, &(uint8_t){  '0' + digit });
        x /= 10;
    } while (x != 0);

    uint8_t *p2 = dy_array_last(*utf8);

    while (p1 < p2) {
        uint8_t temp = *p1;
        *p1 = *p2;
        *p2 = temp;

        ++p1;
        --p2;
    }
}
