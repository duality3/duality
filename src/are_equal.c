/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "are_equal.h"

#include <duality/array.h>

bool dy_exprs_are_equal(dy_core_expr_t e1, dy_core_expr_t e2)
{
    if (e1.tag != e2.tag) {
        return false;
    }

    switch (e1.tag) {
    case DY_CORE_EXPR_INTRO:
        return dy_intros_are_equal(e1.intro, e2.intro);
    case DY_CORE_EXPR_ELIM:
        return dy_elims_are_equal(e1.elim, e2.elim);
    case DY_CORE_EXPR_REF:
        return e1.ref == e2.ref;
    case DY_CORE_EXPR_ELIM_REF:
        return dy_elim_refs_are_equal(e1.elim_ref, e2.elim_ref);
    case DY_CORE_EXPR_COMP:
        return dy_exprs_are_equal(*e1.comp.expr, *e2.comp.expr);
    case DY_CORE_EXPR_SEQUENCE:
        return dy_composes_are_equal(e1.sequence, e2.sequence);
    case DY_CORE_EXPR_TRAP:
        return true;
    case DY_CORE_EXPR_INFERENCE_CTX:
        return dy_exprs_are_equal(*e1.inference_ctx.expr, *e2.inference_ctx.expr);
    }
}

bool dy_intros_are_equal(dy_core_intro_t intro1, dy_core_intro_t intro2)
{
    if (intro1.tag != intro2.tag) {
        return false;
    }

    if (intro1.is_implicit != intro2.is_implicit) {
        return false;
    }

    if (intro1.polarity != intro2.polarity) {
        return false;
    }

    switch (intro1.tag) {
    case DY_INTRO_COMPLEX:
        return dy_complexes_are_equal(intro1.complex, intro2.complex);
    case DY_INTRO_SIMPLE:
        return dy_simples_are_equal(intro1.simple, intro2.simple);
    }
}

bool dy_complexes_are_equal(dy_core_complex_t complex1, dy_core_complex_t complex2)
{
    if (complex1.tag != complex2.tag) {
        return false;
    }

    switch (complex1.tag) {
    case DY_CORE_FUNCTION:
        return dy_functions_are_equal(complex1.function, complex2.function);
    case DY_CORE_TUPLE:
        return dy_pairs_are_equal(complex1.tuple, complex2.tuple);
    case DY_CORE_RECURSION:
        return dy_recursions_are_equal(complex1.recursion, complex2.recursion);
    }
}

bool dy_functions_are_equal(dy_core_function_t fun1, dy_core_function_t fun2)
{
    return dy_exprs_are_equal(*fun1.sequent, *fun2.sequent);
}

bool dy_pairs_are_equal(dy_core_tuple_t pair1, dy_core_tuple_t pair2)
{
    const dy_core_expr_t *end1 = dy_array_past_end(pair1.sequents);
    const dy_core_expr_t *end2 = dy_array_past_end(pair2.sequents);

    size_t size1 = (size_t)(end1 - pair1.sequents);
    size_t size2 = (size_t)(end2 - pair2.sequents);

    if (size1 != size2) {
        return false;
    }

    for (size_t i = 0; i < size1; ++i) {
        if (!dy_exprs_are_equal(pair1.sequents[i], pair2.sequents[i])) {
            return false;
        }
    }

    return true;
}

bool dy_recursions_are_equal(dy_core_recursion_t rec1, dy_core_recursion_t rec2)
{
    return dy_exprs_are_equal(*rec1.sequent, *rec2.sequent);
}

static inline
bool dy_simple_prefixes_are_equal(dy_core_simple_prefix_t prefix1, dy_core_simple_prefix_t prefix2)
{
    if (prefix1.tag != prefix2.tag) {
        return false;
    }

    switch (prefix1.tag) {
    case DY_CORE_MAPPING:
        return prefix1.is_computationally_irrelevant == prefix2.is_computationally_irrelevant && dy_exprs_are_equal(*prefix1.expr, *prefix2.expr);
    case DY_CORE_SELECTION:
        return prefix1.index == prefix2.index;
    case DY_CORE_UNFOLDING:
        return true;
    }
}

bool dy_simples_are_equal(dy_core_simple_t simple1, dy_core_simple_t simple2)
{
    return dy_simple_prefixes_are_equal(simple1.prefix, simple2.prefix) && dy_exprs_are_equal(*simple1.sequent, *simple2.sequent);
}

bool dy_elims_are_equal(dy_core_elim_t elim1, dy_core_elim_t elim2)
{
    if (!dy_exprs_are_equal(*elim1.expr, *elim2.expr)) {
        return false;
    }

    if (elim1.tag != elim2.tag) {
        return false;
    }

    switch (elim1.tag) {
    case DY_INTRO_COMPLEX:
        return dy_complex_eliminators_are_equal(elim1.complex, elim2.complex);
    case DY_INTRO_SIMPLE:
        return dy_simple_prefixes_are_equal(elim1.simple_prefix, elim2.simple_prefix);
    }
}

bool dy_complex_eliminators_are_equal(dy_core_complex_eliminator_t complex1, dy_core_complex_eliminator_t complex2)
{
    if (complex1.tag != complex2.tag) {
        return false;
    }

    switch (complex1.tag) {
    case DY_CORE_FUNCTION:
        return dy_function_eliminators_are_equal(complex1.function, complex2.function);
    case DY_CORE_TUPLE:
        return dy_pair_eliminators_are_equal(complex1.tuple, complex2.tuple);
    case DY_CORE_RECURSION:
        return dy_recursion_eliminators_are_equal(complex1.recursion, complex2.recursion);
    }
}

bool dy_function_eliminators_are_equal(dy_core_function_eliminator_t fun1, dy_core_function_eliminator_t fun2)
{
    return dy_functions_are_equal(fun1.cont, fun2.cont);
}

bool dy_pair_eliminators_are_equal(dy_core_tuple_eliminator_t pair1, dy_core_tuple_eliminator_t pair2)
{
    const dy_core_cont_with_check_status_t *end1 = dy_array_past_end(pair1.conts_with_check_status);
    const dy_core_cont_with_check_status_t *end2 = dy_array_past_end(pair2.conts_with_check_status);

    size_t size1 = (size_t)(end1 - pair1.conts_with_check_status);
    size_t size2 = (size_t)(end2 - pair2.conts_with_check_status);

    if (size1 != size2) {
        return false;
    }

    for (size_t i = 0; i < size1; ++i) {
        if (!dy_functions_are_equal(pair1.conts_with_check_status[i].cont, pair2.conts_with_check_status[i].cont)) {
            return false;
        }
    }

    return true;
}

bool dy_recursion_eliminators_are_equal(dy_core_recursion_eliminator_t rec1, dy_core_recursion_eliminator_t rec2)
{
    return dy_functions_are_equal(rec1.cont, rec2.cont);
}

bool dy_elim_refs_are_equal(dy_core_elim_ref_t elim_ref1, dy_core_elim_ref_t elim_ref2)
{
    return dy_exprs_are_equal(*elim_ref1.expr, *elim_ref2.expr) && elim_ref1.index == elim_ref2.index;
}

bool dy_composes_are_equal(dy_core_sequence_t c1, dy_core_sequence_t c2)
{
    if (!dy_exprs_are_equal(*c1.expr, *c2.expr)) {
        return false;
    }

    return dy_functions_are_equal(c1.cont, c2.cont);
}
