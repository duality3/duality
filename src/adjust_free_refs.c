/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "adjust_free_refs.h"

#include <duality/array.h>

bool dy_adjust_free_refs_of_expr(dy_core_expr_t expr, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_expr_t *result, bool *have_result)
{
    bool res;

    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        res = dy_adjust_free_refs_of_intro(expr.intro, free_ref_cutoff, offset, direction, &result->intro, have_result);
        break;
    case DY_CORE_EXPR_ELIM:
        res = dy_adjust_free_refs_of_elim(expr.elim, free_ref_cutoff, offset, direction, &result->elim, have_result);
        break;
    case DY_CORE_EXPR_REF:
        res = dy_adjust_free_refs_of_ref(expr.ref, free_ref_cutoff, offset, direction, &result->ref, have_result);
        break;
    case DY_CORE_EXPR_ELIM_REF:
        res = dy_adjust_free_refs_of_elim_ref(expr.elim_ref, free_ref_cutoff, offset, direction, &result->elim_ref, have_result);
        break;
    case DY_CORE_EXPR_INFERENCE_CTX:
        *have_result = false;
        res = true;
        break;
    case DY_CORE_EXPR_TRAP:
        *have_result = false;
        res = true;
        break;
    case DY_CORE_EXPR_COMP:
        res = dy_adjust_free_refs_of_comp(expr.comp, free_ref_cutoff, offset, direction, &result->comp, have_result);
        break;
    case DY_CORE_EXPR_SEQUENCE:
        res = dy_adjust_free_refs_of_sequence(expr.sequence, free_ref_cutoff, offset, direction, &result->sequence, have_result);
        break;
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = expr.tag;
    }

    return true;
}

bool dy_adjust_free_refs_of_expr_ptr(const dy_core_expr_t *expr, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_expr_t **result, bool *have_result)
{
    bool have_e;
    dy_core_expr_t e;
    if (!dy_adjust_free_refs_of_expr(*expr, free_ref_cutoff, offset, direction, &e, &have_e)) {
        return false;
    }

    if (have_e) {
        *result = dy_core_expr_new(e);
    }

    *have_result = have_e;
    return true;
}

bool dy_adjust_free_refs_of_intro(dy_core_intro_t intro, size_t free_var_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_intro_t *result, bool *have_result)
{
    bool res;

    switch (intro.tag) {
    case DY_INTRO_COMPLEX:
        res = dy_adjust_free_refs_of_complex(intro.complex, free_var_cutoff, offset, direction, &result->complex, have_result);
        break;
    case DY_INTRO_SIMPLE:
        res = dy_adjust_free_refs_of_simple(intro.simple, free_var_cutoff, offset, direction, &result->simple, have_result);
        break;
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = intro.tag;
        result->polarity = intro.polarity;
        result->is_implicit = intro.is_implicit;
    }

    return true;
}

bool dy_adjust_free_refs_of_complex(dy_core_complex_t complex, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_complex_t *result, bool *have_result)
{
    bool res;

    switch (complex.tag) {
    case DY_CORE_FUNCTION:
        res = dy_adjust_free_refs_of_function(complex.function, free_ref_cutoff, offset, direction, &result->function, have_result);
        break;
    case DY_CORE_TUPLE:
        res = dy_adjust_free_refs_of_tuple(complex.tuple, free_ref_cutoff, offset, direction, &result->tuple, have_result);
        break;
    case DY_CORE_RECURSION:
        res = dy_adjust_free_refs_of_recursion(complex.recursion, free_ref_cutoff, offset, direction, &result->recursion, have_result);
        break;
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = complex.tag;
    }

    return true;
}

bool dy_adjust_free_refs_of_function(dy_core_function_t function, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_function_t *result, bool *have_result)
{
    bool have_type = false;
    if (function.type != NULL) {
        if (!dy_adjust_free_refs_of_expr_ptr(function.type, free_ref_cutoff, offset, direction, &result->type, &have_type)) {
            return false;
        }
    }

    if (dy_size_t_add_overflow(free_ref_cutoff, 1, &free_ref_cutoff)) {
        return false;
    }

    bool have_sequent;
    if (!dy_adjust_free_refs_of_expr_ptr(function.sequent, free_ref_cutoff, offset, direction, &result->sequent, &have_sequent)) {
        return false;
    }

    *have_result = have_type || have_sequent;
    return true;
}

bool dy_adjust_free_refs_of_tuple(dy_core_tuple_t tuple, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_tuple_t *result, bool *have_result)
{
    const dy_core_expr_t *tuple_end = dy_array_past_end(tuple.sequents);
    size_t num_exprs = (size_t)(tuple_end - tuple.sequents);

    dy_core_expr_t *sequents = dy_array_create(sizeof *sequents, num_exprs);

    bool have_anything_new = false;

    for (const dy_core_expr_t *e = tuple.sequents; e != tuple_end; ++e) {
        bool have_sequent;
        dy_core_expr_t sequent;
        if (!dy_adjust_free_refs_of_expr(*e, free_ref_cutoff, offset, direction, &sequent, &have_sequent)) {
            return false;
        }

        if (have_sequent) {
            dy_array_add(&sequents, &sequent);
            have_anything_new = true;
        } else {
            dy_array_add(&sequents, e);
        }
    }

    if (have_anything_new) {
        result->sequents = sequents;
    }

    *have_result = have_anything_new;

    return true;
}

bool dy_adjust_free_refs_of_recursion(dy_core_recursion_t recursion, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_recursion_t *result, bool *have_result)
{
    if (dy_size_t_add_overflow(free_ref_cutoff, 1, &free_ref_cutoff)) {
        return false;
    }

    bool have_sequent;
    if (!dy_adjust_free_refs_of_expr_ptr(recursion.sequent, free_ref_cutoff + 1, offset, direction, &result->sequent, &have_sequent)) {
        return false;
    }

    *have_result = have_sequent;

    return true;
}

bool dy_adjust_free_refs_of_simple_prefix(dy_core_simple_prefix_t prefix, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_simple_prefix_t *result, bool *have_result)
{
    if (prefix.tag != DY_CORE_MAPPING) {
        *have_result = false;
        return true;
    }

    bool have_expr;
    if (!dy_adjust_free_refs_of_expr_ptr(prefix.expr, free_ref_cutoff, offset, direction, &result->expr, &have_expr)) {
        return false;
    }

    if (have_expr) {
        result->tag = prefix.tag;
        result->is_computationally_irrelevant = prefix.is_computationally_irrelevant;
    }

    *have_result = have_expr;
    return true;
}

bool dy_adjust_free_refs_of_simple(dy_core_simple_t simple, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_simple_t *result, bool *have_result)
{
    bool have_prefix;
    if (!dy_adjust_free_refs_of_simple_prefix(simple.prefix, free_ref_cutoff, offset, direction, &result->prefix, &have_prefix)) {
        return false;
    }

    bool have_sequent;
    if (!dy_adjust_free_refs_of_expr_ptr(simple.sequent, free_ref_cutoff, offset, direction, &result->sequent, &have_sequent)) {
        return false;
    }

    *have_result = have_prefix || have_sequent;
    return true;
}

bool dy_adjust_free_refs_of_elim(dy_core_elim_t elim, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_elim_t *result, bool *have_result)
{
    bool have_expr;
    if (!dy_adjust_free_refs_of_expr_ptr(elim.expr, free_ref_cutoff, offset, direction, &result->expr, &have_expr)) {
        return false;
    }

    bool have_result_type;
    if (!dy_adjust_free_refs_of_expr_ptr(elim.result_type, free_ref_cutoff, offset, direction, &result->result_type, &have_result_type)) {
        return false;
    }

    bool have_eliminator;
    bool res;
    switch (elim.tag) {
    case DY_INTRO_COMPLEX:
        res = dy_adjust_free_refs_of_complex_eliminator(elim.complex, free_ref_cutoff, offset, direction, &result->complex, &have_eliminator);
        break;
    case DY_INTRO_SIMPLE:
        res = dy_adjust_free_refs_of_simple_prefix(elim.simple_prefix, free_ref_cutoff, offset, direction, &result->simple_prefix, &have_eliminator);
        break;
    }

    if (!res) {
        return false;
    }

    *have_result = have_expr || have_eliminator || have_result_type;

    if (*have_result) {
        result->check_result = elim.check_result;
    }

    return true;
}

bool dy_adjust_free_refs_of_complex_eliminator(dy_core_complex_eliminator_t eliminator, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_complex_eliminator_t *result, bool *have_result)
{
    bool res;
    switch (eliminator.tag) {
    case DY_CORE_FUNCTION:
        res = dy_adjust_free_refs_of_function_eliminator(eliminator.function, free_ref_cutoff, offset, direction, &result->function, have_result);
        break;
    case DY_CORE_TUPLE:
        res = dy_adjust_free_refs_of_tuple_eliminator(eliminator.tuple, free_ref_cutoff, offset, direction, &result->tuple, have_result);
        break;
    case DY_CORE_RECURSION:
        res = dy_adjust_free_refs_of_recursion_eliminator(eliminator.recursion, free_ref_cutoff, offset, direction, &result->recursion, have_result);
        break;
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = eliminator.tag;
    }

    return true;
}

bool dy_adjust_free_refs_of_function_eliminator(dy_core_function_eliminator_t function, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_function_eliminator_t *result, bool *have_result)
{
    bool have_type = false;
    if (function.type != NULL) {
        if (!dy_adjust_free_refs_of_expr_ptr(function.type, free_ref_cutoff, offset, direction, &result->type, &have_type)) {
            return false;
        }
    }

    if (dy_size_t_add_overflow(free_ref_cutoff, 1, &free_ref_cutoff)) {
        return false;
    }

    bool have_cont;
    if (!dy_adjust_free_refs_of_function(function.cont, free_ref_cutoff, offset, direction, &result->cont, &have_cont)) {
        return false;
    }

    *have_result = have_type || have_cont;
    return true;
}

bool dy_adjust_free_refs_of_tuple_eliminator(dy_core_tuple_eliminator_t tuple, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_tuple_eliminator_t *result, bool *have_result)
{
    const dy_core_cont_with_check_status_t *tuple_end = dy_array_past_end(tuple.conts_with_check_status);
    size_t num_exprs = (size_t)(tuple_end - tuple.conts_with_check_status);

    dy_core_cont_with_check_status_t *conts = dy_array_create(sizeof *conts, num_exprs);

    bool have_anything_new = false;

    for (const dy_core_cont_with_check_status_t *e = tuple.conts_with_check_status; e != tuple_end; ++e) {
        bool have_cont;
        dy_core_function_t cont;
        if (!dy_adjust_free_refs_of_function(e->cont, free_ref_cutoff, offset, direction, &cont, &have_cont)) {
            return false;
        }

        if (have_cont) {
            dy_array_add(&conts, &(dy_core_cont_with_check_status_t){
                .cont = cont,
                .status = e->status
            });
            have_anything_new = true;
        } else {
            dy_array_add(&conts, &e);
        }
    }

    if (have_anything_new) {
        result->conts_with_check_status = conts;
    }

    *have_result = have_anything_new;

    return true;
}

bool dy_adjust_free_refs_of_recursion_eliminator(dy_core_recursion_eliminator_t recursion, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_recursion_eliminator_t *result, bool *have_result)
{
    if (dy_size_t_add_overflow(free_ref_cutoff, 1, &free_ref_cutoff)) {
        return false;
    }

    if (!dy_adjust_free_refs_of_function(recursion.cont, free_ref_cutoff, offset, direction, &result->cont, have_result)) {
        return false;
    }

    if (*have_result) {
        result->num_inference_ctxs = recursion.num_inference_ctxs;
        result->check_result = recursion.check_result;
    }

    return true;
}

bool dy_adjust_free_refs_of_elim_ref(dy_core_elim_ref_t elim_ref, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_elim_ref_t *result, bool *have_result)
{
    bool have_expr;
    dy_core_expr_t expr;
    if (!dy_adjust_free_refs_of_expr(*elim_ref.expr, free_ref_cutoff, offset, direction, &expr, &have_expr)) {
        return false;
    }

    bool have_index;
    if (!dy_adjust_free_refs_of_ref(elim_ref.index, free_ref_cutoff, offset, direction, &result->index, &have_index)) {
        return false;
    }

    if (have_expr) {
        result->expr = dy_core_expr_new(expr);
    }

    *have_result = have_expr || have_index;
    return true;
}

bool dy_adjust_free_refs_of_ref(size_t ref, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, size_t *result, bool *have_result)
{
    if (ref < free_ref_cutoff) {
        *have_result = false;
        return true;
    }

    switch (direction) {
    case DY_ADJUST_FREE_REFS_DIRECTION_UP:
        if (dy_size_t_sub_overflow(ref, offset, result)) {
            return false;
        }

        *have_result = true;
        return true;
    case DY_ADJUST_FREE_REFS_DIRECTION_DOWN:
        if (dy_size_t_add_overflow(ref, offset, result)) {
            return false;
        }

        *have_result = true;
        return true;
    }
}

bool dy_adjust_free_refs_of_comp(dy_core_comp_t comp, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_comp_t *result, bool *have_result)
{
    return dy_adjust_free_refs_of_expr_ptr(comp.expr, free_ref_cutoff, offset, direction, &result->expr, have_result);
}

bool dy_adjust_free_refs_of_sequence(dy_core_sequence_t sequence, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_sequence_t *result, bool *have_result)
{
    bool have_expr;
    if (!dy_adjust_free_refs_of_expr_ptr(sequence.expr, free_ref_cutoff, offset, direction, &result->expr, &have_expr)) {
        return false;
    }

    bool have_cont;
    if (!dy_adjust_free_refs_of_function(sequence.cont, free_ref_cutoff, offset, direction, &result->cont, &have_cont)) {
        return false;
    }

    bool have_type;
    if (!dy_adjust_free_refs_of_expr_ptr(sequence.type, free_ref_cutoff, offset, direction, &result->type, &have_type)) {
        return false;
    }

    *have_result = have_expr || have_cont || have_type;
    return true;
}
