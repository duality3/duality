/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <duality/check.h>

#include "type_of.h"
#include "is_subtype.h"
#include "ref_appears_in_polarity.h"
#include "substitute_in_place.h"
#include "ref_occurs_in.h"
#include "adjust_free_refs.h"

#include <duality/array.h>

static inline
dy_check_res_tag_t dy_check_elim_without_subtype_check(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_elim_t *elim, size_t *inference_ids, size_t *float_offset);

static inline
void dy_subtype_check_elim(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_elim_t *elim);


static inline
dy_core_expr_t dy_elim_supertype(dy_core_elim_t elim);


static inline
void dy_subtype_check_complex_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_complex_eliminator_t *eliminator, dy_core_expr_t result_type);

static inline
void dy_subtype_check_function_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_function_eliminator_t *eliminator, dy_core_expr_t result_type);

static inline
void dy_subtype_check_pair_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_tuple_eliminator_t *eliminator, dy_core_expr_t result_type);

static inline
void dy_subtype_check_recursion_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_recursion_eliminator_t *eliminator, dy_core_expr_t result_type);

static inline
void dy_subtype_check_cont(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_function_t *cont, dy_check_status_t *check_result, dy_core_expr_t result_type);


static inline
dy_check_res_tag_t dy_check_sequence_without_subtype_check(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_sequence_t *compose, size_t *float_offset);

static inline
void dy_subtype_check_sequence(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_sequence_t *sequence);


static inline
dy_check_status_t dy_subtype_result_to_check_status(dy_subtype_res_t res);


static inline
dy_core_expr_t dy_make_single_non_recursive_expr(dy_core_expr_t *exprs, dy_polarity_t polarity);

static inline
dy_core_expr_t dy_make_tuple_with_ref(dy_core_expr_t expr, dy_polarity_t polarity);


dy_check_res_tag_t dy_check_expr(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, size_t *float_offset)
{
    switch (expr->tag) {
    case DY_CORE_EXPR_INTRO:
        return dy_check_intro(binders_in_scope, constraints, &expr->intro, float_offset);
    case DY_CORE_EXPR_ELIM:
        return dy_check_elim(binders_in_scope, constraints, expr, &expr->elim, float_offset);
    case DY_CORE_EXPR_REF:
        return DY_CHECK_RES_FINISHED;
    case DY_CORE_EXPR_ELIM_REF:
        return dy_check_elim_ref(binders_in_scope, constraints, expr, &expr->elim_ref, float_offset);
    case DY_CORE_EXPR_SEQUENCE:
        return dy_check_sequence(binders_in_scope, constraints, expr, &expr->sequence, float_offset);
    case DY_CORE_EXPR_COMP:
        return dy_check_expr(binders_in_scope, constraints, expr->comp.expr, float_offset);
    case DY_CORE_EXPR_TRAP:
        return DY_CHECK_RES_FINISHED;
    case DY_CORE_EXPR_INFERENCE_CTX:
        return dy_check_inference_ctx(binders_in_scope, constraints, expr, &expr->inference_ctx, float_offset);
    }
}

dy_check_res_tag_t dy_check_intro(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_intro_t *intro, size_t *float_offset)
{
    switch (intro->tag) {
    case DY_INTRO_COMPLEX:
        return dy_check_complex(binders_in_scope, constraints, &intro->complex, float_offset);
    case DY_INTRO_SIMPLE:
        return dy_check_simple(binders_in_scope, constraints,  &intro->simple, float_offset);
    }
}

dy_check_res_tag_t dy_check_complex(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_complex_t *complex, size_t *float_offset)
{
    switch (complex->tag) {
    case DY_CORE_FUNCTION:
        return dy_check_function(binders_in_scope, constraints, &complex->function, float_offset);
    case DY_CORE_TUPLE:
        return dy_check_tuple(binders_in_scope, constraints, &complex->tuple, float_offset);
    case DY_CORE_RECURSION:
        return dy_check_recursion(binders_in_scope, constraints, &complex->recursion, float_offset);
    }
}

dy_check_res_tag_t dy_check_function(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_function_t *function, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_binder_t binder;
    if (function->type != NULL) {
        dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, function->type, float_offset);
        if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
            return res;
        }

        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .have_type = true,
            .type = *function->type
        };
    } else {
        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .have_type = false
        };
    }

    dy_array_add(binders_in_scope, &binder);

    size_t constraint_start2 = dy_constraints_length(*constraints);

    dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, function->sequent, float_offset);

    dy_array_remove_last(*binders_in_scope);

    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        --*float_offset;
        return res;
    }

    dy_move_constraints_up(*constraints, &(*constraints)[constraint_start2]);

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_tuple(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_tuple_t *tuple, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    const dy_core_expr_t *end = dy_array_past_end(tuple->sequents);

    for (dy_core_expr_t *expr_ptr = tuple->sequents; expr_ptr != end; expr_ptr++) {
        size_t constraint_start2 = dy_constraints_length(*constraints);

        dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, expr_ptr, float_offset);
        if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
            return res;
        }

        dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);
    }

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_recursion(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_recursion_t *recursion, size_t *float_offset)
{
    static const dy_binder_t binder = {
        .tag = DY_BINDER_RECURSION
    };

    dy_array_add(binders_in_scope, &binder);

    size_t constraint_start = dy_constraints_length(*constraints);

    dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, recursion->sequent, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        --*float_offset;
        return res;
    }

    dy_move_constraints_up(*constraints, *constraints + constraint_start);

    dy_array_remove_last(*binders_in_scope);

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_simple_prefix(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_simple_prefix_t *prefix, size_t *float_offset)
{
    if (prefix->tag != DY_CORE_MAPPING) {
        return DY_CHECK_RES_FINISHED;
    }

    return dy_check_expr(binders_in_scope, constraints, prefix->expr, float_offset);
}

dy_check_res_tag_t dy_check_simple(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_simple_t *simple, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_check_res_tag_t prefix_res = dy_check_simple_prefix(binders_in_scope, constraints, &simple->prefix, float_offset);
    if (prefix_res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return prefix_res;
    }

    size_t constraint_start2 = dy_constraints_length(*constraints);

    dy_check_res_tag_t sequent_res = dy_check_expr(binders_in_scope, constraints, simple->sequent, float_offset);
    if (sequent_res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return sequent_res;
    }

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_elim(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_elim_t *elim, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    size_t num_generalizations;

    dy_check_res_tag_t res = dy_check_elim_without_subtype_check(binders_in_scope, constraints, elim, &num_generalizations, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    if (elim->tag == DY_INTRO_COMPLEX) {
        size_t constraint_start2 = dy_constraints_length(*constraints);

        dy_subtype_check_complex_eliminator(binders_in_scope, constraints, &elim->complex, *elim->result_type);

        dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);
    }

    size_t constraint_start2 = dy_constraints_length(*constraints);

    dy_subtype_check_elim(binders_in_scope, constraints, expr, elim);

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    for (size_t i = 0; i < num_generalizations; ++i) {
        *expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = true,
                .tag = DY_INTRO_COMPLEX,
                .complex = {
                    .tag = DY_CORE_FUNCTION,
                    .function = {
                        .type = NULL,
                        .sequent = dy_core_expr_new(*expr)
                    }
                }
            }
        };
    }

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_elim_without_subtype_check(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_elim_t *elim, size_t *num_inference_ctxs, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, elim->expr, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    size_t constraint_start2 = dy_constraints_length(*constraints);

    res = dy_check_expr(binders_in_scope, constraints, elim->result_type, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    constraint_start2 = dy_constraints_length(*constraints);

    switch (elim->tag) {
    case DY_INTRO_COMPLEX:
        res = dy_check_complex_eliminator(binders_in_scope, constraints,  &elim->complex, num_inference_ctxs, float_offset);
        break;
    case DY_INTRO_SIMPLE:
        res = dy_check_simple_prefix(binders_in_scope, constraints, &elim->simple_prefix, float_offset);
        break;
    }

    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_complex_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_complex_eliminator_t *eliminator, size_t *num_inference_ctxs, size_t *float_offset)
{
    switch (eliminator->tag) {
    case DY_CORE_FUNCTION:
        return dy_check_function_eliminator(binders_in_scope, constraints, &eliminator->function, float_offset);
    case DY_CORE_TUPLE:
        return dy_check_tuple_eliminator(binders_in_scope, constraints, &eliminator->tuple, float_offset);
    case DY_CORE_RECURSION:
        return dy_check_recursion_eliminator(binders_in_scope, constraints, &eliminator->recursion, num_inference_ctxs, float_offset);
    }
}

dy_check_res_tag_t dy_check_function_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_function_eliminator_t *eliminator, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_binder_t binder;
    if (eliminator->type != NULL) {
        dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, eliminator->type, float_offset);
        if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
            return res;
        }

        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .type = *eliminator->type,
            .have_type = true
        };
    } else {
        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .have_type = false
        };
    }

    dy_array_add(&binders_in_scope, &binder);

    size_t constraint_start2 = dy_constraints_length(*constraints);

    dy_check_res_tag_t res = dy_check_function(binders_in_scope, constraints,  &eliminator->cont, float_offset);

    dy_array_remove_last(binders_in_scope);

    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        --*float_offset;
        return res;
    }

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_tuple_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_tuple_eliminator_t *eliminator, size_t *float_offset)
{
    dy_core_cont_with_check_status_t *cont = eliminator->conts_with_check_status;

    const dy_core_cont_with_check_status_t *conts_end = dy_array_past_end(eliminator->conts_with_check_status);

    size_t constraint_start1 = dy_constraints_length(*constraints);

    for (; cont != conts_end; ++cont) {
        size_t constraint_start2 = dy_constraints_length(*constraints);

        dy_check_res_tag_t res = dy_check_function(binders_in_scope, constraints, &cont->cont, float_offset);
        if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
            return res;
        }

        dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);
    }

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_recursion_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_recursion_eliminator_t *eliminator, size_t *num_generalizations, size_t *float_offset)
{
    static const dy_binder_t binder = {
        .tag = DY_BINDER_RECURSION
    };

    dy_array_add(&binders_in_scope, &binder);

    dy_core_expr_t intermediate_expr = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = false,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_FUNCTION,
                .function = eliminator->cont
            }
        }
    };

    for (size_t i = 0; i < eliminator->num_inference_ctxs; ++i) {
        intermediate_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INFERENCE_CTX,
            .inference_ctx = {
                .num_dependent_ctxs = 0,
                .expr = dy_core_expr_new(intermediate_expr)
            }
        };
    }

    dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, &intermediate_expr, float_offset);

    dy_array_remove_last(binders_in_scope);

    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        --*float_offset;
        return res;
    }

    for (;;) {
        if (intermediate_expr.intro.is_implicit) {
            (*num_generalizations)++;

            intermediate_expr = *intermediate_expr.intro.complex.function.sequent;
        } else {
            eliminator->cont = intermediate_expr.intro.complex.function;
            break;
        }
    }

    return DY_CHECK_RES_FINISHED;
}

void dy_subtype_check_elim(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_elim_t *elim)
{
    if (elim->check_result != DY_CHECK_STATUS_NEEDS_CHECKING) {
        return;
    }

    dy_core_expr_t subtype;
    if (!dy_type_of_expr(binders_in_scope, *elim->expr, &subtype)) {
        elim->check_result = DY_CHECK_STATUS_FAIL;
        return;
    }

    dy_core_expr_t supertype = dy_elim_supertype(*elim);

    dy_subtype_res_t res = dy_exprs_are_subtypes_and_transform(binders_in_scope, constraints, subtype, supertype, elim->expr, expr);

    elim->check_result = dy_subtype_result_to_check_status(res);
}

dy_core_expr_t dy_elim_supertype(dy_core_elim_t elim)
{
    switch (elim.tag) {
    case DY_INTRO_COMPLEX:
        switch (elim.complex.tag) {
        case DY_CORE_FUNCTION:
            return (dy_core_expr_t){
                .tag = DY_CORE_EXPR_INTRO,
                .intro = {
                    .is_implicit = elim.is_implicit,
                    .polarity = DY_POLARITY_NEGATIVE,
                    .tag = DY_INTRO_COMPLEX,
                    .complex = {
                        .tag = DY_CORE_FUNCTION,
                        .function = {
                            .type = elim.complex.function.type,
                            .sequent = elim.complex.function.cont.type
                        }
                    }
                }
            };
        case DY_CORE_TUPLE: {
            const dy_core_cont_with_check_status_t *end = dy_array_past_end(elim.complex.tuple.conts_with_check_status);
            size_t num_exprs = (size_t)(end - elim.complex.tuple.conts_with_check_status);

            dy_core_expr_t *exprs = dy_array_create(sizeof *exprs, num_exprs);
            for (const dy_core_cont_with_check_status_t *c = elim.complex.tuple.conts_with_check_status; c != end; ++c) {
                dy_array_add(&exprs, c->cont.type);
            }

            return (dy_core_expr_t){
                .tag = DY_CORE_EXPR_INTRO,
                .intro = {
                    .is_implicit = elim.is_implicit,
                    .polarity = DY_POLARITY_NEGATIVE,
                    .tag = DY_INTRO_COMPLEX,
                    .complex = {
                        .tag = DY_CORE_TUPLE,
                        .tuple = {
                            .sequents = exprs
                        }
                    }
                }
            };
        }
        case DY_CORE_RECURSION:
            return (dy_core_expr_t){
                .tag = DY_CORE_EXPR_INTRO,
                .intro = {
                    .is_implicit = elim.is_implicit,
                    .polarity = DY_POLARITY_NEGATIVE,
                    .tag = DY_INTRO_COMPLEX,
                    .complex = {
                        .tag = DY_CORE_RECURSION,
                        .recursion = {
                            .sequent = elim.complex.recursion.cont.type
                        }
                    }
                }
            };
        }
    case DY_INTRO_SIMPLE:
        return (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .is_implicit = elim.is_implicit,
                .polarity = DY_POLARITY_NEGATIVE,
                .tag = DY_INTRO_SIMPLE,
                .simple = {
                    .prefix = elim.simple_prefix,
                    .sequent = elim.result_type
                }
            }
        };
    }
}

void dy_subtype_check_complex_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_complex_eliminator_t *eliminator, dy_core_expr_t result_type)
{
    switch (eliminator->tag) {
    case DY_CORE_FUNCTION:
        dy_subtype_check_function_eliminator(binders_in_scope, constraints, &eliminator->function, result_type);
        return;
    case DY_CORE_TUPLE:
        dy_subtype_check_pair_eliminator(binders_in_scope, constraints, &eliminator->tuple, result_type);
        return;
    case DY_CORE_RECURSION:
        dy_subtype_check_recursion_eliminator(binders_in_scope, constraints, &eliminator->recursion, result_type);
        return;
    }
}

void dy_subtype_check_function_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_function_eliminator_t *eliminator, dy_core_expr_t result_type)
{
    if (eliminator->check_result != DY_CHECK_STATUS_NEEDS_CHECKING) {
        return;
    }

    dy_binder_t binder;
    if (eliminator->type != NULL) {
        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .have_type = true,
            .type = *eliminator->type
        };
    } else {
        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .have_type = false
        };
    }

    dy_array_add(&binders_in_scope, &binder);

    dy_subtype_check_cont(binders_in_scope, constraints, &eliminator->cont, &eliminator->check_result, result_type);

    dy_array_remove_last(binders_in_scope);
}

void dy_subtype_check_pair_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_tuple_eliminator_t *eliminator, dy_core_expr_t result_type)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    const dy_core_cont_with_check_status_t *end = dy_array_past_end(eliminator->conts_with_check_status);
    for (dy_core_cont_with_check_status_t *cont = eliminator->conts_with_check_status; cont != end; ++cont) {
        if (cont->status != DY_CHECK_STATUS_NEEDS_CHECKING) {
            continue;
        }

        size_t constraint_start2 = dy_constraints_length(*constraints);

        dy_subtype_check_cont(binders_in_scope, constraints, &cont->cont, &cont->status, result_type);

        dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);
    }
}

void dy_subtype_check_recursion_eliminator(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_recursion_eliminator_t *eliminator, dy_core_expr_t result_type)
{
    if (eliminator->check_result != DY_CHECK_STATUS_NEEDS_CHECKING) {
        return;
    }

    static const dy_binder_t binder = {
        .tag = DY_BINDER_RECURSION
    };

    dy_array_add(&binders_in_scope, &binder);

    //assert(eliminator->cont_with_inference_ctx.inference_ids == dy_array_past_end(eliminator->cont_with_inference_ctx.inference_ids));

    dy_subtype_check_cont(binders_in_scope, constraints, &eliminator->cont, &eliminator->check_result, result_type);

    dy_array_remove_last(binders_in_scope);
}

void dy_subtype_check_cont(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_function_t *cont, dy_check_status_t *check_result, dy_core_expr_t result_type)
{
    dy_binder_t binder = {
        .tag = DY_BINDER_FUNCTION,
        .have_type = true,
        .type = *cont->type
    };

    dy_array_add(&binders_in_scope, &binder);

    dy_core_expr_t subtype;
    bool type_of_result = dy_type_of_expr(binders_in_scope, *cont->sequent, &subtype);

    dy_array_remove_last(binders_in_scope);

    if (!type_of_result) {
        *check_result = DY_CHECK_STATUS_FAIL;
        return;
    }

    dy_sequence_step_t *sequence_steps = dy_array_create(sizeof *sequence_steps, 4);

    bool have_new_sequent;
    dy_subtype_res_t res = dy_exprs_are_subtypes(binders_in_scope, constraints, &sequence_steps, subtype, result_type, *cont->sequent, cont->sequent, &have_new_sequent);

    if (sequence_steps != dy_array_past_end(sequence_steps)) {
        *check_result = DY_CHECK_STATUS_FAIL;
    } else {
        *check_result = dy_subtype_result_to_check_status(res);
    }
}

dy_check_res_tag_t dy_check_elim_ref(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_elim_ref_t *elim_ref, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, elim_ref->expr, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    if (elim_ref->check_result != DY_CHECK_STATUS_NEEDS_CHECKING) {
        return DY_CHECK_RES_FINISHED;
    }

    dy_core_expr_t type;
    if (!dy_type_of_expr(binders_in_scope, *elim_ref->expr, &type)) {
        elim_ref->check_result = DY_CHECK_STATUS_FAIL;

        return DY_CHECK_RES_FINISHED;
    }

    size_t constraint_start2 = dy_constraints_length(*constraints);

    dy_core_expr_t supertype = {
        .tag = DY_CORE_EXPR_REF,
        .ref = elim_ref->index
    };

    dy_subtype_res_t subtype_res = dy_exprs_are_subtypes_and_transform(binders_in_scope, constraints, type, supertype, elim_ref->expr, expr);

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    elim_ref->check_result = dy_subtype_result_to_check_status(subtype_res);

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_inference_ctx(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_inference_ctx_t *inference_ctx, size_t *float_offset)
{
    size_t constraint_start = dy_constraints_length(*constraints);

    dy_array_add(binders_in_scope, &(dy_binder_t){
        .tag = DY_BINDER_INFERENCE_CTX,
        .have_type = false
    });

    dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, inference_ctx->expr, float_offset);

    dy_array_remove_last(*binders_in_scope);

    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        --*float_offset;

        if (*float_offset == 0) {
            inference_ctx->num_dependent_ctxs++;

            *expr = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_INFERENCE_CTX,
                .inference_ctx = {
                    .num_dependent_ctxs = 0,
                    .expr = dy_core_expr_new(*expr)
                }
            };

            return dy_check_expr(binders_in_scope, constraints, expr, float_offset);
        }

        return res;
    }

    size_t own_float_offset = dy_constraint_float_offset(*constraints + constraint_start, dy_array_past_end(*constraints));

    if (own_float_offset > inference_ctx->num_dependent_ctxs) {
        *expr = *inference_ctx->expr;
        *float_offset = own_float_offset;
        return DY_CHECK_RES_FLOAT_INFERENCE_CTX;
    }

    dy_core_expr_t *lower_bounds, *upper_bounds;
    dy_constraint_get(*constraints, *constraints + constraint_start, &lower_bounds, &upper_bounds);

    bool do_generalize = false;

    dy_core_expr_t resolved_expr;
    if (lower_bounds != NULL && upper_bounds != NULL) {
        // It doesn't matter which bounds we take if both are available.
        // We just arbitrarily take the lower ones.
        resolved_expr = dy_make_single_non_recursive_expr(lower_bounds, DY_POLARITY_NEGATIVE);
    } else {
        dy_array_add(binders_in_scope, &(dy_binder_t){
            .tag = DY_BINDER_INFERENCE_CTX,
            .have_type = false
        });

        bool appears_positive = false, appears_negative = false;
        dy_core_expr_t type;
        if (dy_type_of_expr(binders_in_scope, *inference_ctx->expr, &type)) {
            dy_ref_appears_in_polarity(0, type, DY_POLARITY_POSITIVE, &appears_positive, &appears_negative);
        }

        dy_array_remove_last(*binders_in_scope);

        if (lower_bounds != NULL) {
            resolved_expr = dy_make_single_non_recursive_expr(lower_bounds, DY_POLARITY_NEGATIVE);

            if (appears_positive) {
                do_generalize = true;
                resolved_expr = dy_make_tuple_with_ref(resolved_expr, DY_POLARITY_NEGATIVE);
            }
        } else if (upper_bounds != NULL) {
            resolved_expr = dy_make_single_non_recursive_expr(upper_bounds, DY_POLARITY_POSITIVE);

            if (appears_negative) {
                do_generalize = true;
                resolved_expr = dy_make_tuple_with_ref(resolved_expr, DY_POLARITY_POSITIVE);
            }
        } else {
            if (appears_negative && appears_positive) {
                do_generalize = true;
                resolved_expr = (dy_core_expr_t){
                    .tag = DY_CORE_EXPR_REF,
                    .ref = 0
                };
            } else {
                resolved_expr = (dy_core_expr_t){
                    .tag = DY_CORE_EXPR_INTRO,
                    .intro = {
                        .polarity = appears_negative ? DY_POLARITY_NEGATIVE : DY_POLARITY_POSITIVE,
                        .is_implicit = true,
                        .tag = DY_INTRO_COMPLEX,
                        .complex = {
                            .tag = DY_CORE_FUNCTION,
                            .function = {
                                .type = NULL,
                                .sequent = dy_core_expr_new((dy_core_expr_t){
                                    .tag = DY_CORE_EXPR_REF,
                                    .ref = 0
                                })
                            }
                        }
                    }
                };
            }
        }
    }

    bool did_substitute;
    if (!dy_substitute_expr_in_place(inference_ctx->expr, 0, resolved_expr, &did_substitute)) {
        return DY_CHECK_RES_FINISHED;
    }

    if (do_generalize) {
        *inference_ctx->expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = true,
                .tag = DY_INTRO_COMPLEX,
                .complex = {
                    .tag = DY_CORE_FUNCTION,
                    .function = {
                        .type = NULL,
                        .sequent = dy_core_expr_new(*inference_ctx->expr)
                    }
                }
            }
        };
    } else {
        bool did_adjust;
        dy_adjust_free_refs_of_expr(*inference_ctx->expr, 0, 1, DY_ADJUST_FREE_REFS_DIRECTION_UP, inference_ctx->expr, &did_adjust);
    }

    *expr = *inference_ctx->expr;

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_sequence(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_sequence_t *sequence, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_check_res_tag_t res = dy_check_sequence_without_subtype_check(binders_in_scope, constraints, sequence, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    size_t constraint_start2 = dy_constraints_length(*constraints);

    dy_subtype_check_sequence(binders_in_scope, constraints, expr, sequence);

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    return DY_CHECK_RES_FINISHED;
}

dy_check_res_tag_t dy_check_sequence_without_subtype_check(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_sequence_t *sequence, size_t *float_offset)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_check_res_tag_t res = dy_check_expr(binders_in_scope, constraints, sequence->expr, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    size_t constraint_start2 = dy_constraints_length(*constraints);

    res = dy_check_function(binders_in_scope, constraints, &sequence->cont, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    constraint_start2 = dy_constraints_length(*constraints);

    res = dy_check_expr(binders_in_scope, constraints, sequence->type, float_offset);
    if (res == DY_CHECK_RES_FLOAT_INFERENCE_CTX) {
        return res;
    }

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);

    return DY_CHECK_RES_FINISHED;
}

void dy_subtype_check_sequence(dy_binder_t **binders_in_scope, dy_constraint_t **constraints, dy_core_expr_t *expr, dy_core_sequence_t *sequence)
{
    size_t constraint_start1 = dy_constraints_length(*constraints);

    dy_core_expr_t comp_second_type = {
        .tag = DY_CORE_EXPR_COMP,
        .comp = {
            .expr = sequence->type
        }
    };

    dy_subtype_check_cont(binders_in_scope, constraints, &sequence->cont, &sequence->type_result, comp_second_type);

    dy_core_expr_t type_of_expr;
    if (!dy_type_of_expr(binders_in_scope, *sequence->expr, &type_of_expr)) {
        sequence->expr_result = DY_CHECK_STATUS_FAIL;
        return;
    }

    dy_core_expr_t comp_cont_in_type = {
        .tag = DY_CORE_EXPR_COMP,
        .comp = {
            .expr = sequence->cont.type
        }
    };

    size_t constraint_start2 = dy_constraints_length(*constraints);

    dy_subtype_res_t res_first = dy_exprs_are_subtypes_and_transform(binders_in_scope, constraints, type_of_expr, comp_cont_in_type, sequence->expr, expr);

    sequence->expr_result = dy_subtype_result_to_check_status(res_first);

    dy_join_constraints(*constraints, &(*constraints)[constraint_start1], &(*constraints)[constraint_start2]);
}

dy_check_status_t dy_subtype_result_to_check_status(dy_subtype_res_t res)
{
    switch (res) {
    case DY_SUBTYPE_RES_OK:
        return DY_CHECK_STATUS_OK;
    case DY_SUBTYPE_RES_FAIL:
        return DY_CHECK_STATUS_FAIL;
    case DY_SUBTYPE_RES_INFERENCE:
        return DY_CHECK_STATUS_NEEDS_CHECKING;
    }
}

dy_core_expr_t dy_make_single_non_recursive_expr(dy_core_expr_t *exprs, dy_polarity_t polarity)
{
    dy_core_expr_t expr;
    if (exprs == dy_array_past_end(exprs)) {
        expr = *exprs;
    } else {
        expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = polarity,
                .is_implicit = true,
                .tag = DY_INTRO_COMPLEX,
                .complex = {
                    .tag = DY_CORE_TUPLE,
                    .tuple = {
                        .sequents = exprs
                    }
                }
            }
        };
    }

    if (dy_ref_occurs_in_expr(0, expr)) {
        expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = polarity,
                .is_implicit = true,
                .tag = DY_INTRO_COMPLEX,
                .complex = {
                    .tag = DY_CORE_RECURSION,
                    .recursion = {
                        .sequent = dy_core_expr_new(expr)
                    }
                }
            }
        };
    }

    return expr;
}

dy_core_expr_t dy_make_tuple_with_ref(dy_core_expr_t expr, dy_polarity_t polarity)
{
    dy_core_expr_t *sequents = dy_array_create(sizeof *sequents, 2);
    dy_array_add(&sequents, &expr);
    dy_array_add(&sequents, &(dy_core_expr_t){ .tag = DY_CORE_EXPR_REF, .ref = 0 });

    return (dy_core_expr_t){
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = polarity,
            .is_implicit = true,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_TUPLE,
                .tuple = {
                    .sequents = sequents
                }
            }
        }
    };
}
