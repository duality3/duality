/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "ref_appears_in_polarity.h"

#include <duality/array.h>

static inline
dy_polarity_t dy_flip_polarity(dy_polarity_t polarity);

void dy_ref_appears_in_polarity(size_t ref, dy_core_expr_t expr, dy_polarity_t current_polarity, bool *positive, bool *negative)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        switch (expr.intro.tag) {
        case DY_INTRO_COMPLEX:
            switch (expr.intro.complex.tag) {
            case DY_CORE_FUNCTION:
                if (expr.intro.complex.function.type != NULL) {
                    dy_ref_appears_in_polarity(ref, *expr.intro.complex.function.type, dy_flip_polarity(current_polarity), positive, negative);

                    if (*positive && *negative) {
                        return;
                    }
                }

                dy_ref_appears_in_polarity(ref + 1, *expr.intro.complex.function.sequent, current_polarity, positive, negative);
                return;
            case DY_CORE_TUPLE:
                for (const dy_core_expr_t *e = expr.intro.complex.tuple.sequents, *end = dy_array_past_end(e); e != end; ++ e) {
                    dy_ref_appears_in_polarity(ref, *e, current_polarity, positive, negative);
                    if (*positive && *negative) {
                        return;
                    }
                }

                return;
            case DY_CORE_RECURSION:
                dy_ref_appears_in_polarity(ref + 1, *expr.intro.complex.recursion.sequent, current_polarity, positive, negative);
                return;
            }
        case DY_INTRO_SIMPLE:
            dy_ref_appears_in_polarity(ref, *expr.intro.simple.sequent, current_polarity, positive, negative);
            return;
        }
    case DY_CORE_EXPR_ELIM:
        return;
    case DY_CORE_EXPR_REF:
        if (expr.ref == ref) {
            switch (current_polarity) {
            case DY_POLARITY_POSITIVE:
                *positive = true;
                return;
            case DY_POLARITY_NEGATIVE:
                *negative = true;
                return;
            }
        }

        return;
    case DY_CORE_EXPR_ELIM_REF:
        return;
    case DY_CORE_EXPR_COMP:
        dy_ref_appears_in_polarity(ref, *expr.comp.expr, current_polarity, positive, negative);
        return;
    case DY_CORE_EXPR_SEQUENCE:
        return;
    case DY_CORE_EXPR_INFERENCE_CTX:
        return;
    case DY_CORE_EXPR_TRAP:
        return;
    }
}

dy_polarity_t dy_flip_polarity(dy_polarity_t polarity)
{
    switch (polarity) {
    case DY_POLARITY_POSITIVE:
        return DY_POLARITY_NEGATIVE;
    case DY_POLARITY_NEGATIVE:
        return DY_POLARITY_POSITIVE;
    }
}
