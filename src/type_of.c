/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "type_of.h"

#include "adjust_free_refs.h"

#include <duality/array.h>

bool dy_type_of_expr(dy_binder_t **binders_in_scope, dy_core_expr_t expr, dy_core_expr_t *type)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        if (!dy_type_of_intro(binders_in_scope, expr.intro, &expr.intro)) {
            return false;
        }

        *type = expr;
        return true;
    case DY_CORE_EXPR_ELIM:
        *type = dy_type_of_elim(expr.elim);
        return true;
    case DY_CORE_EXPR_REF:
        return dy_type_of_variable(binders_in_scope, expr.ref, type);
    case DY_CORE_EXPR_ELIM_REF:
        return dy_type_of_elim_ref(binders_in_scope, expr.elim_ref, type);
    case DY_CORE_EXPR_SEQUENCE:
        *type = dy_type_of_sequence(expr.sequence);
        return true;
    case DY_CORE_EXPR_COMP:
        if (!dy_type_of_comp(binders_in_scope, expr.comp, &expr.comp)) {
            return false;
        }

        *type = expr;
        return true;
    case DY_CORE_EXPR_TRAP:
        *type = dy_type_of_trap();
        return true;
    case DY_CORE_EXPR_INFERENCE_CTX:
        return false;
    }
}

bool dy_type_of_intro(dy_binder_t **binders_in_scope, dy_core_intro_t intro, dy_core_intro_t *type)
{
    switch (intro.tag) {
    case DY_INTRO_COMPLEX:
        if (!dy_type_of_complex(binders_in_scope, intro.complex, &intro.complex)) {
            return false;
        }

        break;
    case DY_INTRO_SIMPLE:
        if (!dy_type_of_simple(binders_in_scope, intro.simple, &intro.simple)) {
            return false;
        }

        break;
    }

    intro.polarity = DY_POLARITY_POSITIVE;

    *type = intro;
    return true;
}

bool dy_type_of_complex(dy_binder_t **binders_in_scope, dy_core_complex_t complex, dy_core_complex_t *type)
{
    switch (complex.tag) {
    case DY_CORE_FUNCTION:
        if (!dy_type_of_function(binders_in_scope, complex.function, &complex.function)) {
            return false;
        }

        *type = complex;
        return true;
    case DY_CORE_TUPLE:
        if (!dy_type_of_tuple(binders_in_scope, complex.tuple, &complex.tuple)) {
            return false;
        }

        *type = complex;
        return true;
    case DY_CORE_RECURSION:
        if (!dy_type_of_recursion(binders_in_scope, complex.recursion, &complex.recursion)) {
            return false;
        }

        *type = complex;
        return true;
    }
}

bool dy_type_of_function(dy_binder_t **binders_in_scope, dy_core_function_t function, dy_core_function_t *type)
{
    dy_binder_t binder;
    if (function.type != NULL) {
        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .have_type = true,
            .type = *function.type
        };
    } else {
        binder = (dy_binder_t){
            .tag = DY_BINDER_FUNCTION,
            .have_type = false
        };
    }

    dy_array_add(binders_in_scope, &binder);

    dy_core_expr_t sequent;
    bool res = dy_type_of_expr(binders_in_scope, *function.sequent, &sequent);

    dy_array_remove_last(*binders_in_scope);

    if (!res) {
        return false;
    }

    function.sequent = dy_core_expr_new(sequent);

    *type = function;
    return true;
}

bool dy_type_of_tuple(dy_binder_t **binders_in_scope, dy_core_tuple_t tuple, dy_core_tuple_t *type)
{
    const dy_core_expr_t *e = tuple.sequents;
    const dy_core_expr_t *end = dy_array_past_end(e);

    size_t num_exprs = (size_t)(end - e);

    dy_core_expr_t *sequents = dy_array_create(sizeof *tuple.sequents, num_exprs);

    for (; e != end; ++e) {
        dy_core_expr_t new_e;
        if (!dy_type_of_expr(binders_in_scope, *e, &new_e)) {
            return false;
        }

        dy_array_add(&sequents, &new_e);
    }

    tuple.sequents = sequents;

    *type = tuple;
    return true;
}

bool dy_type_of_recursion(dy_binder_t **binders_in_scope, dy_core_recursion_t recursion, dy_core_recursion_t *type)
{
    static const dy_binder_t binder = {
        .tag = DY_BINDER_RECURSION
    };

    dy_array_add(binders_in_scope, &binder);

    dy_core_expr_t sequent;
    bool res = dy_type_of_expr(binders_in_scope, *recursion.sequent, &sequent);

    dy_array_remove_last(*binders_in_scope);

    if (!res) {
        return false;
    }

    recursion.sequent = dy_core_expr_new(sequent);

    *type = recursion;
    return true;
}

bool dy_type_of_simple(dy_binder_t **binders_in_scope, dy_core_simple_t simple, dy_core_simple_t *type)
{
    dy_core_expr_t sequent;
    if (!dy_type_of_expr(binders_in_scope, *simple.sequent, &sequent)) {
        return false;
    }

    type->prefix = simple.prefix;
    type->sequent = dy_core_expr_new(sequent);

    return true;
}

dy_core_expr_t dy_type_of_elim(dy_core_elim_t elim)
{
    return (dy_core_expr_t){
        .tag = DY_CORE_EXPR_COMP,
        .comp = {
            .expr = elim.result_type
        }
    };
}

bool dy_type_of_variable(dy_binder_t **binders_in_scope, size_t variable_index, dy_core_expr_t *type)
{
    dy_binder_t binder = *((const dy_binder_t *)dy_array_past_end(*binders_in_scope) - 1 - variable_index);

    switch (binder.tag) {
    case DY_BINDER_FUNCTION:
        if (binder.have_type) {
            bool have_result = false;
            if (!dy_adjust_free_refs_of_expr(binder.type, 0, variable_index + 1, DY_ADJUST_FREE_REFS_DIRECTION_DOWN, type, &have_result)) {
                return false;
            }

            if (!have_result) {
                *type = binder.type;
            }

            return true;
        } else {
            return false;
        }
    case DY_BINDER_RECURSION:
        *type = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_REF,
            .ref = variable_index
        };
        return true;
    case DY_BINDER_REC_ELIM: {
        bool have_result = false;
        if (!dy_adjust_free_refs_of_expr(binder.type, 0, variable_index + 1, DY_ADJUST_FREE_REFS_DIRECTION_DOWN, type, &have_result)) {
            return false;
        }

        if (!have_result) {
            *type = binder.type;
        }

        return true;
    }
    case DY_BINDER_INFERENCE_CTX:
        return false;
    }
}

bool dy_type_of_elim_ref(dy_binder_t **binders_in_scope, dy_core_elim_ref_t elim_ref, dy_core_expr_t *type)
{
    return dy_type_of_variable(binders_in_scope, elim_ref.index, type);
}

dy_core_expr_t dy_type_of_sequence(dy_core_sequence_t sequence)
{
    return (dy_core_expr_t){
        .tag = DY_CORE_EXPR_COMP,
        .comp = {
            .expr = sequence.type
        }
    };
}

bool dy_type_of_comp(dy_binder_t **binders_in_scope, dy_core_comp_t comp, dy_core_comp_t *type)
{
    dy_core_expr_t expr;
    if (!dy_type_of_expr(binders_in_scope, *comp.expr, &expr)) {
        return false;
    }

    comp.expr = dy_core_expr_new(expr);

    *type = comp;
    return true;
}

dy_core_expr_t dy_type_of_trap(void)
{
    return (dy_core_expr_t){
        .tag = DY_CORE_EXPR_COMP,
        .comp = {
            .expr = dy_core_expr_new((dy_core_expr_t){
                .tag = DY_CORE_EXPR_INTRO,
                .intro = {
                    .polarity = DY_POLARITY_POSITIVE,
                    .is_implicit = true,
                    .tag = DY_INTRO_COMPLEX,
                    .complex = {
                        .tag = DY_CORE_FUNCTION,
                        .function = {
                            .type = NULL,
                            .sequent = dy_core_expr_new((dy_core_expr_t){
                                .tag = DY_CORE_EXPR_REF,
                                .ref = 0
                            })
                        }
                    }
                }
            })
        }
    };
}
