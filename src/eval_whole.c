/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <duality/eval_whole.h>

#include "eval_step.h"
#include "substitute.h"

#include <duality/array.h>

bool dy_eval_whole(dy_core_expr_t expr, dy_core_expr_t *new_expr)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        if (expr.intro.is_complex) {
            switch (expr.intro.complex.tag) {
            case DY_CORE_FUNCTION: {
                bool have_new_type = false;
                if (expr.intro.complex.function.type != NULL) {
                    have_new_type = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.intro.complex.function.type);
                }

                bool have_new_e = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.intro.complex.function.sequent);

                if (!have_new_type && !have_new_e) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_TUPLE: {
                const dy_core_expr_t *first = expr.intro.complex.tuple.sequents;
                const dy_core_expr_t *one_past_last = dy_array_past_end(expr.intro.complex.tuple.sequents);

                size_t num_exprs = (size_t)(one_past_last - first);

                dy_core_expr_t *tuple = dy_array_create(sizeof *tuple, num_exprs);

                bool have_anything_new = false;

                for (size_t i = 0; i < num_exprs; ++i) {
                    dy_core_expr_t e;
                    if (dy_eval_whole(running_id, equal_variables, first[i], &e)) {
                        have_anything_new = true;
                        dy_array_add(&tuple, &e);
                    } else {
                        dy_array_add(&tuple, &first[i]);
                    }
                }

                if (!have_anything_new) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_RECURSION:
                if (!dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.intro.complex.recursion.sequent)) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
        } else {
            switch (expr.intro.simple.tag) {
            case DY_CORE_MAPPING: {
                bool have_new_expr = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.intro.simple.mapping.expr);

                bool have_new_sequent = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.intro.simple.mapping.sequent);

                if (!have_new_expr && !have_new_sequent) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_SELECTION:
                if (!dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.intro.simple.selection.sequent)) {
                    return false;
                }

                *new_expr = expr;

                return true;
            case DY_CORE_UNFOLDING:
                if (!dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.intro.simple.unfolding.sequent)) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
        }
    case DY_CORE_EXPR_ELIM: {
        bool have_new_e = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.expr);

        if (expr.elim.is_complex) {
            bool have_new_result_type = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.complex.result_type);

            switch (expr.elim.complex.tag) {
            case DY_CORE_FUNCTION: {
                bool have_new_type = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.complex.function.type);
                bool have_new_cont_type = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.complex.function.cont.type);
                bool have_new_cont_expr = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.complex.function.cont.sequent);

                if (!have_new_e && !have_new_type && !have_new_cont_type && !have_new_cont_expr && !have_new_result_type) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_TUPLE: {
                const dy_core_cont_with_check_status_t *src = expr.elim.complex.tuple.conts_with_check_status;
                const dy_core_cont_with_check_status_t *src_end = dy_array_past_end(expr.elim.complex.tuple.conts_with_check_status);

                size_t num_conts = (size_t)(src_end - src);

                dy_core_cont_with_check_status_t *conts_with_check_status = dy_array_create(sizeof *conts_with_check_status, num_conts);

                bool have_anything_new = false;
                for (size_t i = 0; i < num_conts; ++i) {
                    dy_array_add(&conts_with_check_status, &src[i]);

                    dy_core_expr_t type;
                    if (dy_eval_whole(running_id, equal_variables, *src[i].cont.type, &type)) {
                        conts_with_check_status[i].cont.type = dy_core_expr_new(type);
                        have_anything_new = true;
                    } else {
                        conts_with_check_status[i].cont.type = src[i].cont.type;
                    }

                    dy_core_expr_t e;
                    if (dy_eval_whole(running_id, equal_variables, *src[i].cont.sequent, &e)) {
                        conts_with_check_status[i].cont.sequent = dy_core_expr_new(e);
                        have_anything_new = true;
                    } else {
                        conts_with_check_status[i].cont.sequent = src[i].cont.sequent;
                    }
                }

                if (!have_anything_new && !have_new_result_type) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_RECURSION: {
                if (expr.elim.complex.recursion.is_only_id) {
                    if (!have_new_e && !have_new_result_type) {
                        return false;
                    }

                    *new_expr = expr;

                    return true;
                } else {
                    bool have_new_cont_type = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.complex.recursion.cont_with_inference_ctx.cont.type);

                    bool have_new_cont_expr = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.complex.recursion.cont_with_inference_ctx.cont.sequent);

                    if (!have_new_e && !have_new_cont_type && !have_new_cont_expr && !have_new_result_type) {
                        return false;
                    }

                    *new_expr = expr;

                    return true;
                }
            }
            }
        } else {
            switch (expr.elim.simple.tag) {
            case DY_CORE_MAPPING: {
                bool have_new_mapping_e = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.simple.mapping.expr);
                bool have_new_mapping_sequent = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.simple.mapping.sequent);

                if (!have_new_e && !have_new_mapping_e && !have_new_mapping_sequent) {
                    return false;
                }

                *new_expr = expr;

                return true;
            }
            case DY_CORE_SELECTION: {
                bool have_new_sequent = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.simple.selection.sequent);

                *new_expr = expr;

                return true;
            }
            case DY_CORE_UNFOLDING: {
                bool have_new_sequent = dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.elim.simple.unfolding.sequent);

                *new_expr = expr;

                return true;
            }
            }
        }
    }
    case DY_CORE_EXPR_COMP:
        if (!dy_eval_whole_ptr_in_place(running_id, equal_variables, &expr.comp.expr)) {
            return false;
        }

        *new_expr = expr;

        return true;
    case DY_CORE_EXPR_REF:
    case DY_CORE_EXPR_INFERENCE_VAR:
    case DY_CORE_EXPR_TRAP:
    case DY_CORE_EXPR_INFERENCE_CTX:
        return false;
    case DY_CORE_EXPR_SEQUENCE: {
        dy_core_expr_t new_first;
        bool have_new_first = dy_eval_step_expr(running_id, equal_variables, *expr.compose.first, &new_first);

        if (have_new_first) {
            dy_core_expr_t subst_second;
            bool have_subst_second = dy_substitute(running_id, equal_variables, *expr.compose.second, expr.compose.var.id, new_first, &subst_second);
            if (!have_subst_second) {
                subst_second = *expr.compose.second;
            }

            if (!dy_eval_whole(running_id, equal_variables, subst_second, new_expr)) {
                *new_expr = subst_second;
            }

            return true;
        } else {
            dy_core_expr_t evaled_second;
            if (dy_eval_step_expr(running_id, equal_variables, *expr.compose.second, &evaled_second)) {
                if (evaled_second.tag == DY_CORE_EXPR_REF && evaled_second.variable.id == expr.compose.var.id) {
                    if (dy_eval_whole(running_id, equal_variables, *expr.compose.first, new_expr)) {
                        return true;
                    } else {
                        *new_expr = *expr.compose.first;
                        return true;
                    }
                }
            }

            have_new_first = dy_eval_whole(running_id, equal_variables, *expr.compose.first, &new_first);

            dy_core_expr_t new_second;
            bool have_new_second = dy_eval_whole(running_id, equal_variables, *expr.compose.second, &new_second);

            if (!have_new_first && !have_new_second) {
                return false;
            }

            if (have_new_first) {
                expr.compose.first = dy_core_expr_new(new_first);
            }

            if (have_new_second) {
                expr.compose.second = dy_core_expr_new(new_second);
            }

            *new_expr = expr;

            return true;
        }
    }
    }
}

bool dy_eval_whole_ptr(dy_core_expr_t *expr, dy_core_expr_t **result)
{
    dy_core_expr_t e;
    if (!dy_eval_whole(*expr, &e)) {
        return false;
    }

    *result = dy_core_expr_new(e);

    return true;
}

bool dy_eval_whole_ptr_in_place(dy_core_expr_t **expr)
{
    return dy_eval_whole_ptr(*expr, expr);
}
