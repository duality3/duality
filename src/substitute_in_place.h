/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

static inline
bool dy_substitute_expr_in_place(dy_core_expr_t *expr, size_t index, dy_core_expr_t sub, bool *did_substitute);

static inline
bool dy_substitute_intro_in_place(dy_core_intro_t *intro, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_complex_in_place(dy_core_complex_t *complex, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_function_in_place(dy_core_function_t *function, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_tuple_in_place(dy_core_tuple_t *tuple, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_recursion_in_place(dy_core_recursion_t *recursion, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_simple_prefix_in_place(dy_core_simple_prefix_t *simple, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_simple_in_place(dy_core_simple_t *simple, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_elim_in_place(dy_core_elim_t *elim, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_complex_eliminator_in_place(dy_core_complex_eliminator_t *complex_elim, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_function_eliminator_in_place(dy_core_function_eliminator_t *function_elim, size_t index, dy_core_expr_t sub,  bool *have_result);

static inline
bool dy_substitute_tuple_eliminator_in_place(dy_core_tuple_eliminator_t *tuple_elim, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_recursion_eliminator_in_place(dy_core_recursion_eliminator_t *recursion_elim, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_variable_in_place(size_t *variable_index, dy_core_expr_t *expr, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_elim_ref_in_place(dy_core_elim_ref_t *elim_ref, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_sequence_in_place(dy_core_sequence_t *sequence, size_t index, dy_core_expr_t sub, bool *have_result);

static inline
bool dy_substitute_comp_in_place(dy_core_comp_t *comp, size_t index, dy_core_expr_t sub, bool *have_result);
