/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <duality/array.h>

#include <string.h>

#include "checked_arith.h"

void *dy_array_create(void *alloc_env, dy_alloc_fn alloc, size_t elem_size, size_t align, size_t capacity)
{
    size_t capacity_in_bytes;
    if (dy_size_t_mul_overflow(elem_size, capacity, &capacity_in_bytes)) {
        return NULL;
    }

    void *ptr = alloc(alloc_env, capacity_in_bytes, align);
    if (ptr == NULL) {
        return NULL;
    }

   return ptr;
}

void dy_array_destroy(void *env, dy_free_fn free, void *array, size_t elem_size, size_t capacity)
{
    size_t capacity_in_bytes;
    if (dy_size_t_mul_overflow(elem_size, capacity, &capacity_in_bytes)) {
        return;
    }

    free(env, array, capacity_in_bytes);
}

void *dy_array_add_capacity(void *env, dy_alloc_fn alloc, dy_resize_fn resize, void **array_ptr, size_t elem_size, size_t num_elems, size_t offset)
{
    size_t old_size_in_bytes;
    if (dy_size_t_mul_overflow(elem_size, num_elems, &old_size_in_bytes)) {
        return NULL;
    }

    if (dy_size_t_add_overflow(num_elems, offset, &num_elems)) {
        return NULL;
    }

    size_t new_size_in_bytes;
    if (dy_size_t_mul_overflow(num_elems, elem_size, &new_size_in_bytes)) {
        return NULL:
    }

    if (resize(env, *array_ptr, old_size_in_bytes, new_size_in_bytes)) {

    }
}

void *dy_array_add(void *array_ptr, const void *value_ptr, dy_alloc_fn alloc, dy_alloc_resize_fn resize, dy_alloc_past_end_fn past_end)
{
    dy_array_header_t *header = (*(dy_array_header_t **)array_ptr) - 1;

    void *alloc_end = past_end(header);

    if (header->elems_end == alloc_end) {
        size_t old_num_bytes = (size_t)((const char *)alloc_end - (char *)header);

        if (!resize(header, (ptrdiff_t)old_num_bytes)) {
            size_t new_num_bytes;
            if (dy_size_t_add_overflow(old_num_bytes, old_num_bytes, &new_num_bytes)) {
                return NULL;
            }

            header = alloc(env, new_num_bytes, )
        }

        header = resize(header, new_num_bytes);

        if (header == NULL) {
            return NULL; // TODO: Retry with smaller factor.
        }

        header->elems_end = (char *)header + old_num_bytes;

        *(void **)array_ptr = header + 1;
    }

    char *new_slot = header->elems_end;

    memmove(new_slot, value_ptr, header->elem_size);

    header->elems_end = new_slot + header->elem_size;

    return new_slot;
}

void dy_array_remove(void *array, void *value)
{
    dy_array_header_t *header = (dy_array_header_t *)array - 1;

    header->elems_end = (char *)header->elems_end - header->elem_size;

    memmove(value, header->elems_end, header->elem_size);
}

void dy_array_remove_last(void *array)
{
    dy_array_header_t *header = (dy_array_header_t *)array - 1;

    header->elems_end = (char *)header->elems_end - header->elem_size;
}

void *dy_array_last(const void *array)
{
    const dy_array_header_t *header = (const dy_array_header_t *)array - 1;

    if (header->elems_end == array) {
        return NULL;
    } else {
        return (char *)header->elems_end - header->elem_size;
    }
}

const void *dy_array_past_end(const void *array)
{
    const dy_array_header_t *header = (const dy_array_header_t *)array - 1;

    return header->elems_end;
}

void dy_array_set_end(void *array, void *new_end)
{
    dy_array_header_t *header = (dy_array_header_t *)array - 1;

    header->elems_end = new_end;
}
