/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

typedef union dy_substitute {
    dy_core_expr_t expr;
    struct {
        dy_core_function_t cont;
        bool is_implicit;
        dy_core_expr_t *result_type;
    };
} dy_substitute_t;

static inline
bool dy_substitute_expr_with_expr(dy_core_expr_t expr, size_t index, dy_core_expr_t sub, dy_core_expr_t *result, bool *have_result);

static inline
bool dy_substitute_expr(dy_core_expr_t expr, size_t index, dy_substitute_t sub, dy_core_expr_t *result, bool *have_result);

static inline
bool dy_substitute_intro(dy_core_intro_t intro, size_t index, dy_substitute_t sub, dy_core_intro_t *result, bool *have_result);

static inline
bool dy_substitute_complex(dy_core_complex_t complex, size_t index, dy_substitute_t sub, dy_core_complex_t *result, bool *have_result);

static inline
bool dy_substitute_function(dy_core_function_t function, size_t index, dy_substitute_t sub, dy_core_function_t *result, bool *have_result);

static inline
bool dy_substitute_tuple(dy_core_tuple_t tuple, size_t index, dy_substitute_t sub, dy_core_tuple_t *result, bool *have_result);

static inline
bool dy_substitute_recursion(dy_core_recursion_t recursion, size_t index, dy_substitute_t sub, dy_core_recursion_t *result, bool *have_result);

static inline
bool dy_substitute_simple_prefix(dy_core_simple_prefix_t simple, size_t index, dy_substitute_t sub, dy_core_simple_prefix_t *result, bool *have_result);

static inline
bool dy_substitute_simple(dy_core_simple_t simple, size_t index, dy_substitute_t sub, dy_core_simple_t *result, bool *have_result);

static inline
bool dy_substitute_elim(dy_core_elim_t elim, size_t index, dy_substitute_t sub, dy_core_elim_t *result, bool *have_result);

static inline
bool dy_substitute_complex_eliminator(dy_core_complex_eliminator_t complex_elim, size_t index, dy_substitute_t sub, dy_core_complex_eliminator_t *result, bool *have_result);

static inline
bool dy_substitute_function_eliminator(dy_core_function_eliminator_t function_elim, size_t index, dy_substitute_t sub, dy_core_function_eliminator_t *result, bool *have_result);

static inline
bool dy_substitute_tuple_eliminator(dy_core_tuple_eliminator_t tuple_elim, size_t index, dy_substitute_t sub, dy_core_tuple_eliminator_t *result, bool *have_result);

static inline
bool dy_substitute_recursion_eliminator(dy_core_recursion_eliminator_t recursion_elim, size_t index, dy_substitute_t sub, dy_core_recursion_eliminator_t *result, bool *have_result);

static inline
bool dy_substitute_variable(size_t variable_index, size_t index, dy_substitute_t sub, dy_core_expr_t *result, bool *have_result);

static inline
bool dy_substitute_elim_ref(dy_core_elim_ref_t elim_ref, size_t index, dy_substitute_t sub, dy_core_expr_t *result, bool *have_result);

static inline
bool dy_substitute_sequence(dy_core_sequence_t sequence, size_t index, dy_substitute_t sub, dy_core_sequence_t *result, bool *have_result);

static inline
bool dy_substitute_comp(dy_core_comp_t comp, size_t index, dy_substitute_t sub, dy_core_comp_t *result, bool *have_result);
