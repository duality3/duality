/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "ref_occurs_in.h"

#include <duality/array.h>

bool dy_ref_occurs_in_expr(size_t ref, dy_core_expr_t expr)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        switch (expr.intro.tag) {
        case DY_INTRO_COMPLEX:
            switch (expr.intro.complex.tag) {
            case DY_CORE_FUNCTION:
                if (expr.intro.complex.function.type != NULL) {
                    if (dy_ref_occurs_in_expr(ref, *expr.intro.complex.function.type)) {
                        return true;
                    }
                }

                return dy_ref_occurs_in_expr(ref + 1, *expr.intro.complex.function.sequent);
            case DY_CORE_TUPLE:
                for (const dy_core_expr_t *e = expr.intro.complex.tuple.sequents, *end = dy_array_past_end(e); e != end; ++ e) {
                    if (dy_ref_occurs_in_expr(ref, *e)) {
                        return true;
                    }
                }

                return false;
            case DY_CORE_RECURSION:
                return dy_ref_occurs_in_expr(ref + 1, *expr.intro.complex.recursion.sequent);
            }
        case DY_INTRO_SIMPLE:
            if (expr.intro.simple.prefix.tag == DY_CORE_MAPPING) {
                if (dy_ref_occurs_in_expr(ref, *expr.intro.simple.prefix.expr)) {
                    return true;
                }
            }

            return dy_ref_occurs_in_expr(ref, *expr.intro.simple.sequent);
        }
    case DY_CORE_EXPR_ELIM:
        if (dy_ref_occurs_in_expr(ref, *expr.elim.expr) || dy_ref_occurs_in_expr(ref, *expr.elim.result_type)) {
            return true;
        }

        switch (expr.elim.tag) {
        case DY_INTRO_COMPLEX:
            switch (expr.elim.complex.tag) {
            case DY_CORE_FUNCTION:
                if (expr.elim.complex.function.type != NULL) {
                    if (dy_ref_occurs_in_expr(ref, *expr.elim.complex.function.type)) {
                        return true;
                    }
                }

                return dy_ref_occurs_in_expr(ref + 1, *expr.elim.complex.function.cont.type)
                    || dy_ref_occurs_in_expr(ref + 2, *expr.elim.complex.function.cont.sequent);
            case DY_CORE_TUPLE:
                for (const dy_core_cont_with_check_status_t *c = expr.elim.complex.tuple.conts_with_check_status, *end = dy_array_past_end(c); c != end; ++c) {
                    if (dy_ref_occurs_in_expr(ref, *c->cont.type) || dy_ref_occurs_in_expr(ref + 1, *c->cont.sequent)) {
                        return true;
                    }
                }

                return false;
            case DY_CORE_RECURSION:
                return dy_ref_occurs_in_expr(ref, *expr.elim.complex.recursion.cont.type)
                    || dy_ref_occurs_in_expr(ref + 1, *expr.elim.complex.recursion.cont.sequent);
            }
        case DY_INTRO_SIMPLE:
            if (expr.elim.simple_prefix.tag == DY_CORE_MAPPING) {
                return dy_ref_occurs_in_expr(ref, *expr.elim.simple_prefix.expr);
            } else {
                return false;
            }
        }
    case DY_CORE_EXPR_REF:
        return expr.ref == ref;
    case DY_CORE_EXPR_ELIM_REF:
        return dy_ref_occurs_in_expr(ref, *expr.elim_ref.expr) || expr.elim_ref.index == ref;
    case DY_CORE_EXPR_COMP:
        return dy_ref_occurs_in_expr(ref, *expr.comp.expr);
    case DY_CORE_EXPR_SEQUENCE:
        return dy_ref_occurs_in_expr(ref, *expr.sequence.expr)
            || dy_ref_occurs_in_expr(ref, *expr.sequence.type)
            || dy_ref_occurs_in_expr(ref, *expr.sequence.cont.type)
            || dy_ref_occurs_in_expr(ref + 1, *expr.sequence.cont.sequent);
    case DY_CORE_EXPR_INFERENCE_CTX:
        return dy_ref_occurs_in_expr(ref + 1, *expr.inference_ctx.expr);
    case DY_CORE_EXPR_TRAP:
        return false;
    }
}
