/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

#include "checked_arith.h"

typedef enum dy_adjust_free_refs_direction {
    DY_ADJUST_FREE_REFS_DIRECTION_UP,
    DY_ADJUST_FREE_REFS_DIRECTION_DOWN
} dy_adjust_free_vars_direction_t;

static inline
bool dy_adjust_free_refs_of_expr(dy_core_expr_t expr, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_expr_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_expr_ptr(const dy_core_expr_t *expr, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_expr_t **result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_intro(dy_core_intro_t intro, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_intro_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_complex(dy_core_complex_t complex, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_complex_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_function(dy_core_function_t function, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_function_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_tuple(dy_core_tuple_t tuple, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_tuple_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_recursion(dy_core_recursion_t recursion, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_recursion_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_simple_prefix(dy_core_simple_prefix_t prefix, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_simple_prefix_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_simple(dy_core_simple_t simple, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_simple_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_elim(dy_core_elim_t elim, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_elim_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_complex_eliminator(dy_core_complex_eliminator_t eliminator, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_complex_eliminator_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_function_eliminator(dy_core_function_eliminator_t function, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_function_eliminator_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_tuple_eliminator(dy_core_tuple_eliminator_t tuple, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_tuple_eliminator_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_recursion_eliminator(dy_core_recursion_eliminator_t recursion, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_recursion_eliminator_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_ref(size_t ref, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, size_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_elim_ref(dy_core_elim_ref_t elim_ref, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_elim_ref_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_comp(dy_core_comp_t comp, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_comp_t *result, bool *have_result);

static inline
bool dy_adjust_free_refs_of_sequence(dy_core_sequence_t sequence, size_t free_ref_cutoff, size_t offset, dy_adjust_free_vars_direction_t direction, dy_core_sequence_t *result, bool *have_result);
