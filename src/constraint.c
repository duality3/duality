/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <duality/constraint.h>

#include "adjust_free_refs.h"
#include "ref_occurs_in.h"

#include <duality/array.h>

size_t dy_constraint_float_offset(const dy_constraint_t *start, const dy_constraint_t *end)
{
    size_t highest_capturing_index = 0;

    const dy_constraint_t *c = NULL;

    for (const dy_constraint_t *current = start; current != end; ++current) {
        if (c->index == 0) {
            c = current;
            break;
        }
    }

    if (c == NULL) {
        return highest_capturing_index;
    }

    for (const dy_constraint_t *current = start; current != end; ++current) {
        if (current->index == 0) {
            continue;
        }

        bool found = false;

        const dy_core_expr_t *lower_bounds_end = dy_array_past_end(current->lower_bounds);
        for (const dy_core_expr_t *lower = current->lower_bounds; lower != lower_bounds_end; ++lower) {
            if (dy_ref_occurs_in_expr(0, *lower)) {
                if (current->index > highest_capturing_index) {
                    highest_capturing_index = current->index;
                    found = true;
                    break;
                }
            }
        }

        if (found) {
            continue;
        }

        const dy_core_expr_t *upper_bounds_end = dy_array_past_end(current->upper_bounds);
        for (const dy_core_expr_t *upper = current->upper_bounds; upper != upper_bounds_end; ++upper) {
            if (dy_ref_occurs_in_expr(0, *upper_bounds_end)) {
                if (current->index > highest_capturing_index) {
                    highest_capturing_index = current->index;
                    break;
                }
            }
        }
    }

    return highest_capturing_index;
}

void dy_constraint_get(dy_constraint_t *constraints, dy_constraint_t *start, dy_core_expr_t **lower, dy_core_expr_t **upper)
{
    const dy_constraint_t *end = dy_array_past_end(constraints);
    for (dy_constraint_t *c = start; c != end; ++c) {
        if (c->index != 0) {
            continue;
        }

        if (c->lower_bounds != dy_array_past_end(c->lower_bounds)) {
            *lower = c->lower_bounds;
        } else {
            *lower = NULL;
        }

        if (c->upper_bounds != dy_array_past_end(c->upper_bounds)) {
            *upper = c->upper_bounds;
        } else {
            *upper = NULL;
        }

        return;
    }

    *lower = NULL;
    *upper = NULL;
}

void dy_move_constraints_up(dy_constraint_t *constraints, dy_constraint_t *start)
{
    for (dy_constraint_t *c = start; c != dy_array_past_end(constraints); ++c) {
        // __builtin_assume(c->index != 0);

        c->index--;

        for (dy_core_expr_t *e = c->lower_bounds; e != dy_array_past_end(c->lower_bounds); ++e) {
            dy_core_expr_t new_e;
            bool have_new_e = false;
            if (!dy_adjust_free_refs_of_expr(*e, 0, 1, DY_ADJUST_FREE_REFS_DIRECTION_UP, &new_e, &have_new_e)) {
                dy_array_remove(c->lower_bounds, e);
                --e;

                continue;
            }

            if (have_new_e) {
                *e = new_e;
            }
        }

        for (dy_core_expr_t *e = c->upper_bounds; e != dy_array_past_end(c->upper_bounds); ++e) {
            dy_core_expr_t new_e;
            bool have_new_e = false;
            if (!dy_adjust_free_refs_of_expr(*e, 0, 1, DY_ADJUST_FREE_REFS_DIRECTION_UP, &new_e, &have_new_e)) {
                dy_array_remove(c->upper_bounds, e);
                --e;

                continue;
            }

            if (have_new_e) {
                *e = new_e;
            }
        }
    }
}

void dy_join_constraints(dy_constraint_t *constraints, dy_constraint_t *start1, dy_constraint_t *start2)
{
    for (dy_constraint_t *c = start2; c != dy_array_past_end(constraints); ++c) {
        bool merged = false;

        for (dy_constraint_t *c2 = start1; c2 != c; ++c2) {
            if (c->index != c2->index) {
                continue;
            }

            const dy_core_expr_t *lower_bounds_end = dy_array_past_end(c->lower_bounds);
            for (dy_core_expr_t *bound = c->lower_bounds; bound != lower_bounds_end; ++bound) {
                dy_array_add(&c2->lower_bounds, bound);
            }

            const dy_core_expr_t *upper_bounds_end = dy_array_past_end(c->upper_bounds);
            for (dy_core_expr_t *bound = c->upper_bounds; bound != upper_bounds_end; ++bound) {
                dy_array_add(&c2->upper_bounds, bound);
            }

            merged = true;

            break;
        }

        if (merged) {
            dy_array_remove(constraints, c);
            --c;
        }
    }
}

void dy_free_constraints_from(dy_constraint_t *constraints, dy_constraint_t *start, const dy_constraint_t *end)
{
    for (dy_constraint_t *c = start; c != end; ++c) {
        dy_array_remove(constraints, c);
    }
}

bool dy_constraint_promote_list(const dy_core_expr_t *first, const dy_core_expr_t *one_past_last, dy_polarity_t polarity, dy_core_expr_t *result)
{
    dy_core_expr_t *promoted_list = dy_array_create(sizeof *promoted_list, (size_t)(one_past_last - first));

    size_t current = 1;

    for (const dy_core_expr_t *e = first; e != one_past_last;) {
        if (e->tag != DY_CORE_EXPR_INTRO || e->intro.tag == DY_INTRO_COMPLEX || e->intro.simple.prefix.tag != DY_CORE_SELECTION || e->intro.polarity != polarity) {
            // TODO: free promoted_list
            return false;
        }

        if (e->intro.simple.prefix.index == current) {
            dy_array_add(&promoted_list, e->intro.simple.sequent);

            ++current;

            e = first;
        } else {
            e++;
        }
    }

    if (first + current != one_past_last) {
        // TODO: free promoted_list
        return false;
    }

    *result = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .is_implicit = true,
            .polarity = polarity,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_TUPLE,
                .tuple = {
                    .sequents = promoted_list
                }
            }
        }
    };

    return true;
}

size_t dy_constraints_length(const dy_constraint_t *constraints)
{
    const dy_constraint_t *end = dy_array_past_end(constraints);

    return (size_t)(end - constraints);
}
