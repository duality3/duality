/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

static inline
bool dy_type_of_expr(dy_binder_t **binders_in_scope, dy_core_expr_t expr, dy_core_expr_t *type);

static inline
bool dy_type_of_intro(dy_binder_t **binders_in_scope, dy_core_intro_t intro, dy_core_intro_t *type);

static inline
bool dy_type_of_complex(dy_binder_t **binders_in_scope, dy_core_complex_t complex, dy_core_complex_t *type);

static inline
bool dy_type_of_function(dy_binder_t **binders_in_scope, dy_core_function_t function, dy_core_function_t *type);

static inline
bool dy_type_of_tuple(dy_binder_t **binders_in_scope, dy_core_tuple_t tuple, dy_core_tuple_t *type);

static inline
bool dy_type_of_recursion(dy_binder_t **binders_in_scope, dy_core_recursion_t recursion, dy_core_recursion_t *type);

static inline
bool dy_type_of_simple(dy_binder_t **binders_in_scope, dy_core_simple_t simple, dy_core_simple_t *type);

static inline
dy_core_expr_t dy_type_of_elim(dy_core_elim_t elim);

static inline
bool dy_type_of_variable(dy_binder_t **binders_in_scope, size_t variable_index, dy_core_expr_t *type);

static inline
bool dy_type_of_elim_ref(dy_binder_t **binders_in_scope, dy_core_elim_ref_t elim_ref, dy_core_expr_t *type);

static inline
dy_core_expr_t dy_type_of_sequence(dy_core_sequence_t sequence);

static inline
bool dy_type_of_comp(dy_binder_t **binders_in_scope, dy_core_comp_t comp, dy_core_comp_t *type);

static inline
dy_core_expr_t dy_type_of_trap(void);
