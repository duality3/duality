/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "substitute.h"

#include "adjust_free_refs.h"

#include <duality/array.h>

bool dy_substitute_expr_with_expr(dy_core_expr_t expr, size_t index, dy_core_expr_t sub, dy_core_expr_t *result, bool *have_result)
{
    return dy_substitute_expr(expr, index, (dy_substitute_t){ .expr = sub }, result, have_result);
}

bool dy_substitute_expr(dy_core_expr_t expr, size_t index, dy_substitute_t sub, dy_core_expr_t *result, bool *have_result)
{
    bool res;

    switch (expr.tag) {
    case DY_CORE_EXPR_INTRO:
        res = dy_substitute_intro(expr.intro, index, sub, &result->intro, have_result);
        break;
    case DY_CORE_EXPR_ELIM:
        res = dy_substitute_elim(expr.elim, index, sub, &result->elim, have_result);
        break;
    case DY_CORE_EXPR_REF:
        return dy_substitute_variable(expr.ref, index, sub, result, have_result);
    case DY_CORE_EXPR_ELIM_REF:
        return dy_substitute_elim_ref(expr.elim_ref, index, sub, result, have_result);
    case DY_CORE_EXPR_SEQUENCE:
        res = dy_substitute_sequence(expr.sequence, index, sub, &result->sequence, have_result);
        break;
    case DY_CORE_EXPR_COMP:
        res = dy_substitute_comp(expr.comp, index, sub, &result->comp, have_result);
        break;
    case DY_CORE_EXPR_TRAP:
        res = false;
        break;
    case DY_CORE_EXPR_INFERENCE_CTX:
        __builtin_unreachable();
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = expr.tag;
    }

    return true;
}

bool dy_substitute_intro(dy_core_intro_t intro, size_t index, dy_substitute_t sub, dy_core_intro_t *result, bool *have_result)
{
    bool res;

    switch (intro.tag) {
    case DY_INTRO_COMPLEX:
        res = dy_substitute_complex(intro.complex, index, sub, &result->complex, have_result);
        break;
    case DY_INTRO_SIMPLE:
        res = dy_substitute_simple(intro.simple, index, sub, &result->simple, have_result);
        break;
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = intro.tag;
    }

    return true;
}

bool dy_substitute_complex(dy_core_complex_t complex, size_t index, dy_substitute_t sub, dy_core_complex_t *result, bool *have_result)
{
    bool res;

    switch (complex.tag) {
    case DY_CORE_FUNCTION:
        res = dy_substitute_function(complex.function, index, sub, &result->function, have_result);
        break;
    case DY_CORE_TUPLE:
        res = dy_substitute_tuple(complex.tuple, index, sub, &result->tuple, have_result);
        break;
    case DY_CORE_RECURSION:
        res = dy_substitute_recursion(complex.recursion, index, sub, &result->recursion, have_result);
        break;
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = complex.tag;
    }

    return true;
}

bool dy_substitute_function(dy_core_function_t function, size_t index, dy_substitute_t sub, dy_core_function_t *result, bool *have_result)
{
    bool have_type = false;
    dy_core_expr_t type;

    if (function.type != NULL) {
        if (!dy_substitute_expr(*function.type, index, sub, &type, &have_type)) {
            return false;
        }
    }

    dy_core_expr_t sequent;
    bool have_sequent;
    if (!dy_substitute_expr(*function.sequent, index + 1, sub, &sequent, &have_sequent)) {
        return false;
    }

    if (have_type) {
        result->type = dy_core_expr_new(type);
    }

    if (have_sequent) {
        result->sequent = dy_core_expr_new(sequent);
    }

    *have_result = have_type || have_sequent;
    return true;
}

bool dy_substitute_tuple(dy_core_tuple_t tuple, size_t index, dy_substitute_t sub, dy_core_tuple_t *result, bool *have_result)
{
    const dy_core_expr_t *e = tuple.sequents;
    const dy_core_expr_t *end = dy_array_past_end(e);

    size_t num_elems = (size_t)(end - e);

    dy_core_expr_t *sequents = dy_array_create(sizeof *tuple.sequents, num_elems);

    bool have_anything_new = false;

    for (; e != end; ++e) {
        dy_core_expr_t new_e;
        bool have_new_e;
        if (!dy_substitute_expr(*e, index, sub, &new_e, &have_new_e)) {
            return false;
        }
        if (have_new_e) {
            have_anything_new = true;
            dy_array_add(&sequents, &new_e);
        } else {
            dy_array_add(&sequents, e);
        }
    }

    if (have_anything_new) {
        result->sequents = sequents;
    }

    *have_result = have_anything_new;

    return true;
}

bool dy_substitute_recursion(dy_core_recursion_t recursion, size_t index, dy_substitute_t sub, dy_core_recursion_t *result, bool *have_result)
{
    dy_core_expr_t sequent;
    if (!dy_substitute_expr(*recursion.sequent, index + 1, sub, &sequent, have_result)) {
        return false;
    }

    if (*have_result) {
        result->sequent = dy_core_expr_new(sequent);
    }

    return true;
}

bool dy_substitute_simple_prefix(dy_core_simple_prefix_t simple, size_t index, dy_substitute_t sub, dy_core_simple_prefix_t *result, bool *have_result)
{
    if (simple.tag != DY_CORE_MAPPING) {
        *have_result = false;
        return true;
    }

    dy_core_expr_t expr;
    if (!dy_substitute_expr(*simple.expr, index, sub, &expr, have_result)) {
        return false;
    }

    if (*have_result) {
        result->expr = dy_core_expr_new(expr);
        result->is_computationally_irrelevant = simple.is_computationally_irrelevant;
    }

    return true;
}

bool dy_substitute_simple(dy_core_simple_t simple, size_t index, dy_substitute_t sub, dy_core_simple_t *result, bool *have_result)
{
    bool have_simple_prefix;
    if (!dy_substitute_simple_prefix(simple.prefix, index, sub, &result->prefix, &have_simple_prefix)) {
        return false;
    }

    bool have_sequent;
    dy_core_expr_t sequent;
    if (!dy_substitute_expr(*simple.sequent, index, sub, &sequent, &have_sequent)) {
        return false;
    }

    *have_result = have_simple_prefix || have_sequent;
    return true;
}

bool dy_substitute_elim(dy_core_elim_t elim, size_t index, dy_substitute_t sub, dy_core_elim_t *result, bool *have_result)
{
    dy_core_expr_t expr;
    bool have_expr;
    if (!dy_substitute_expr(*elim.expr, index, sub, &expr, &have_expr)) {
        return false;
    }

    dy_core_expr_t result_type;
    bool have_result_type;
    if (!dy_substitute_expr(*elim.result_type, index, sub, &result_type, &have_result_type)) {
        return false;
    }

    bool have_simple_or_complex;
    switch (elim.tag) {
    case DY_INTRO_COMPLEX:
        if (!dy_substitute_complex_eliminator(elim.complex, index, sub, &result->complex, &have_simple_or_complex)) {
            return false;
        }

        break;
    case DY_INTRO_SIMPLE:
        if (!dy_substitute_simple_prefix(elim.simple_prefix, index, sub, &result->simple_prefix, &have_simple_or_complex)) {
            return false;
        }

        break;
    }

    if (have_expr) {
        result->expr = dy_core_expr_new(expr);
    }

    if (have_result_type) {
        result->result_type = dy_core_expr_new(result_type);
    }

    *have_result = have_expr || have_result_type || have_simple_or_complex;

    if (*have_result) {
        result->check_result = elim.check_result;
        result->is_implicit = elim.is_implicit;
    }

    return true;
}

bool dy_substitute_complex_eliminator(dy_core_complex_eliminator_t complex_elim, size_t index, dy_substitute_t sub, dy_core_complex_eliminator_t *result, bool *have_result)
{
    bool res;

    switch (complex_elim.tag) {
    case DY_CORE_FUNCTION:
        res = dy_substitute_function_eliminator(complex_elim.function, index, sub, &result->function, have_result);
        break;
    case DY_CORE_TUPLE:
        res = dy_substitute_tuple_eliminator(complex_elim.tuple, index, sub, &result->tuple, have_result);
        break;
    case DY_CORE_RECURSION:
        res = dy_substitute_recursion_eliminator(complex_elim.recursion, index, sub, &result->recursion, have_result);
        break;
    }

    if (!res) {
        return false;
    }

    if (*have_result) {
        result->tag = complex_elim.tag;
    }

    return true;
}

bool dy_substitute_function_eliminator(dy_core_function_eliminator_t function_elim, size_t index, dy_substitute_t sub, dy_core_function_eliminator_t *result, bool *have_result)
{
    bool have_type = false;
    dy_core_expr_t type;

    if (function_elim.type != NULL) {
        if (!dy_substitute_expr(*function_elim.type, index, sub, &type, &have_type)) {
            return false;
        }
    }

    bool have_cont;
    if (!dy_substitute_function(function_elim.cont, index + 1, sub, &result->cont, &have_cont)) {
        return false;
    }

    if (have_type) {
        function_elim.type = dy_core_expr_new(type);
    }

    *have_result = have_type || have_cont;

    return true;
}

bool dy_substitute_tuple_eliminator(dy_core_tuple_eliminator_t tuple_elim, size_t index, dy_substitute_t sub, dy_core_tuple_eliminator_t *result, bool *have_result)
{
    const dy_core_cont_with_check_status_t *cont = tuple_elim.conts_with_check_status;
    const dy_core_cont_with_check_status_t *end = dy_array_past_end(cont);

    size_t num_conts = (size_t)(end - cont);

    dy_core_cont_with_check_status_t *conts = dy_array_create(sizeof *conts, num_conts);

    bool have_anything_new = false;

    for (; cont != end; ++cont) {
        dy_core_cont_with_check_status_t cont_with_status = *cont;

        bool have_cont;
        if (!dy_substitute_function(cont->cont, index, sub, &cont_with_status.cont, &have_cont)) {
            return false;
        }
        if (have_cont) {
            have_anything_new = true;
        }

        dy_array_add(&conts, cont);
    }

    if (have_anything_new) {
        result->conts_with_check_status = conts;
    }

    *have_result = have_anything_new;

    return true;
}

bool dy_substitute_recursion_eliminator(dy_core_recursion_eliminator_t recursion_elim, size_t index, dy_substitute_t sub, dy_core_recursion_eliminator_t *result, bool *have_result)
{
    if (!dy_substitute_function(recursion_elim.cont, index + 1, sub, &result->cont, have_result)) {
        return false;
    }

    if (*have_result) {
        result->check_result = recursion_elim.check_result;
    }

    return true;
}

bool dy_substitute_variable(size_t variable_index, size_t index, dy_substitute_t sub, dy_core_expr_t *result, bool *have_result)
{
    if (variable_index != index) {
        *have_result = false;
        return true;
    }

    if (!dy_adjust_free_refs_of_expr(sub.expr, 0, index, DY_ADJUST_FREE_REFS_DIRECTION_DOWN, result, have_result)) {
        return false;
    }

    if (!*have_result) {
        *result = sub.expr;
        *have_result = true;
    }

    return true;
}

bool dy_substitute_elim_ref(dy_core_elim_ref_t elim_ref, size_t index, dy_substitute_t sub, dy_core_expr_t *result, bool *have_result)
{
    bool have_expr;
    dy_core_expr_t expr;
    if (!dy_substitute_expr(*elim_ref.expr, index, sub, &expr, &have_expr)) {
        return false;
    }

    if (elim_ref.index != index) {
        if (have_expr) {
            elim_ref.expr = dy_core_expr_new(expr);
            *result = (dy_core_expr_t){
                .tag = DY_CORE_EXPR_ELIM_REF,
                .elim_ref = elim_ref
            };
        }
        *have_result = have_expr;
        return true;
    }

    bool have_cont;
    dy_core_function_t cont;
    if (!dy_adjust_free_refs_of_function(sub.cont, 0, elim_ref.index, DY_ADJUST_FREE_REFS_DIRECTION_DOWN, &cont, &have_cont)) {
        return false;
    }

    bool have_result_type;
    dy_core_expr_t result_type;
    if (!dy_adjust_free_refs_of_expr(*sub.result_type, 0, elim_ref.index, DY_ADJUST_FREE_REFS_DIRECTION_DOWN, &result_type, &have_result_type)) {
        return false;
    }

    *result = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_ELIM,
        .elim = {
            .expr = have_expr ? dy_core_expr_new(expr) : elim_ref.expr,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = {
                    .num_inference_ctxs = 0,
                    .cont = have_cont ? cont : sub.cont,
                    .check_result = DY_CHECK_STATUS_OK // TODO
                }
            },
            .is_implicit = sub.is_implicit,
            .result_type = have_result_type ? dy_core_expr_new(result_type) : sub.result_type,
            .check_result = elim_ref.check_result
        }
    };
    *have_result = true;
    return true;
}

bool dy_substitute_sequence(dy_core_sequence_t sequence, size_t index, dy_substitute_t sub, dy_core_sequence_t *result, bool *have_result)
{
    dy_core_expr_t expr;
    bool have_expr;
    if (!dy_substitute_expr(*sequence.expr, index, sub, &expr, &have_expr)) {
        return false;
    }

    bool have_cont;
    if (!dy_substitute_function(sequence.cont, index, sub, &result->cont, &have_cont)) {
        return false;
    }

    dy_core_expr_t type;
    bool have_type;
    if (!dy_substitute_expr(*sequence.type, index, sub, &type, &have_type)) {
        return false;
    }

    if (have_expr) {
        result->expr = dy_core_expr_new(expr);
    }

    if (have_type) {
        result->type = dy_core_expr_new(type);
    }

    *have_result = have_expr || have_cont || have_type;

    if (*have_result) {
        result->expr_result = sequence.expr_result;
        result->type_result = sequence.type_result;
    }

    return true;
}

bool dy_substitute_comp(dy_core_comp_t comp, size_t index, dy_substitute_t sub, dy_core_comp_t *result, bool *have_result)
{
    dy_core_expr_t expr;
    if (!dy_substitute_expr(*comp.expr, index, sub, &expr, have_result)) {
        return false;
    }

    if (have_result) {
        result->expr = dy_core_expr_new(expr);
    }

    return true;
}
