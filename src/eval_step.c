/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "eval_step.h"

#include "substitute.h"

#include <assert.h>

static inline bool dy_eval_step_complex_elim(dy_core_simple_t simple, dy_core_complex_eliminator_t eliminator, bool is_implicit, dy_core_expr_t *result);

bool dy_eval_step_expr(dy_core_expr_t expr, dy_core_expr_t *result)
{
    switch (expr.tag) {
    case DY_CORE_EXPR_ELIM:
        return dy_eval_step_elim(expr.elim, result);
    case DY_CORE_EXPR_SEQUENCE:
        return dy_eval_step_compose(expr.sequence, result);
    case DY_CORE_EXPR_COMP:
        *result = *expr.comp.expr;
        return true;
    case DY_CORE_EXPR_TRAP:
    case DY_CORE_EXPR_REF:
        return false;
    case DY_CORE_EXPR_INTRO:
    case DY_CORE_EXPR_INFERENCE_CTX:
        assert(!"impossible");
    }
}

bool dy_eval_step_elim(dy_core_elim_t elim, dy_core_expr_t *result)
{
    if (elim.check_result != DY_CHECK_STATUS_OK) {
        return false;
    }

    if (elim.expr->tag != DY_CORE_EXPR_INTRO) {
        return false;
    }

    if (elim.expr->intro.is_complex) {
        assert(!elim.is_complex);

        switch (elim.expr->intro.complex.tag) {
        case DY_CORE_FUNCTION:
            if (!dy_substitute_expr(*elim.expr->intro.complex.function.sequent, elim.expr->intro.complex.function.var.id, *elim.simple.mapping.expr, result)) {
                *result = *elim.expr->intro.complex.function.sequent;
            }

            return true;
        case DY_CORE_TUPLE:
            *result = elim.expr->intro.complex.tuple.sequents[elim.simple.selection.index - 1];
            return true;
        case DY_CORE_RECURSION:
            if (!dy_substitute(running_id, equal_variables, *elim.expr->intro.complex.recursion.sequent, elim.expr->intro.complex.recursion.var.id, *elim.expr, result)) {
                *result = *elim.expr->intro.complex.recursion.sequent;
            }

            return true;
        }
    } else {
        if (elim.is_complex) {
            return dy_eval_step_complex_elim(running_id, equal_variables, elim.expr->intro.simple, elim.complex, elim.is_implicit, result);
        } else {
            switch (elim.expr->intro.simple.tag) {
            case DY_CORE_MAPPING:
                *result = *elim.expr->intro.simple.mapping.sequent;
                return true;
            case DY_CORE_SELECTION:
                *result = *elim.expr->intro.simple.selection.sequent;
                return true;
            case DY_CORE_UNFOLDING:
                *result = *elim.expr->intro.simple.unfolding.sequent;
                return true;
            }
        }
    }
}

bool dy_eval_step_complex_elim(dy_core_simple_t simple, dy_core_complex_eliminator_t eliminator, bool is_implicit, dy_core_expr_t *result)
{
    dy_core_function_t fun;

    switch (eliminator.tag) {
    case DY_CORE_FUNCTION: {
        dy_core_expr_t e;
        if (!dy_substitute(running_id, equal_variables, *eliminator.function.cont.sequent, eliminator.function.var.id, *simple.mapping.expr, &e)) {
            e = *eliminator.function.cont.sequent;
        }

        fun = eliminator.function.cont;
        fun.sequent = dy_core_expr_new(e);

        break;
    }
    case DY_CORE_TUPLE:
        fun = eliminator.tuple.conts_with_check_status[simple.selection.index - 1].cont;

        break;
    case DY_CORE_RECURSION: {
        dy_core_expr_t self = {
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_NEGATIVE,
                .is_implicit = is_implicit,
                .is_complex = true,
                .complex = {
                    .tag = DY_CORE_RECURSION,
                    .recursion = {
                        .var = eliminator.recursion.var,
                        .sequent = dy_core_expr_new((dy_core_expr_t){
                            .tag = DY_CORE_EXPR_INTRO,
                            .intro = {
                                .polarity = DY_POLARITY_POSITIVE,
                                .is_implicit = false,
                                .is_complex = true,
                                .complex = {
                                    .tag = DY_CORE_FUNCTION,
                                    .function = {
                                        .var = eliminator.recursion.cont_with_inference_ctx.cont.var,
                                        .type = eliminator.recursion.cont_with_inference_ctx.cont.type,
                                        .sequent = eliminator.recursion.cont_with_inference_ctx.cont.sequent
                                    }
                                }
                            }
                        })
                    }
                }
            }
        };

        dy_core_expr_t e;
        if (!dy_substitute(running_id, equal_variables, *eliminator.recursion.cont_with_inference_ctx.cont.sequent, eliminator.recursion.var.id, self, &e)) {
            e = *eliminator.recursion.cont_with_inference_ctx.cont.sequent;
        }

        fun = eliminator.recursion.cont_with_inference_ctx.cont;
        fun.sequent = dy_core_expr_new(e);

        break;
    }
    }

    dy_core_elim_t elim = {
        .check_result = DY_CHECK_STATUS_OK,
        .expr = dy_core_expr_new((dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = false,
                .is_complex = true,
                .complex = {
                    .tag = DY_CORE_FUNCTION,
                    .function = fun
                }
            }
        }),
        .is_complex = false,
        .is_implicit = false,
        .simple = {
            .tag = DY_CORE_MAPPING,
            .mapping = {
                .expr = dy_core_simple_get_sequent_unretained(simple),
                .sequent = dy_core_eliminator_get_result_type_unretained(eliminator)
            }
        }
    };

    if (dy_eval_step_elim(running_id, equal_variables, elim, result)) {
    } else {
        *result = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_ELIM,
            .elim = elim
        };
    }

    return true;
}

bool dy_eval_step_compose(dy_core_sequence_t compose, dy_core_expr_t *result)
{
    dy_core_expr_t new_first;
    if (!dy_eval_step_expr(running_id, equal_variables, *compose.first, &new_first)) {
        return false;
    }

    dy_core_expr_t subst_second;
    if (!dy_substitute(running_id, equal_variables, *compose.second, compose.var.id, new_first, &subst_second)) {
        subst_second = *compose.second;
    }

    if (!dy_eval_step_expr(running_id, equal_variables, subst_second, result)) {
        return false;
    }

    return true;
}
