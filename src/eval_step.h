/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

static inline bool dy_eval_step_expr(dy_core_expr_t expr, dy_core_expr_t *result);

static inline bool dy_eval_step_elim(dy_core_elim_t elim, dy_core_expr_t *result);

static inline bool dy_eval_step_compose(dy_core_sequence_t compose, dy_core_expr_t *result);
