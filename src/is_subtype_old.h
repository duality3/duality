/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

/**
 * Implementation of the subtype check.
 *
 * Each subtype check has the following inputs:
 *    - The supposed subtype.
 *    - The supposed supertype.
 *    - The expression that is of type 'subtype'.
 *
 * And the following outputs:
 *    - Whether the subtype actually is a subtype of supertype; this is a ternary result.
 *    - Constraints that arose during subtype-checking inference variables.
 *    - A transformed 'subtype expr' as a result of subtype-checking implicit complex types.
 *    - Inference variables created by transforming implicit functions;
 *      They are supposed to be resolved by the caller of 'is_subtype'.
 */

struct dy_core_past_subtype_check {
    dy_core_expr_t subtype;
    dy_core_expr_t supertype;
    dy_core_variable_t substitute_var;
    bool have_substitute_var;
    dy_polarity_t polarity;
    dy_core_expr_t ret_expr;
};

typedef struct dy_core_sequence_step {
    dy_core_expr_t expr;
    dy_core_expr_t type;
} dy_core_sequence_step_t;

enum dy_subtype_result {
    DY_SUBTYPE_RESULT_YES_EQUAL,
    DY_SUBTYPE_RESULT_YES_INEQUAL,
    DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS,
    DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL,
    DY_SUBTYPE_RESULT_NO
};

enum dy_subtype_transform_result {
    DY_SUBTYPE_TRANSFORM_RESULT_YES,
    DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED,
    DY_SUBTYPE_TRANSFORM_RESULT_NO,
    DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS
};


static inline
enum dy_subtype_result dy_is_subtype(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_is_subtype_no_transformation(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, bool *musnt_transform_further);


static inline
enum dy_subtype_result dy_intros_are_subtypes(dy_core_ctx_t *ctx, dy_core_intro_t subtype, dy_core_intro_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_complex_are_subtypes(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_complex_t supertype, dy_polarity_t polariry, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_complex_is_subtype_of_simple(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_simple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_simple_prefix_is_subtype(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_simple_prefix_t prefix, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_function_is_subtype_of_mapping(dy_core_ctx_t *ctx, dy_core_function_t function, dy_core_expr_t mapping_expr, bool is_computationally_irrelevant, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype);

static inline
bool dy_tuple_is_subtype_of_selection(dy_core_ctx_t *ctx, dy_core_tuple_t tuple, size_t index, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype);

static inline
bool dy_recursion_is_subtype_of_unfolding(dy_core_ctx_t *ctx, dy_core_recursion_t recursion, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype);

static inline
enum dy_subtype_result dy_simple_is_subtype_of_complex(dy_core_ctx_t *ctx, dy_core_simple_t subtype, dy_core_complex_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_simple_are_subtypes(dy_core_ctx_t *ctx, dy_core_simple_t subtype, dy_core_simple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_implicit_is_subtype(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_is_subtype_of_implicit(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_complex_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_positive_functions_are_subtypes(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_function_t supertype, bool is_implicit, bool *musnt_transform_further);

static inline
enum dy_subtype_result dy_positive_pairs_are_subtypes(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_tuple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_positive_recursions_are_subtypes(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_recursion_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_negative_functions_are_subtypes(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_function_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_negative_pairs_are_subtypes(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_tuple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_negative_recursions_are_subtypes(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_recursion_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_mappings_are_subtypes(dy_core_ctx_t *ctx, dy_core_mapping_t subtype, dy_core_mapping_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_selections_are_subtypes(dy_core_ctx_t *ctx, dy_core_selection_t subtype, dy_core_selection_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_unfolds_are_subtypes(dy_core_ctx_t *ctx, dy_core_unfolding_t subtype, dy_core_unfolding_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_function_is_subtype_of_mapping(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_mapping_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_pair_is_subtype_of_selection(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_selection_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_recursion_is_subtype_of_unfold(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_unfolding_t unfolding, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_mapping_is_subtype_of_function(dy_core_ctx_t *ctx, dy_core_mapping_t subtype, dy_core_function_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_selection_is_subtype_of_pair(dy_core_ctx_t *ctx, dy_core_selection_t subtype, dy_core_tuple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_unfold_is_subtype_of_recursion(dy_core_ctx_t *ctx, dy_core_unfolding_t unfolding, dy_core_recursion_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_implicit_function_is_subtype(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_implicit_pair_is_subtype(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_implicit_recursion_is_subtype(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_is_subtype_of_implicit_function(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_function_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_is_subtype_of_implicit_pair(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_tuple_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_is_subtype_of_implicit_recursion(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_recursion_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_is_subtype_of_implicit_positive_pair(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_tuple_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_implicit_negative_pair_is_subtype(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
enum dy_subtype_result dy_comp_types_are_subtypes(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_comp_type_is_subtype(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);

static inline
enum dy_subtype_result dy_is_subtype_of_comp_type(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);


static inline
void dy_compose_build_expr(dy_core_expr_t *expr, dy_core_sequence_step_t *steps, const dy_core_sequence_step_t *until, dy_core_expr_t *type);

static inline
size_t dy_sequence_steps_length(dy_core_ctx_t *ctx);
