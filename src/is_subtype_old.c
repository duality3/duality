/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "is_subtype.h"

#include "are_equal.h"
#include "constraint.h"
#include "substitute.h"
#include "type_of.h"

#include <duality/array.h>

static inline
dy_core_expr_t dy_build_id_app(dy_core_ctx_t *ctx, dy_core_expr_t expr, dy_core_expr_t type);

static inline
enum dy_subtype_result dy_join_subtype_results(enum dy_subtype_result res1, enum dy_subtype_result res2);

static inline
enum dy_subtype_transform_result dy_join_subtype_transform_results(enum dy_subtype_transform_result res1, enum dy_subtype_transform_result res2, bool *illegal_transform);

static inline
void dy_remove_size_t_from_array_starting_at(size_t *array, size_t s, size_t start_index);

static inline
size_t dy_size_t_array_length(size_t *array);

static inline
size_t dy_expr_array_length(dy_core_expr_t *exprs);

enum dy_subtype_result dy_is_subtype_no_transformation(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, bool *no_other_transformation_allowed)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    dy_core_expr_t dummy_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = 0
        }
    };

    size_t potential_transformation_ids_start = dy_size_t_array_length(ctx->potential_transform_recursion_ids);

    enum dy_subtype_transform_result did_transform_dummy_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, subtype, supertype, dummy_id_expr, &dummy_id_expr, &did_transform_dummy_id_expr);

    dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

    if (res == DY_SUBTYPE_RESULT_NO || did_transform_dummy_id_expr == DY_SUBTYPE_TRANSFORM_RESULT_YES) {
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_dummy_id_expr == DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED) {
        for (size_t i = potential_transformation_ids_start; i < dy_size_t_array_length(ctx->potential_transform_recursion_ids); ++i) {
            dy_array_add(&ctx->no_transformation_recursion_ids, &ctx->potential_transform_recursion_ids[i]);
        }

        *no_other_transformation_allowed = true;
    }

    if (did_transform_dummy_id_expr == DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS) {
        *no_other_transformation_allowed = true;
    }

    return res;
}

enum dy_subtype_result dy_is_subtype(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    const dy_core_past_subtype_check_t *past_checks_end = dy_array_past_end(ctx->past_subtype_checks);
    for (const dy_core_past_subtype_check_t *check = ctx->past_subtype_checks; check != past_checks_end; ++check) {
        if (dy_are_equal(subtype, check->subtype) && dy_are_equal(supertype, check->supertype)) {
            if (check->have_substitute_var) {
                dy_array_add(&ctx->potential_transform_recursion_ids, &check->substitute_var.id);

                *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED;

                if (check->polarity == DY_POLARITY_POSITIVE) {
                    *new_subtype_expr = (dy_core_expr_t){
                        .tag = DY_CORE_EXPR_REF,
                        .variable = check->substitute_var
                    };
                } else {
                    size_t elim_id = ctx->running_id++;

                    dy_core_sequence_step_t first_step = {
                        .id = elim_id,
                        .type = {
                            .tag = DY_CORE_EXPR_COMP,
                            .comp = {
                                .expr = dy_core_expr_new(check->ret_expr)
                            }
                        },
                        .expr = {
                            .tag = DY_CORE_EXPR_ELIM,
                            .elim = {
                                .is_complex = true,
                                .expr = dy_core_expr_new(subtype_expr),
                                .complex = {
                                    .tag = DY_CORE_RECURSION,
                                    .recursion = {
                                        .is_only_id = true,
                                        .var = check->substitute_var,
                                        .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                                    },
                                    .result_type = dy_core_expr_new((dy_core_expr_t){
                                        .tag = DY_CORE_EXPR_COMP,
                                        .comp = {
                                            .expr = dy_core_expr_new(check->ret_expr)
                                        }
                                    })
                                },
                                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                            }
                        }
                    };

                    dy_array_add(&ctx->sequence_steps, &first_step);

                    size_t final_id = ctx->running_id++;

                    dy_core_sequence_step_t second_step = {
                        .id = final_id,
                        .type = check->ret_expr,
                        .expr = {
                            .tag = DY_CORE_EXPR_REF,
                            .variable = {
                                .id = elim_id
                            }
                        }
                    };

                    dy_array_add(&ctx->sequence_steps, &second_step);

                    *new_subtype_expr = (dy_core_expr_t){
                        .tag = DY_CORE_EXPR_REF,
                        .variable = {
                            .id = final_id
                        }
                    };
                }

                return DY_SUBTYPE_RESULT_YES_EQUAL;
            } else {
                return DY_SUBTYPE_RESULT_NO;
            }
        } else if (dy_are_equal(subtype, check->supertype) && dy_are_equal(supertype, check->subtype)) {
            if (check->have_substitute_var) {
                return DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL;
            } else {
                return DY_SUBTYPE_RESULT_NO;
            }
        }
    }

    if (subtype.tag == DY_CORE_EXPR_REF && supertype.tag == DY_CORE_EXPR_REF) {
        if (subtype.ref == supertype.ref) {
            return DY_SUBTYPE_RESULT_YES_EQUAL;
        }

        dy_binder_t subtype_binder = *((const dy_binder_t *)dy_array_past_end(ctx->binders_in_scope) - 1 - subtype.ref);

        dy_binder_t supertype_binder = *((const dy_binder_t *)dy_array_past_end(ctx->binders_in_scope) - 1 - supertype.ref);

        if (subtype_binder.tag == DY_BINDER_INFERENCE_CTX) {
            if (supertype_binder.tag == DY_BINDER_INFERENCE_CTX) {
                dy_core_expr_t *upper_bounds = dy_array_create(sizeof *upper_bounds, 2);
                dy_array_add(&upper_bounds, &supertype);

                dy_constraint_t c1 = {
                    .index = subtype.ref,
                    .lower_bounds = dy_array_create(sizeof *c1.lower_bounds, 2),
                    .upper_bounds = upper_bounds,
                };

                dy_array_add(&ctx->constraints, &c1);

                dy_core_expr_t *lower_bounds = dy_array_create(sizeof *lower_bounds, 2);
                dy_array_add(&lower_bounds, &subtype);

                dy_constraint_t c2 = {
                    .index = supertype.ref,
                    .lower_bounds = lower_bounds,
                    .upper_bounds = dy_array_create(sizeof *c2.upper_bounds, 2)
                };

                dy_array_add(&ctx->constraints, &c2);

                return DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS;
            }

            dy_core_expr_t *upper_bounds = dy_array_create(sizeof *upper_bounds, 2);
            dy_array_add(&upper_bounds, &supertype);

            dy_constraint_t c = {
                .index = subtype.ref,
                .lower_bounds = dy_array_create(sizeof *c.lower_bounds, 2),
                .upper_bounds = upper_bounds,
            };

            dy_array_add(&ctx->constraints, &c);

            return DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS;
        }

        if (supertype_binder.tag == DY_BINDER_INFERENCE_CTX) {
            dy_core_expr_t *lower_bounds = dy_array_create(sizeof *lower_bounds, 2);
            dy_array_add(&lower_bounds, &subtype);

            dy_constraint_t c = {
                .index = supertype.ref,
                .lower_bounds = lower_bounds,
                .upper_bounds = dy_array_create(sizeof *c.upper_bounds, 2)
            };

            dy_array_add(&ctx->constraints, &c);

            return DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS;
        }

        return DY_SUBTYPE_RESULT_NO;
    }

    if (subtype.tag == DY_CORE_EXPR_INTRO && supertype.tag == DY_CORE_EXPR_INTRO) {
        enum dy_subtype_result res = dy_intros_are_subtypes(ctx, subtype.intro, supertype.intro, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        if (res == DY_SUBTYPE_RESULT_NO) {
            goto try_implicit_coercions;
        } else {
            return res;
        }
    }

    if (subtype.tag == DY_CORE_EXPR_ELIM && supertype.tag == DY_CORE_EXPR_ELIM) {
        if (dy_elims_are_equal(subtype.elim, supertype.elim)) {
            return DY_SUBTYPE_RESULT_YES_EQUAL;
        } else {
            return DY_SUBTYPE_RESULT_NO;
        }
    }

    if (subtype.tag == DY_CORE_EXPR_COMP && supertype.tag == DY_CORE_EXPR_COMP) {
        return dy_comp_types_are_subtypes(ctx, *subtype.comp.expr, *supertype.comp.expr, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }

try_implicit_coercions:
    if (subtype.tag == DY_CORE_EXPR_INTRO && subtype.intro.polarity == DY_POLARITY_POSITIVE && subtype.intro.tag == DY_INTRO_COMPLEX && subtype.intro.is_implicit) {
        enum dy_subtype_result res = dy_implicit_is_subtype(ctx, subtype.intro.complex, supertype, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        if (res != DY_SUBTYPE_RESULT_NO) {
            return res;
        }
    }

    if (supertype.tag == DY_CORE_EXPR_INTRO && supertype.intro.polarity == DY_POLARITY_NEGATIVE && supertype.intro.tag == DY_INTRO_COMPLEX && supertype.intro.is_implicit) {
        enum dy_subtype_result res = dy_is_subtype_of_implicit(ctx, subtype, supertype.intro.complex, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        if (res != DY_SUBTYPE_RESULT_NO) {
            return res;
        }
    }

    if (subtype.tag == DY_CORE_EXPR_INTRO && subtype.intro.polarity == DY_POLARITY_NEGATIVE && subtype.intro.tag == DY_INTRO_COMPLEX && subtype.intro.is_implicit && subtype.intro.complex.tag == DY_CORE_TUPLE) {
        enum dy_subtype_result res = dy_implicit_negative_pair_is_subtype(ctx, subtype.intro.complex.tuple, supertype, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        if (res != DY_SUBTYPE_RESULT_NO) {
            return res;
        }
    }

    if (supertype.tag == DY_CORE_EXPR_INTRO && supertype.intro.polarity == DY_POLARITY_POSITIVE && supertype.intro.tag == DY_INTRO_COMPLEX && supertype.intro.is_implicit && supertype.intro.complex.tag == DY_CORE_TUPLE) {
        enum dy_subtype_result res = dy_is_subtype_of_implicit_positive_pair(ctx, subtype, supertype.intro.complex.tuple, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        if (res != DY_SUBTYPE_RESULT_NO) {
            return res;
        }
    }
/*
    if (subtype.tag == DY_CORE_EXPR_COMP_TYPE) {
        dy_ternary_t res = dy_comp_type_is_subtype(ctx, *subtype.comp_type, supertype, subtype_expr, preceding_steps, new_subtype_expr, did_transform_subtype_expr);
        if (res != DY_NO) {
            return res;
        }
    }

    if (supertype.tag == DY_CORE_EXPR_COMP_TYPE) {
        dy_ternary_t res = dy_is_subtype_of_comp_type(ctx, subtype, *supertype.comp_type, subtype_expr, preceding_steps, new_subtype_expr, did_transform_subtype_expr);
        if (res != DY_NO) {
            return res;
        }
    }
*/
    return DY_SUBTYPE_RESULT_NO;
}

enum dy_subtype_result dy_intros_are_subtypes(dy_core_ctx_t *ctx, dy_core_intro_t subtype, dy_core_intro_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if (subtype.tag == DY_INTRO_COMPLEX && supertype.tag == DY_INTRO_COMPLEX) {
        if (subtype.polarity != supertype.polarity) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (subtype.is_implicit != supertype.is_implicit) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (subtype.complex.tag != supertype.complex.tag) {
            return DY_SUBTYPE_RESULT_NO;
        }

        return dy_complex_are_subtypes(ctx, subtype.complex, supertype.complex, subtype.polarity, subtype.is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }

    if (subtype.tag == DY_INTRO_SIMPLE && supertype.tag == DY_INTRO_SIMPLE) {
        if (subtype.polarity == DY_POLARITY_NEGATIVE && supertype.polarity == DY_POLARITY_POSITIVE) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (subtype.is_implicit != supertype.is_implicit) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (subtype.simple.prefix.tag != supertype.simple.prefix.tag) {
            return DY_SUBTYPE_RESULT_NO;
        }

        enum dy_subtype_result res = dy_simple_are_subtypes(ctx, subtype.simple, supertype.simple, subtype.is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        if (res == DY_SUBTYPE_RESULT_YES_EQUAL && subtype.polarity != supertype.polarity) {
            return DY_SUBTYPE_RESULT_YES_INEQUAL;
        } else {
            return res;
        }
    }

    if (subtype.tag == DY_INTRO_COMPLEX && supertype.tag == DY_INTRO_SIMPLE) {
        if (subtype.polarity != DY_POLARITY_POSITIVE || supertype.polarity != DY_POLARITY_NEGATIVE) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (subtype.is_implicit != supertype.is_implicit) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if ((int)subtype.complex.tag != (int)supertype.simple.prefix.tag) {
            return DY_SUBTYPE_RESULT_NO;
        }

        return dy_complex_is_subtype_of_simple(ctx, subtype.complex, supertype.simple, subtype.is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }

    if (subtype.tag == DY_INTRO_SIMPLE && supertype.tag == DY_INTRO_COMPLEX) {
        if (subtype.polarity != DY_POLARITY_POSITIVE || supertype.polarity != DY_POLARITY_NEGATIVE) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (subtype.is_implicit != supertype.is_implicit) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if ((int)subtype.simple.prefix.tag != (int)supertype.complex.tag) {
            return DY_SUBTYPE_RESULT_NO;
        }

        return dy_simple_is_subtype_of_complex(ctx, subtype.simple, supertype.complex, subtype.is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }

    __builtin_unreachable();
}

enum dy_subtype_result dy_complex_are_subtypes(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_complex_t supertype, dy_polarity_t polarity, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if (polarity == DY_POLARITY_POSITIVE) {
        switch (subtype.tag) {
        case DY_CORE_FUNCTION: {
            bool musnt_transform_further = false;
            enum dy_subtype_result res = dy_positive_functions_are_subtypes(ctx, subtype.function, supertype.function, is_implicit, &musnt_transform_further);

            if (musnt_transform_further) {
                *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS;
            }

            return res;
        }
        case DY_CORE_TUPLE:
            return dy_positive_pairs_are_subtypes(ctx, subtype.tuple, supertype.tuple, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        case DY_CORE_RECURSION:
            return dy_positive_recursions_are_subtypes(ctx, subtype.recursion, supertype.recursion, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        }
    } else {
        switch (subtype.tag) {
        case DY_CORE_FUNCTION:
            return dy_negative_functions_are_subtypes(ctx, subtype.function, supertype.function, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        case DY_CORE_TUPLE:
            return dy_negative_pairs_are_subtypes(ctx, subtype.tuple, supertype.tuple, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        case DY_CORE_RECURSION:
            return dy_negative_recursions_are_subtypes(ctx, subtype.recursion, supertype.recursion, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
        }
    }
}

enum dy_subtype_result dy_complex_is_subtype_of_simple(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_simple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    dy_core_expr_t new_sequent_subtype, new_sequent_expr;
    enum dy_subtype_transform_result prefix_transform_result = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result prefix_res = dy_simple_prefix_is_subtype(ctx, subtype, supertype.prefix, is_implicit, subtype_expr, &new_sequent_subtype, &new_sequent_expr, &prefix_transform_result);


    dy_is_subtype(ctx, new_sequent_subtype, *supertype.sequent, new_sequent_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr);
}

enum dy_subtype_result dy_simple_prefix_is_subtype(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_simple_prefix_t prefix, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    switch (prefix.tag) {
    case DY_CORE_MAPPING:
        return dy_function_is_subtype_of_mapping(ctx, subtype.function, *prefix.expr, prefix.is_computationally_irrelevant, is_implicit, subtype_expr, new_subtype, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_SELECTION:
        return dy_tuple_is_subtype_of_selection(ctx, subtype.tuple, prefix.index, is_implicit, subtype_expr, new_subtype, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_UNFOLDING:
        return dy_recursion_is_subtype_of_unfolding(ctx, subtype.recursion, is_implicit, subtype_expr, new_subtype, new_subtype_expr, did_transform_subtype_expr);
    }
}

enum dy_subtype_result dy_function_is_subtype_of_mapping(dy_core_ctx_t *ctx, dy_core_function_t function, dy_core_expr_t mapping_expr, bool is_computationally_irrelevant, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype)
{
    if ((function.type == NULL) != is_computationally_irrelevant) {
        return DY_SUBTYPE_RESULT_NO;
    }

    if (function.type != NULL) {
        dy_core_expr_t type_of_mapping_expr;
        if (!dy_type_of_expr(ctx->binders_in_scope, mapping_expr, &type_of_mapping_expr)) {
            return DY_SUBTYPE_RESULT_NO;
        }

        enum dy_subtype_transform_result transform_res;
        enum dy_subtype_result res = dy_is_subtype(ctx, type_of_mapping_expr, *function.type, mapping_expr, &mapping_expr, &transform_res);

    }
}

bool dy_tuple_is_subtype_of_selection(dy_core_ctx_t *ctx, dy_core_tuple_t tuple, size_t index, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype)
{
    dy_core_expr_t *sequent = tuple.sequents + index;
    if (sequent >= (const dy_core_expr_t *)dy_array_past_end(tuple.sequents)) {
        return false;
    }

    *new_subtype = *sequent;

    dy_core_sequence_step_t step = {
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .tag = DY_INTRO_SIMPLE,
                .simple_prefix = {
                    .tag = DY_CORE_UNFOLDING
                },
                .result_type = new_subtype
            }
        },
        .type = *new_subtype
    };

    dy_array_add(&ctx->sequence_steps, &step);

    return true;
}

bool dy_recursion_is_subtype_of_unfolding(dy_core_ctx_t *ctx, dy_core_recursion_t recursion, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype)
{
    dy_core_expr_t recursion_expr = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = is_implicit,
            .tag = DY_INTRO_COMPLEX,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = recursion
            }
        }
    };

    bool did_substitute;
    if (!dy_substitute_expr_with_expr(*recursion.sequent, 0, recursion_expr, new_subtype, &did_substitute)) {
        return false;
    }
    if (!did_substitute) {
        *new_subtype = *recursion.sequent;
    }

    dy_core_sequence_step_t step = {
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .tag = DY_INTRO_SIMPLE,
                .simple_prefix = {
                    .tag = DY_CORE_UNFOLDING
                },
                .result_type = new_subtype
            }
        },
        .type = *new_subtype
    };

    dy_array_add(&ctx->sequence_steps, &step);

    return true;
}

enum dy_subtype_result dy_simple_is_subtype_of_complex(dy_core_ctx_t *ctx, dy_core_simple_t subtype, dy_core_complex_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    switch (subtype.tag) {
    case DY_CORE_FUNCTION:
        return dy_mapping_is_subtype_of_function(ctx, subtype.mapping, supertype.function, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_TUPLE:
        return dy_selection_is_subtype_of_pair(ctx, subtype.selection, supertype.tuple, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_RECURSION:
        return dy_unfold_is_subtype_of_recursion(ctx, subtype.unfolding, supertype.recursion, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }
}

enum dy_subtype_result dy_simple_are_subtypes(dy_core_ctx_t *ctx, dy_core_simple_t subtype, dy_core_simple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    switch (subtype.tag) {
    case DY_CORE_FUNCTION:
        return dy_mappings_are_subtypes(ctx, subtype.mapping, supertype.mapping, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_TUPLE:
        return dy_selections_are_subtypes(ctx, subtype.selection, supertype.selection, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_RECURSION:
        return dy_unfolds_are_subtypes(ctx, subtype.unfolding, supertype.unfolding, is_implicit, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }
}

enum dy_subtype_result dy_implicit_is_subtype(dy_core_ctx_t *ctx, dy_core_complex_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    switch (subtype.tag) {
    case DY_CORE_FUNCTION:
        return dy_implicit_function_is_subtype(ctx, subtype.function, supertype, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_TUPLE:
        return dy_implicit_pair_is_subtype(ctx, subtype.tuple, supertype, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_RECURSION:
        return dy_implicit_recursion_is_subtype(ctx, subtype.recursion, supertype, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }
}

enum dy_subtype_result dy_is_subtype_of_implicit(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_complex_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    switch (supertype.tag) {
    case DY_CORE_FUNCTION:
        return dy_is_subtype_of_implicit_function(ctx, subtype, supertype.function, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_TUPLE:
        return dy_is_subtype_of_implicit_pair(ctx, subtype, supertype.tuple, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    case DY_CORE_RECURSION:
        return dy_is_subtype_of_implicit_recursion(ctx, subtype, supertype.recursion, subtype_expr, new_subtype_expr, did_transform_subtype_expr);
    }
}

enum dy_subtype_result dy_positive_functions_are_subtypes(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_function_t supertype, bool is_implicit, bool *musnt_transform_further)
{
    if ((subtype.type == NULL) != (supertype.type == NULL)) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_result res1 = DY_SUBTYPE_RESULT_YES_EQUAL;
    if (subtype.type != NULL) {
        res1 = dy_is_subtype_no_transformation(ctx, *supertype.type, *subtype.type, musnt_transform_further);
        if (res1 == DY_SUBTYPE_RESULT_NO) {
            return DY_SUBTYPE_RESULT_NO;
        }
    }

    dy_equal_variables_t var = {
        .id1 = subtype.var.id,
        .id2 = supertype.var.id
    };

    dy_array_add(&ctx->equal_variables, &var);

    size_t constraint_start2 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_result res2 = dy_is_subtype_no_transformation(ctx, *subtype.sequent, *supertype.sequent, musnt_transform_further);

    dy_array_remove_last(ctx->equal_variables);

    dy_move_constraints_up(&ctx->running_id, ctx->variables_in_scope, &ctx->constraints[constraint_start2], dy_array_past_end(ctx->constraints), subtype.var.id);
    dy_move_constraints_up(&ctx->running_id, ctx->variables_in_scope, &ctx->constraints[constraint_start2], dy_array_past_end(ctx->constraints), supertype.var.id);

    enum dy_subtype_result combined_res = dy_join_subtype_results(res1, res2);

    if (combined_res == DY_SUBTYPE_RESULT_NO) {
        dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);
        return DY_SUBTYPE_RESULT_NO;
    }

    dy_join_constraints(ctx, &ctx->constraints[constraint_start1], &ctx->constraints[constraint_start2]);

    return combined_res;
}

enum dy_subtype_result dy_positive_pairs_are_subtypes(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_tuple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    const dy_core_expr_t *old_subtype_end = dy_array_past_end(subtype.sequents);
    const dy_core_expr_t *old_supertype_end = dy_array_past_end(supertype.sequents);

    size_t num_elems_subtype = (size_t)(old_subtype_end - subtype.sequents);
    size_t num_elems_supertype = (size_t)(old_supertype_end - supertype.sequents);

    if (num_elems_subtype < num_elems_supertype) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_result combined_res = DY_SUBTYPE_RESULT_YES_EQUAL;

    enum dy_subtype_transform_result transform_res = DY_SUBTYPE_TRANSFORM_RESULT_NO;

    dy_core_expr_t *transforms = dy_array_create(sizeof *transforms, num_elems_supertype);

    size_t index = 1;
    for (size_t i = 0; i < num_elems_supertype; ++i) {
        dy_core_expr_t *sub = &subtype.sequents[i];
        dy_core_expr_t *super = &supertype.sequents[i];

        size_t id = ctx->running_id++;

        dy_core_sequence_step_t step = {
            .id = id,
            .type = *sub,
            .expr = {
                .tag = DY_CORE_EXPR_ELIM,
                .elim = {
                    .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                    .expr = dy_core_expr_new(subtype_expr),
                    .simple = {
                        .tag = DY_CORE_SELECTION,
                        .selection = {
                            .index = index,
                            .sequent = sub
                        }
                    },
                    .is_implicit = is_implicit
                }
            }
        };

        dy_array_add(&ctx->sequence_steps, &step);

        dy_core_expr_t id_subtype_expr = {
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = id
            }
        };

        size_t constraint_start2 = dy_constraints_length(ctx->constraints);

        enum dy_subtype_transform_result did_transform_id_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
        enum dy_subtype_result res = dy_is_subtype(ctx, *sub, *super, id_subtype_expr, &id_subtype_expr, &did_transform_id_subtype_expr);

        combined_res = dy_join_subtype_results(combined_res, res);

        bool have_illegal_transform = false;
        transform_res = dy_join_subtype_transform_results(transform_res, did_transform_id_subtype_expr, &have_illegal_transform);

        if (combined_res == DY_SUBTYPE_RESULT_NO || have_illegal_transform) {
            dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

            dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);

            return DY_SUBTYPE_RESULT_NO;
        }

        dy_join_constraints(ctx, &ctx->constraints[constraint_start1], &ctx->constraints[constraint_start2]);

        transforms[index - 1] = id_subtype_expr;

        ++index;
    }

    if (combined_res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

        dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);

        return DY_SUBTYPE_RESULT_NO;
    }

    if (transform_res != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = transform_res;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = true,
                .complex = {
                    .tag = DY_CORE_TUPLE,
                    .tuple = {
                        .sequents = transforms
                    }
                }
            }
        };
    } else {
        // TODO: Free transforms
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    return combined_res;
}

enum dy_subtype_result dy_positive_recursions_are_subtypes(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_recursion_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    dy_core_expr_t subtype_wrapper = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = is_implicit,
            .is_complex = true,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = subtype
            }
        }
    };

    dy_core_expr_t subst_subtype;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *subtype.sequent, subtype.var.id, subtype_wrapper, &subst_subtype)) {
        subst_subtype = *subtype.sequent;
    }

    size_t id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = id,
        .type = subst_subtype,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_UNFOLDING,
                    .unfolding = {
                        .sequent = dy_core_expr_new(subst_subtype)
                    }
                },
                .is_implicit = is_implicit,
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t id_subtype_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = id
        }
    };

    dy_equal_variables_t equal_vars = {
        .id1 = subtype.var.id,
        .id2 = supertype.var.id
    };

    dy_array_add(&ctx->equal_variables, &equal_vars);

    struct dy_core_past_subtype_check memoized_check = {
        .subtype = {
            .tag = DY_CORE_EXPR_REF,
            .variable = subtype.var
        },
        .supertype = {
            .tag = DY_CORE_EXPR_REF,
            .variable = supertype.var
        },
        .substitute_var = supertype.var,
        .have_substitute_var = true,
        .polarity = DY_POLARITY_POSITIVE
    };

    dy_array_add(&ctx->past_subtype_checks, &memoized_check);

    size_t inverse_subtype_recursion_ids_start = dy_size_t_array_length(ctx->inverse_subtype_recursion_ids);
    size_t potential_transform_recursion_ids_start = dy_size_t_array_length(ctx->potential_transform_recursion_ids);
    size_t no_transformation_recursion_ids_start = dy_size_t_array_length(ctx->no_transformation_recursion_ids);

    enum dy_subtype_transform_result did_transform_id_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, *subtype.sequent, *supertype.sequent, id_subtype_expr, &id_subtype_expr, &did_transform_id_subtype_expr);

    dy_array_remove_last(ctx->past_subtype_checks);

    dy_array_remove_last(ctx->equal_variables);

    dy_remove_size_t_from_array_starting_at(ctx->inverse_subtype_recursion_ids, supertype.var.id, inverse_subtype_recursion_ids_start);

    dy_remove_size_t_from_array_starting_at(ctx->potential_transform_recursion_ids, supertype.var.id, potential_transform_recursion_ids_start);

    dy_remove_size_t_from_array_starting_at(ctx->no_transformation_recursion_ids, supertype.var.id, no_transformation_recursion_ids_start);

    if (res == DY_SUBTYPE_RESULT_NO) {
        return DY_SUBTYPE_RESULT_NO;
    }

    enum dy_subtype_transform_result new_transform_result;
    if (did_transform_id_subtype_expr == DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS && dy_size_t_array_length(ctx->no_transformation_recursion_ids) == no_transformation_recursion_ids_start) {
        new_transform_result = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    } else if (did_transform_id_subtype_expr == DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED && dy_size_t_array_length(ctx->potential_transform_recursion_ids) == potential_transform_recursion_ids_start) {
        new_transform_result = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    } else {
        new_transform_result = did_transform_id_subtype_expr;
    }

    if (new_transform_result != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = new_transform_result;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = true,
                .complex = {
                    .tag = DY_CORE_RECURSION,
                    .recursion = {
                        .var = supertype.var,
                        .sequent = dy_core_expr_new(id_subtype_expr)
                    }
                }
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (res == DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL && dy_size_t_array_length(ctx->inverse_subtype_recursion_ids) == inverse_subtype_recursion_ids_start) {
        return DY_SUBTYPE_RESULT_YES_EQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_negative_functions_are_subtypes(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_function_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if ((subtype.type == NULL) != (supertype.type == NULL)) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start1 = dy_sequence_steps_length(ctx);

    dy_core_expr_t type_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = subtype.var
    };

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_transform_result did_transform_type_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res1 = DY_SUBTYPE_RESULT_YES_EQUAL;
    if (subtype.type != NULL) {
        res1 = dy_is_subtype(ctx, *subtype.type, *supertype.type, type_id_expr, &type_id_expr, &did_transform_type_id_expr);

        if (res1 == DY_SUBTYPE_RESULT_NO) {
            return DY_SUBTYPE_RESULT_NO;
        }
    }

    dy_equal_variables_t equal_vars = {
        .id1 = subtype.var.id,
        .id2 = supertype.var.id
    };

    dy_array_add(&ctx->equal_variables, &equal_vars);

    size_t sequent_id = ctx->running_id++;

    dy_core_expr_t sequent_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = sequent_id
        }
    };

    size_t constraint_start2 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_transform_result did_transform_sequent_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res2 = dy_is_subtype(ctx, *subtype.sequent, *supertype.sequent, sequent_id_expr, &sequent_id_expr, &did_transform_sequent_id_expr);

    dy_array_remove_last(ctx->equal_variables);

    enum dy_subtype_result res = dy_join_subtype_results(res1, res2);

    bool have_illegal_transform = false;
    enum dy_subtype_transform_result transform_result = dy_join_subtype_transform_results(did_transform_type_id_expr, did_transform_sequent_id_expr, &have_illegal_transform);

    if (res == DY_SUBTYPE_RESULT_NO || have_illegal_transform) {
        dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start1]);
        return DY_SUBTYPE_RESULT_NO;
    }

    dy_join_constraints(ctx, &ctx->constraints[constraint_start1], &ctx->constraints[constraint_start2]);

    if (transform_result != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = transform_result;

        dy_core_expr_t result_type = {
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_NEGATIVE,
                .is_implicit = is_implicit,
                .is_complex = true,
                .complex = {
                    .tag = DY_CORE_FUNCTION,
                    .function = {
                        .var = supertype.var,
                        .type = supertype.type,
                        .sequent = supertype.sequent
                    }
                }
            }
        };

        dy_core_expr_t *result_type_ptr = dy_core_expr_new(result_type);

        size_t trivial_comp_arg_id = ctx->running_id++;

        dy_core_expr_t last_comp = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = false,
                .expr = dy_core_expr_new((dy_core_expr_t){
                    .tag = DY_CORE_EXPR_INTRO,
                    .intro = {
                        .polarity = DY_POLARITY_POSITIVE,
                        .is_implicit = false,
                        .is_complex = true,
                        .complex = {
                            .tag = DY_CORE_FUNCTION,
                            .function = {
                                .var = {
                                    .id = trivial_comp_arg_id
                                },
                                .type = result_type_ptr,
                                .sequent = dy_core_expr_new((dy_core_expr_t){
                                    .tag = DY_CORE_EXPR_REF,
                                    .variable = {
                                        .id = trivial_comp_arg_id
                                    }
                                })
                            }
                        }
                    }
                }),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new((dy_core_expr_t){
                            .tag = DY_CORE_EXPR_INTRO,
                            .intro = {
                                .is_implicit = is_implicit,
                                .polarity = DY_POLARITY_POSITIVE,
                                .is_complex = false,
                                .simple = {
                                    .tag = DY_CORE_MAPPING,
                                    .mapping = {
                                        .expr = dy_core_expr_new(type_id_expr),
                                        .sequent = dy_core_expr_new(sequent_id_expr)
                                    }
                                }
                            }
                        }),
                        .sequent = result_type_ptr,
                        .is_computationally_irrelevant = false
                    }
                }
            }
        };

        dy_compose_build_expr(&last_comp, ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start1], result_type_ptr);

        dy_core_expr_t comp_result_type = {
            .tag = DY_CORE_EXPR_COMP,
            .comp = {
                .expr = result_type_ptr
            }
        };

        dy_core_expr_t elim = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_complex = true,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .complex = {
                    .tag = DY_CORE_FUNCTION,
                    .function = {
                        .var = subtype.var,
                        .type = subtype.type,
                        .cont = {
                            .var = {
                                .id = sequent_id
                            },
                            .type = subtype.sequent,
                            .sequent = dy_core_expr_new(last_comp)
                        },
                        .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                    },
                    .result_type = dy_core_expr_new(comp_result_type),
                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        };

        size_t elim_result_id = ctx->running_id++;

        dy_core_sequence_step_t step = {
            .id = elim_result_id,
            .type = comp_result_type,
            .expr = elim
        };

        dy_array_add(&ctx->sequence_steps, &step);

        size_t final_result_id = ctx->running_id++;

        step = (dy_core_compose_step_t){
            .id = final_result_id,
            .type = result_type,
            .expr = {
                .tag = DY_CORE_EXPR_REF,
                .variable = {
                    .id = elim_result_id
                }
            }
        };

        dy_array_add(&ctx->sequence_steps, &step);

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = final_result_id
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start1]);
    }

    return res;
}

enum dy_subtype_result dy_negative_pairs_are_subtypes(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_tuple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    const dy_core_expr_t *subtype_end = dy_array_past_end(subtype.sequents);
    const dy_core_expr_t *supertype_end = dy_array_past_end(supertype.sequents);

    size_t num_elems_subtype = (size_t)(subtype_end - subtype.sequents);
    size_t num_elems_supertype = (size_t)(supertype_end - supertype.sequents);

    if (num_elems_subtype > num_elems_supertype) {
        return DY_SUBTYPE_RESULT_NO;
    }

    dy_core_expr_t supertype_expr = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .is_implicit = is_implicit,
            .polarity = DY_POLARITY_NEGATIVE,
            .is_complex = true,
            .complex = {
                .tag = DY_CORE_TUPLE,
                .tuple = supertype
            }
        }
    };

    dy_core_expr_t *supertype_expr_ptr = dy_core_expr_new(supertype_expr);

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_result combined_res = DY_SUBTYPE_RESULT_YES_EQUAL;
    enum dy_subtype_transform_result transform_res = DY_SUBTYPE_TRANSFORM_RESULT_NO;

    dy_core_cont_with_check_status_t *conts = dy_array_create(sizeof *conts, num_elems_subtype);

    size_t index = 1;
    for (size_t i = 0; i < num_elems_subtype; ++i) {
        dy_core_expr_t *sub = &subtype.sequents[i];
        const dy_core_expr_t *super = &supertype.sequents[i];

        size_t sequence_steps_current_start = dy_sequence_steps_length(ctx);

        size_t id = ctx->running_id++;

        dy_core_expr_t id_expr = {
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = id
            }
        };

        size_t constraint_start2 = dy_constraints_length(ctx->constraints);

        enum dy_subtype_transform_result have_transform = DY_SUBTYPE_TRANSFORM_RESULT_NO;
        enum dy_subtype_result res = dy_is_subtype(ctx, *sub, *super, id_expr, &id_expr, &have_transform);

        combined_res = dy_join_subtype_results(combined_res, res);

        bool have_illegal_transform = false;
        transform_res = dy_join_subtype_transform_results(transform_res, have_transform, &have_illegal_transform);

        if (combined_res == DY_SUBTYPE_RESULT_NO || have_illegal_transform) {
            dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);

            dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

            return DY_SUBTYPE_RESULT_NO;
        }

        dy_join_constraints(ctx, &ctx->constraints[constraint_start1], &ctx->constraints[constraint_start2]);

        size_t trivial_comp_id = ctx->running_id++;

        dy_core_expr_t last_comp = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = false,
                .expr = dy_core_expr_new((dy_core_expr_t){
                    .tag = DY_CORE_EXPR_INTRO,
                    .intro = {
                        .polarity = DY_POLARITY_POSITIVE,
                        .is_implicit = false,
                        .is_complex = true,
                        .complex = {
                            .tag = DY_CORE_FUNCTION,
                            .function = {
                                .var = {
                                    .id = trivial_comp_id
                                },
                                .type = supertype_expr_ptr,
                                .sequent = dy_core_expr_new((dy_core_expr_t){
                                    .tag = DY_CORE_EXPR_REF,
                                    .variable = {
                                        .id = trivial_comp_id
                                    }
                                })
                            }
                        }
                    }
                }),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new((dy_core_expr_t){
                            .tag = DY_CORE_EXPR_INTRO,
                            .intro = {
                                .polarity = DY_POLARITY_POSITIVE,
                                .is_implicit = false,
                                .is_complex = false,
                                .simple = {
                                    .tag = DY_CORE_SELECTION,
                                    .selection = {
                                        .index = index,
                                        .sequent = dy_core_expr_new(id_expr)
                                    }
                                }
                            }
                        }),
                        .sequent = supertype_expr_ptr,
                        .is_computationally_irrelevant = false
                    }
                }
            }
        };

        dy_compose_build_expr(
            &last_comp,
            ctx->sequence_steps,
            &ctx->sequence_steps[sequence_steps_current_start],
            supertype_expr_ptr
        );

        dy_core_cont_with_check_status_t cont_with_status = {
            .status = DY_CHECK_STATUS_NEEDS_CHECKING,
            .cont = {
                .var = {
                    .id = id
                },
                .type = sub,
                .sequent = dy_core_expr_new(last_comp)
            }
        };

        dy_array_add(&conts, &cont_with_status);

        ++index;
    }

    if (transform_res != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = transform_res;

        dy_core_expr_t comp_result_type = {
            .tag = DY_CORE_EXPR_COMP,
            .comp = {
                .expr = dy_core_expr_new(supertype_expr)
            }
        };

        dy_core_expr_t elim = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .expr = dy_core_expr_new(subtype_expr),
                .is_implicit = is_implicit,
                .is_complex = true,
                .complex = {
                    .tag = DY_CORE_TUPLE,
                    .tuple = {
                        .conts_with_check_status = conts
                    },
                    .result_type = dy_core_expr_new(comp_result_type),
                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        };

        size_t elim_result_id = ctx->running_id++;

        dy_core_sequence_step_t step = {
            .id = elim_result_id,
            .type = comp_result_type,
            .expr = elim
        };

        dy_array_add(&ctx->sequence_steps, &step);

        size_t final_result_id = ctx->running_id++;

        step = (dy_core_compose_step_t){
            .id = final_result_id,
            .type =supertype_expr,
            .expr = {
                .tag = DY_CORE_EXPR_REF,
                .variable = {
                    .id = elim_result_id
                }
            }
        };

        dy_array_add(&ctx->sequence_steps, &step);

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = final_result_id
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    return combined_res;
}

enum dy_subtype_result dy_negative_recursions_are_subtypes(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_recursion_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t sequent_id = ctx->running_id++;

    dy_core_expr_t sequent_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = sequent_id
        }
    };

    dy_core_expr_t result_type = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .is_implicit = is_implicit,
            .polarity = DY_POLARITY_NEGATIVE,
            .is_complex = true,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = {
                    .var = supertype.var,
                    .sequent = supertype.sequent
                }
            }
        }
    };

    dy_core_expr_t *result_type_ptr = dy_core_expr_new(result_type);

    size_t new_subtype_id = ctx->running_id++;

    dy_core_expr_t new_subtype_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = new_subtype_id
        }
    };

    dy_core_expr_t new_subtype_sequent;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *subtype.sequent, subtype.var.id, new_subtype_id_expr, &new_subtype_sequent)) {
        new_subtype_sequent = *subtype.sequent;
    }

    struct dy_core_past_subtype_check memoized_check = {
        .subtype = {
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = new_subtype_id
            }
        },
        .supertype = {
            .tag = DY_CORE_EXPR_REF,
            .variable = supertype.var
        },
        .substitute_var = {
            .id = new_subtype_id
        },
        .have_substitute_var = true,
        .polarity = DY_POLARITY_NEGATIVE,
        .ret_expr = result_type
    };

    dy_array_add(&ctx->past_subtype_checks, &memoized_check);

    size_t inverse_subtype_recursion_ids_start = dy_size_t_array_length(ctx->inverse_subtype_recursion_ids);
    size_t potential_transform_recursion_ids_start = dy_size_t_array_length(ctx->potential_transform_recursion_ids);
    size_t no_transformation_recursion_ids_start = dy_size_t_array_length(ctx->no_transformation_recursion_ids);

    enum dy_subtype_transform_result did_transform_sequent_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, new_subtype_sequent, *supertype.sequent, sequent_id_expr, &sequent_id_expr, &did_transform_sequent_id_expr);

    dy_array_remove_last(ctx->past_subtype_checks);

    dy_remove_size_t_from_array_starting_at(ctx->inverse_subtype_recursion_ids, new_subtype_id, inverse_subtype_recursion_ids_start);

    dy_remove_size_t_from_array_starting_at(ctx->potential_transform_recursion_ids, new_subtype_id, potential_transform_recursion_ids_start);

    dy_remove_size_t_from_array_starting_at(ctx->no_transformation_recursion_ids, new_subtype_id, no_transformation_recursion_ids_start);

    if (res == DY_SUBTYPE_RESULT_NO) {
        return DY_SUBTYPE_RESULT_NO;
    }

    enum dy_subtype_transform_result new_transform_result;
    if (did_transform_sequent_id_expr == DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS && dy_size_t_array_length(ctx->no_transformation_recursion_ids) == no_transformation_recursion_ids_start) {
        new_transform_result = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    } else if (did_transform_sequent_id_expr == DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED && dy_size_t_array_length(ctx->potential_transform_recursion_ids) == potential_transform_recursion_ids_start) {
        new_transform_result = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    } else {
        new_transform_result = did_transform_sequent_id_expr;
    }

    if (new_transform_result != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = new_transform_result;

        size_t trivial_comp_id = ctx->running_id++;

        dy_core_expr_t last_comp = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = false,
                .expr = dy_core_expr_new((dy_core_expr_t){
                    .tag = DY_CORE_EXPR_INTRO,
                    .intro = {
                        .polarity = DY_POLARITY_POSITIVE,
                        .is_implicit = false,
                        .is_complex = true,
                        .complex = {
                            .tag = DY_CORE_FUNCTION,
                            .function = {
                                .var = {
                                    .id = trivial_comp_id
                                },
                                .type = result_type_ptr,
                                .sequent = dy_core_expr_new((dy_core_expr_t){
                                    .tag = DY_CORE_EXPR_REF,
                                    .variable = {
                                        .id = trivial_comp_id
                                    }
                                })
                            }
                        }
                    }
                }),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new((dy_core_expr_t){
                            .tag = DY_CORE_EXPR_INTRO,
                            .intro = {
                                .is_implicit = is_implicit,
                                .polarity = DY_POLARITY_POSITIVE,
                                .is_complex = false,
                                .simple = {
                                    .tag = DY_CORE_UNFOLDING,
                                    .unfolding = {
                                        .sequent = dy_core_expr_new(sequent_id_expr)
                                    }
                                }
                            }
                        }),
                        .sequent = result_type_ptr,
                        .is_computationally_irrelevant = false
                    }
                }
            }
        };

        dy_compose_build_expr(
            &last_comp,
            ctx->sequence_steps,
            &ctx->sequence_steps[sequence_steps_start],
            result_type_ptr
        );

        dy_core_expr_t comp_result_type = {
            .tag = DY_CORE_EXPR_COMP,
            .comp = {
                .expr = dy_core_expr_new(result_type)
            }
        };

        dy_core_expr_t elim = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = true,
                .complex = {
                    .tag = DY_CORE_RECURSION,
                    .recursion = {
                        .is_only_id = false,
                        .var = subtype.var,
                        .cont_with_inference_ctx = {
                            .inference_ids = dy_array_create(sizeof (size_t), 1),
                            .cont = {
                                .var = {
                                    .id = sequent_id
                                },
                                .type = subtype.sequent,
                                .sequent = dy_core_expr_new((dy_core_expr_t){
                                    .tag = DY_CORE_EXPR_INTRO,
                                    .intro = {
                                        .is_implicit = is_implicit,
                                        .polarity = DY_POLARITY_POSITIVE,
                                        .is_complex = false,
                                        .simple = {
                                            .tag = DY_CORE_UNFOLDING,
                                            .unfolding = {
                                                .sequent = dy_core_expr_new(last_comp)
                                            }
                                        }
                                    }
                                })
                            }
                        },
                        .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                    },
                    .result_type = dy_core_expr_new(comp_result_type)
                }
            }
        };

        size_t comp_elim_id = ctx->running_id++;

        dy_core_sequence_step_t step = {
            .id = comp_elim_id,
            .type = comp_result_type,
            .expr = elim
        };

        dy_array_add(&ctx->sequence_steps, &step);

        size_t final_result_id = ctx->running_id++;

        step = (dy_core_compose_step_t){
            .id = final_result_id,
            .type = result_type,
            .expr = {
                .tag = DY_CORE_EXPR_REF,
                .variable = {
                    .id = comp_elim_id
                }
            }
        };

        dy_array_add(&ctx->sequence_steps, &step);

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = final_result_id
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (res == DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL && dy_size_t_array_length(ctx->inverse_subtype_recursion_ids) == inverse_subtype_recursion_ids_start) {
        return DY_SUBTYPE_RESULT_YES_EQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_mappings_are_subtypes(dy_core_ctx_t *ctx, dy_core_mapping_t subtype, dy_core_mapping_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if (subtype.is_computationally_irrelevant != supertype.is_computationally_irrelevant) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    bool res1 = dy_are_equal(ctx->equal_variables, *subtype.expr, *supertype.expr);
    if (!res1) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = *subtype.sequent,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .expr = dy_core_expr_new(subtype_expr),
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = subtype.expr,
                        .sequent = subtype.sequent,
                        .is_computationally_irrelevant = subtype.is_computationally_irrelevant
                    }
                },
                .check_result = res1
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res2 = dy_is_subtype(ctx, *subtype.sequent, *supertype.sequent, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res2 == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = supertype.expr,
                        .sequent = dy_core_expr_new(elim_id_expr),
                        .is_computationally_irrelevant = subtype.is_computationally_irrelevant
                    }
                }
            }
        };

        *did_transform_subtype_expr = did_transform_elim_id_expr;
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    return res2;
}

enum dy_subtype_result dy_selections_are_subtypes(dy_core_ctx_t *ctx, dy_core_selection_t subtype, dy_core_selection_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if (subtype.index != supertype.index) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = *subtype.sequent,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_SELECTION,
                    .selection = {
                        .index = subtype.index,
                        .sequent = subtype.sequent
                    }
                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, *subtype.sequent, *supertype.sequent, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_SELECTION,
                    .selection = {
                        .index = subtype.index,
                        .sequent = dy_core_expr_new(elim_id_expr)
                    }
                }
            }
        };

        *did_transform_subtype_expr = did_transform_elim_id_expr;
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    return res;
}

enum dy_subtype_result dy_unfolds_are_subtypes(dy_core_ctx_t *ctx, dy_core_unfolding_t subtype, dy_core_unfolding_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = *subtype.sequent,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .expr = dy_core_expr_new(subtype_expr),
                .is_implicit = is_implicit,
                .simple = {
                    .tag = DY_CORE_UNFOLDING,
                    .unfolding = {
                        .sequent = subtype.sequent
                    }

                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, *subtype.sequent, *supertype.sequent, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_UNFOLDING,
                    .unfolding = {
                        .sequent = dy_core_expr_new(elim_id_expr)
                    }
                }
            }
        };

        *did_transform_subtype_expr = did_transform_elim_id_expr;
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    return res;
}

enum dy_subtype_result dy_function_is_subtype_of_mapping(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_mapping_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if ((subtype.type == NULL) != supertype.is_computationally_irrelevant) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    dy_core_expr_t transformed_supertype_expr;
    enum dy_subtype_transform_result did_transform_supertype_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res1 = DY_SUBTYPE_RESULT_YES_EQUAL;

    if (subtype.type != NULL) {
        dy_core_expr_t type_of_mapping;
        if (!dy_type_of(&ctx->running_id, ctx->variables_in_scope, *supertype.expr, &type_of_mapping)) {
            return DY_SUBTYPE_RESULT_NO;
        }

        res1 = dy_is_subtype(ctx, type_of_mapping, *subtype.type, *supertype.expr, &transformed_supertype_expr, &did_transform_supertype_expr);

        if (res1 == DY_SUBTYPE_RESULT_NO) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (did_transform_supertype_expr == DY_SUBTYPE_TRANSFORM_RESULT_NO) {
            transformed_supertype_expr = *supertype.expr;
        }
    } else {
        transformed_supertype_expr = *supertype.expr;
    }

    dy_core_expr_t subst_subtype;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *subtype.sequent, subtype.var.id, transformed_supertype_expr, &subst_subtype)) {
        subst_subtype = *subtype.sequent;
    }

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = subst_subtype,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new(transformed_supertype_expr),
                        .sequent = dy_core_expr_new(subst_subtype),
                        .is_computationally_irrelevant = supertype.is_computationally_irrelevant
                    }
                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    size_t constraint_start2 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res2 = dy_is_subtype(ctx, subst_subtype, *supertype.sequent, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    enum dy_subtype_result combined_res = dy_join_subtype_results(res1, res2);

    bool have_illegal_transform = false;
    enum dy_subtype_transform_result transform_result = dy_join_subtype_transform_results(did_transform_supertype_expr, did_transform_elim_id_expr, &have_illegal_transform);

    if (combined_res == DY_SUBTYPE_RESULT_NO || have_illegal_transform) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

        dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);

        return DY_SUBTYPE_RESULT_NO;
    }

    dy_join_constraints(ctx, &ctx->constraints[constraint_start1], &ctx->constraints[constraint_start2]);

    if (transform_result != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = transform_result;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = supertype.expr,
                        .sequent = dy_core_expr_new(elim_id_expr),
                        .is_computationally_irrelevant = supertype.is_computationally_irrelevant
                    }
                }
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (combined_res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return combined_res;
    }
}

enum dy_subtype_result dy_pair_is_subtype_of_selection(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_selection_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if (supertype.index == 0 || dy_expr_array_length(subtype.sequents) < supertype.index) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    dy_core_expr_t subtype_sequent = subtype.sequents[supertype.index - 1];

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = subtype_sequent,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_SELECTION,
                    .selection = {
                        .index = supertype.index,
                        .sequent = dy_core_expr_new(subtype_sequent)
                    }
                }
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, subtype_sequent, *supertype.sequent, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = did_transform_elim_id_expr;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_SELECTION,
                    .selection = {
                        .index = supertype.index,
                        .sequent = dy_core_expr_new(elim_id_expr)
                    }
                }
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_recursion_is_subtype_of_unfold(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_unfolding_t unfolding, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    dy_core_expr_t subtype_wrap = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = is_implicit,
            .is_complex = true,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = subtype
            }
        }
    };

    dy_core_expr_t subst_subtype_expr;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *subtype.sequent, subtype.var.id, subtype_wrap, &subst_subtype_expr)) {
        subst_subtype_expr = *subtype.sequent;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = subst_subtype_expr,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_UNFOLDING,
                    .unfolding = {
                        .sequent = dy_core_expr_new(subst_subtype_expr)
                    }
                }
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, subst_subtype_expr, *unfolding.sequent, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = did_transform_elim_id_expr;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_UNFOLDING,
                    .unfolding = {
                        .sequent = dy_core_expr_new(elim_id_expr)
                    }
                }
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_mapping_is_subtype_of_function(dy_core_ctx_t *ctx, dy_core_mapping_t subtype, dy_core_function_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if (subtype.is_computationally_irrelevant != (supertype.type == NULL)) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    dy_core_expr_t transformed_mapping;
    enum dy_subtype_transform_result did_transform_mapping = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res1 = DY_SUBTYPE_RESULT_YES_EQUAL;

    if (supertype.type != NULL) {
        dy_core_expr_t type_of_mapping;
        if (!dy_type_of(&ctx->running_id, ctx->variables_in_scope, *subtype.expr, &type_of_mapping)) {
            return DY_SUBTYPE_RESULT_NO;
        }

        res1 = dy_is_subtype(ctx, type_of_mapping, *supertype.type, *subtype.expr, &transformed_mapping, &did_transform_mapping);
        if (res1 == DY_SUBTYPE_RESULT_NO) {
            return DY_SUBTYPE_RESULT_NO;
        }

        if (did_transform_mapping == DY_SUBTYPE_TRANSFORM_RESULT_NO) {
            transformed_mapping = *subtype.expr;
        }
    } else {
        transformed_mapping = *subtype.expr;
    }

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = *subtype.sequent,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = subtype.expr,
                        .sequent = subtype.sequent,
                        .is_computationally_irrelevant = subtype.is_computationally_irrelevant
                    }
                }
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    dy_core_expr_t subst_supertype_expr;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *supertype.sequent, supertype.var.id, transformed_mapping, &subst_supertype_expr)) {
        subst_supertype_expr = *supertype.sequent;
    }

    size_t constraint_start2 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res2 = dy_is_subtype(ctx, *subtype.sequent, subst_supertype_expr, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    enum dy_subtype_result combined_res = dy_join_subtype_results(res1, res2);

    bool have_illegal_transform = false;
    enum dy_subtype_transform_result transform_result = dy_join_subtype_transform_results(did_transform_mapping, did_transform_elim_id_expr, &have_illegal_transform);

    if (combined_res == DY_SUBTYPE_RESULT_NO || have_illegal_transform) {
        dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);

        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

        return DY_SUBTYPE_RESULT_NO;
    }

    dy_join_constraints(ctx, &ctx->constraints[constraint_start1], &ctx->constraints[constraint_start2]);

    if (transform_result != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = transform_result;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new(transformed_mapping),
                        .sequent = dy_core_expr_new(elim_id_expr),
                        .is_computationally_irrelevant = subtype.is_computationally_irrelevant
                    }
                }
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (combined_res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return combined_res;
    }
}

enum dy_subtype_result dy_selection_is_subtype_of_pair(dy_core_ctx_t *ctx, dy_core_selection_t subtype, dy_core_tuple_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    if (subtype.index == 0 || subtype.index > dy_expr_array_length(supertype.sequents)) {
        return DY_SUBTYPE_RESULT_NO;
    }

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = *subtype.sequent,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .simple = {
                    .tag = DY_CORE_SELECTION,
                    .selection = {
                        .index = subtype.index,
                        .sequent = subtype.sequent
                    }
                }
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, *subtype.sequent, supertype.sequents[subtype.index - 1], elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = did_transform_elim_id_expr;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .is_implicit = is_implicit,
                .polarity = DY_POLARITY_POSITIVE,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_SELECTION,
                    .selection = {
                        .index = subtype.index,
                        .sequent = dy_core_expr_new(elim_id_expr),
                    }
                }
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_unfold_is_subtype_of_recursion(dy_core_ctx_t *ctx, dy_core_unfolding_t subtype, dy_core_recursion_t supertype, bool is_implicit, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    dy_core_expr_t supertype_wrap = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_NEGATIVE,
            .is_implicit = is_implicit,
            .is_complex = true,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = supertype
            }
        }
    };

    dy_core_expr_t subst_supertype_expr;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *supertype.sequent, supertype.var.id, supertype_wrap, &subst_supertype_expr)) {
        subst_supertype_expr = *supertype.sequent;
    }

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = *subtype.sequent,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
                .is_implicit = is_implicit,
                .expr = dy_core_expr_new(subtype_expr),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_UNFOLDING,
                    .unfolding = {
                        .sequent = subtype.sequent
                    }
                }
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, *subtype.sequent, subst_supertype_expr, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = did_transform_elim_id_expr;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_INTRO,
            .intro = {
                .polarity = DY_POLARITY_POSITIVE,
                .is_implicit = is_implicit,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_UNFOLDING,
                    .unfolding = {
                        .sequent = dy_core_expr_new(elim_id_expr)
                    }
                }
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_implicit_function_is_subtype(dy_core_ctx_t *ctx, dy_core_function_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    // TODO: Handle NULL type.

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t inference_id = ctx->running_id++;

    dy_core_expr_t inference_id_expr = {
        .tag = DY_CORE_EXPR_INFERENCE_VAR,
        .inference_var.id = inference_id
    };

    dy_core_expr_t type;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *subtype.sequent, subtype.var.id, inference_id_expr, &type)) {
        type = *subtype.sequent;
    }

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = type,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = true,
                .expr = dy_core_expr_new(subtype_expr),
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new(inference_id_expr),
                        .sequent = dy_core_expr_new(type),
                        .is_computationally_irrelevant = subtype.type == NULL
                    }
                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    size_t constraint_start = dy_constraints_length(ctx->constraints);

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, type, supertype, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    bool got_captured = false;
    dy_core_expr_t sub;
    dy_resolve_inference_var_simple(ctx, &ctx->constraints[constraint_start], inference_id, &sub, &got_captured);
    if (!got_captured) {
        dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (got_captured) {
        *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;

        *new_subtype_expr = elim_id_expr;

        if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
            return DY_SUBTYPE_RESULT_YES_INEQUAL;
        } else {
            return res;
        }
    }

    dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);
    dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *subtype.sequent, subtype.var.id, sub, &type)) {
        type = *subtype.sequent;
    }

    step = (dy_core_compose_step_t){
        .id = elim_id,
        .type = type,
        .expr = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = true,
                .expr = dy_core_expr_new(subtype_expr),
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new(sub),
                        .sequent = dy_core_expr_new(type),
                        .is_computationally_irrelevant = subtype.type == NULL
                    }
                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    res = dy_is_subtype(ctx, type, supertype, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;

    *new_subtype_expr = elim_id_expr;

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_implicit_pair_is_subtype(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_transform_result trans_result = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    dy_core_expr_t trans_expr;
    enum dy_subtype_result res = DY_SUBTYPE_RESULT_NO;

    const dy_core_expr_t *subtype_end = dy_array_past_end(subtype.sequents);

    size_t index = 1;
    for (const dy_core_expr_t *e = subtype.sequents; e != subtype_end; ++e) {
        size_t elim_id = ctx->running_id++;

        dy_core_sequence_step_t step = {
            .id = elim_id,
            .type = *e,
            .expr = {
                .tag = DY_CORE_EXPR_ELIM,
                .elim = {
                    .is_implicit = true,
                    .expr = dy_core_expr_new(subtype_expr),
                    .simple = {
                        .tag = DY_CORE_SELECTION,
                        .selection = {
                            .index = index,
                            .sequent = dy_core_expr_new(*e)
                        }
                    },
                    .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
                }
            }
        };

        dy_array_add(&ctx->sequence_steps, &step);

        dy_core_expr_t elim_id_expr = {
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = elim_id
            }
        };

        enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
        enum dy_subtype_result local_res = dy_is_subtype(ctx, *e, supertype, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

        if (local_res != DY_SUBTYPE_RESULT_NO) {
            if (res != DY_SUBTYPE_RESULT_NO) {
                dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);
                dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
                return DY_SUBTYPE_RESULT_NO;
            }

            trans_expr = elim_id_expr;
            trans_result = did_transform_elim_id_expr;
            res = local_res;
        }

        ++index;
    }

    if (res == DY_SUBTYPE_RESULT_NO) {
        return DY_SUBTYPE_RESULT_NO;
    }

    *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;
    *new_subtype_expr = trans_expr;

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_implicit_recursion_is_subtype(dy_core_ctx_t *ctx, dy_core_recursion_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    assert(!"not yet");
    /*
    dy_core_expr_t subtype_wrapper = {
        .tag = DY_CORE_EXPR_COMPLEX,
        .complex = {
            .is_implicit = true,
            .polarity = DY_POLARITY_POSITIVE,
            .tag = DY_CORE_RECURSION,
            .recursion = subtype
        }
    };

    dy_array_add(ctx.past_subtype_checks, &(struct dy_core_past_subtype_check){
        .subtype = subtype_wrapper,
        .supertype = supertype,
        .have_substitute_var_id = false
    });

    dy_core_expr_t elim_type;
    if (!dy_substitute(dy_is_subtype_ctx_to_substitute_ctx(ctx), *subtype.expr, subtype.id, subtype_wrapper, &elim_type)) {
        elim_type = dy_core_expr_retain(*subtype.expr);
    }

    dy_core_expr_t elim = {
        .tag = DY_CORE_EXPR_ELIM,
        .elim = {
            .check_result = DY_MAYBE,
            .expr = dy_core_expr_new(dy_core_expr_retain(subtype_expr)),
            .simple = {
                .is_implicit = true,
                .tag = DY_CORE_RECURSION,
                .out = dy_core_expr_new(elim_type),
                .polarity = DY_POLARITY_NEGATIVE
            }
        }
    };

    dy_core_expr_t transformed_elim;
    dy_ternary_t did_transform_elim = DY_NO;
    dy_ternary_t res = dy_is_subtype(ctx, elim_type, supertype, elim, &transformed_elim, &did_transform_elim);

    ctx.past_subtype_checks->len--;

    if (res == DY_NO) {
        dy_core_expr_release(elim);
        return DY_NO;
    }

    if (did_transform_elim) {
        dy_core_expr_release(elim);

        *did_transform_subtype_expr = did_transform_elim;
        *new_subtype_expr = transformed_elim;
    } else {
        *did_transform_subtype_expr = DY_YES;
        *new_subtype_expr = elim;
    }

    return res;
    */
}

enum dy_subtype_result dy_is_subtype_of_implicit_function(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_function_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    // TODO: Handle NULL type.
    assert(!"guarded");

    /*
    size_t inference_id = (*ctx.running_id)++;

    dy_core_expr_t inference_id_expr = {
        .tag = DY_CORE_EXPR_INFERENCE_VAR,
        .inference_var_id = inference_id
    };

    dy_core_expr_t type;
    if (!dy_substitute(dy_is_subtype_ctx_to_substitute_ctx(ctx), *supertype.expr, supertype.id, inference_id_expr, &type)) {
        type = dy_core_expr_retain(*supertype.expr);
    }

    dy_core_expr_t transformed_expr;
    dy_ternary_t did_transform_expr = DY_NO;
    dy_ternary_t res = dy_is_subtype(ctx, subtype, type, subtype_expr, &transformed_expr, &did_transform_expr);

    dy_core_expr_release(type);

    // TODO: Resolve

    if (!did_transform_expr) {
        transformed_expr = dy_core_expr_retain(subtype_expr);
    }

    if (res == DY_NO) {
        dy_core_expr_release(transformed_expr);
        return DY_NO;
    }

    *new_subtype_expr = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_SIMPLE,
        .simple = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = true,
            .tag = DY_CORE_FUNCTION,
            .mapping = dy_core_expr_new(inference_id_expr),
            .is_computationally_irrelevant = supertype.type == NULL,
            .out = dy_core_expr_new(transformed_expr)
        }
    };

    *did_transform_subtype_expr = DY_YES;

    return res;
    */
}

enum dy_subtype_result dy_is_subtype_of_implicit_pair(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_tuple_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t compose_start = dy_sequence_steps_length(ctx);

    size_t constraint_start = dy_constraints_length(ctx->constraints);

    dy_core_expr_t transformed_expr;
    enum dy_subtype_transform_result did_transform_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = DY_SUBTYPE_RESULT_NO;
    size_t local_index = 1;
    size_t index;

    const dy_core_expr_t *supertype_end = dy_array_past_end(supertype.sequents);
    for (const dy_core_expr_t *e = supertype.sequents; e != supertype_end; ++e) {
        ++local_index;

        dy_core_expr_t transformed_expr_local;
        enum dy_subtype_transform_result did_transform_expr_local = DY_SUBTYPE_TRANSFORM_RESULT_NO;
        enum dy_subtype_result res_local = dy_is_subtype(ctx, subtype, *e, subtype_expr, &transformed_expr_local, &did_transform_expr_local);

        if (res_local != DY_SUBTYPE_RESULT_NO) {
            if (res != DY_SUBTYPE_RESULT_NO) {
                // Can't have more than one candiate
                dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[compose_start]);
                dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);
                return DY_SUBTYPE_RESULT_NO;
            }

            index = local_index;

            res = res_local;
            transformed_expr = transformed_expr_local;
            did_transform_expr = did_transform_expr_local;
        }
    }

    if (res == DY_SUBTYPE_RESULT_NO) {
        return DY_SUBTYPE_RESULT_NO;
    }

    *new_subtype_expr = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = true,
            .is_complex = false,
            .simple = {
                .tag = DY_CORE_SELECTION,
                .selection = {
                    .index = index,
                    .sequent = dy_core_expr_new(transformed_expr)
                }
            }
        }
    };

    *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_is_subtype_of_implicit_recursion(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_recursion_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    dy_core_expr_t supertype_wrapper = {
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_NEGATIVE,
            .is_implicit = true,
            .is_complex = true,
            .complex = {
                .tag = DY_CORE_RECURSION,
                .recursion = supertype
            }
        }
    };

    struct dy_core_past_subtype_check memoized_check = {
        .subtype = subtype,
        .supertype = supertype_wrapper,
        .have_substitute_var = false
    };

    dy_array_add(&ctx->past_subtype_checks, &memoized_check);

    dy_core_expr_t out_type;
    if (!dy_substitute(&ctx->running_id, ctx->equal_variables, *supertype.sequent, supertype.var.id, supertype_wrapper, &out_type)) {
        out_type = *supertype.sequent;
    }

    dy_core_expr_t transformed_expr;
    enum dy_subtype_transform_result did_transform_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, subtype, out_type, subtype_expr, &transformed_expr, &did_transform_expr);

    dy_array_remove_last(ctx->past_subtype_checks);

    if (res == DY_SUBTYPE_RESULT_NO) {
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_expr == DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        transformed_expr = subtype_expr;
    }

    *new_subtype_expr = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_INTRO,
        .intro = {
            .polarity = DY_POLARITY_POSITIVE,
            .is_implicit = true,
            .is_complex = false,
            .simple = {
                .tag = DY_CORE_UNFOLDING,
                .unfolding = {
                    .sequent = dy_core_expr_new(transformed_expr)
                }
            }
        }
    };

    *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_is_subtype_of_implicit_positive_pair(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_tuple_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    assert(!"not yet");
    /*
    size_t constraint_start1 = ctx.constraints->len;

    dy_core_expr_t e1;
    dy_ternary_t did_transform_e1 = DY_NO;
    dy_ternary_t res1 = dy_is_subtype(ctx, subtype, *supertype.left, subtype_expr, &e1, &did_transform_e1);

    if (res1 == DY_NO) {
        return DY_NO;
    }

    size_t constraint_start2 = ctx.constraints->len;

    dy_core_expr_t e2;
    dy_ternary_t did_transform_e2 = DY_NO;
    dy_ternary_t res2 = dy_is_subtype(ctx, subtype, *supertype.right, subtype_expr, &e2, &did_transform_e2);

    if (res2 == DY_NO) {
        dy_free_constraints_from(ctx, constraint_start1, ctx.constraints->len);
        return DY_NO;
    }

    dy_join_constraints(ctx, constraint_start1, constraint_start2);

    if (did_transform_e1 == DY_NO) {
        e1 = dy_core_expr_retain(subtype_expr);
    }
    if (did_transform_e2 == DY_NO) {
        e2 = dy_core_expr_retain(subtype_expr);
    }

    *new_subtype_expr = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_COMPLEX,
        .complex = {
            .is_implicit = true,
            .polarity = DY_POLARITY_POSITIVE,
            .tag = DY_CORE_PAIR,
            .pair = {
                .left = dy_core_expr_new(e1),
                .right = dy_core_expr_new(e2)
            }
        }
    };

    *did_transform_subtype_expr = DY_YES;

    if (res1 == DY_MAYBE || res2 == DY_MAYBE) {
        return DY_MAYBE;
    }

    return DY_YES;
    */
}

enum dy_subtype_result dy_implicit_negative_pair_is_subtype(dy_core_ctx_t *ctx, dy_core_tuple_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    const dy_core_expr_t *subtype_end = dy_array_past_end(subtype.sequents);

    size_t num_elems_subtype = (size_t)(subtype_end - subtype.sequents);

    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t constraint_start1 = dy_constraints_length(ctx->constraints);

    enum dy_subtype_result combined_res = DY_SUBTYPE_RESULT_YES_EQUAL;
    enum dy_subtype_transform_result transform_res = DY_SUBTYPE_TRANSFORM_RESULT_NO;

    dy_core_cont_with_check_status_t *conts = dy_array_create(sizeof *conts, num_elems_subtype);

    size_t index = 1;
    for (dy_core_expr_t *sub = subtype.sequents; sub != subtype_end; ++sub) {
        size_t sequence_steps_current_start = dy_sequence_steps_length(ctx);

        size_t id = ctx->running_id++;

        dy_core_expr_t id_expr = {
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = id
            }
        };

        size_t constraint_start2 = dy_constraints_length(ctx->constraints);

        enum dy_subtype_transform_result have_transform = DY_SUBTYPE_TRANSFORM_RESULT_NO;
        enum dy_subtype_result res = dy_is_subtype(ctx, *sub, supertype, id_expr, &id_expr, &have_transform);

        combined_res = dy_join_subtype_results(combined_res, res);

        bool have_illegal_transform = false;
        transform_res = dy_join_subtype_transform_results(transform_res, have_transform, &have_illegal_transform);

        if (combined_res == DY_SUBTYPE_RESULT_NO || have_illegal_transform || transform_res == DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS) {
            dy_free_constraints_from(ctx->constraints, &ctx->constraints[constraint_start1], dy_array_past_end(ctx->constraints), ctx->free_expr_arrays);

            dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);

            return DY_SUBTYPE_RESULT_NO;
        }

        dy_join_constraints(ctx, &ctx->constraints[constraint_start1], &ctx->constraints[constraint_start2]);

        dy_core_expr_t *supertype_ptr = dy_core_expr_new(supertype);

        size_t trivial_comp_id = ctx->running_id++;

        dy_core_expr_t last_comp = {
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .is_implicit = false,
                .expr = dy_core_expr_new((dy_core_expr_t){
                    .tag = DY_CORE_EXPR_INTRO,
                    .intro = {
                        .polarity = DY_POLARITY_POSITIVE,
                        .is_implicit = false,
                        .is_complex = true,
                        .complex = {
                            .tag = DY_CORE_FUNCTION,
                            .function = {
                                .var = {
                                    .id = trivial_comp_id
                                },
                                .type = supertype_ptr,
                                .sequent = dy_core_expr_new((dy_core_expr_t){
                                    .tag = DY_CORE_EXPR_REF,
                                    .variable = {
                                        .id = trivial_comp_id
                                    }
                                })
                            }
                        }
                    }
                }),
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new((dy_core_expr_t){
                            .tag = DY_CORE_EXPR_INTRO,
                            .intro = {
                                .polarity = DY_POLARITY_POSITIVE,
                                .is_implicit = false,
                                .is_complex = false,
                                .simple = {
                                    .tag = DY_CORE_SELECTION,
                                    .selection = {
                                        .index = index,
                                        .sequent = dy_core_expr_new(id_expr)
                                    }
                                }
                            }
                        }),
                        .sequent = supertype_ptr,
                        .is_computationally_irrelevant = false
                    }
                }
            }
        };

        dy_compose_build_expr(
            &last_comp,
            ctx->sequence_steps,
            &ctx->sequence_steps[sequence_steps_current_start],
            supertype_ptr
        );

        dy_core_cont_with_check_status_t cont_with_status = {
            .status = DY_CHECK_STATUS_NEEDS_CHECKING,
            .cont = (dy_core_function_t){
                .var = {
                    .id = id
                },
                .type = dy_core_expr_new(*sub),
                .sequent = dy_core_expr_new(last_comp)
            }
        };

        dy_array_add(&conts, &cont_with_status);

        index++;
    }

    *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;

    dy_core_expr_t comp_result_type = {
        .tag = DY_CORE_EXPR_COMP,
        .comp = {
            .expr = dy_core_expr_new(supertype)
        }
    };

    dy_core_expr_t elim = {
        .tag = DY_CORE_EXPR_ELIM,
        .elim = {
            .expr = dy_core_expr_new(subtype_expr),
            .is_implicit = true,
            .is_complex = true,
            .complex = {
                .tag = DY_CORE_TUPLE,
                .tuple = {
                    .conts_with_check_status = conts
                },
                .result_type = dy_core_expr_new(comp_result_type)
            },
            .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
        }
    };

    size_t elim_result_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_result_id,
        .type = comp_result_type,
        .expr = elim
    };

    dy_array_add(&ctx->sequence_steps, &step);

    size_t final_result_id = ctx->running_id++;

    step = (dy_core_compose_step_t){
        .id = final_result_id,
        .type = supertype,
        .expr = {
            .tag = DY_CORE_EXPR_REF,
            .variable = {
                .id = elim_result_id
            }
        }
    };

    dy_array_add(&ctx->sequence_steps, &step);

    *new_subtype_expr = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = final_result_id
        }
    };

    if (combined_res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return combined_res;
    }
}

enum dy_subtype_result dy_comp_types_are_subtypes(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = subtype,
        .expr = subtype_expr
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, subtype, supertype, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    if (did_transform_elim_id_expr != DY_SUBTYPE_TRANSFORM_RESULT_NO) {
        *did_transform_subtype_expr = did_transform_elim_id_expr;

        size_t comp_id = ctx->running_id++;

        *new_subtype_expr = (dy_core_expr_t){
            .tag = DY_CORE_EXPR_ELIM,
            .elim = {
                .expr = dy_core_expr_new((dy_core_expr_t){
                    .tag = DY_CORE_EXPR_INTRO,
                    .intro = {
                        .polarity = DY_POLARITY_POSITIVE,
                        .is_implicit = false,
                        .is_complex = true,
                        .complex = {
                            .tag = DY_CORE_FUNCTION,
                            .function = {
                                .var = {
                                    .id = comp_id
                                },
                                .type = dy_core_expr_new(supertype),
                                .sequent = dy_core_expr_new((dy_core_expr_t){
                                    .tag = DY_CORE_EXPR_REF,
                                    .variable = {
                                        .id = comp_id
                                    }
                                })
                            }
                        }
                    }
                }),
                .is_implicit = false,
                .is_complex = false,
                .simple = {
                    .tag = DY_CORE_MAPPING,
                    .mapping = {
                        .expr = dy_core_expr_new(elim_id_expr),
                        .sequent = dy_core_expr_new(supertype),
                        .is_computationally_irrelevant = false
                    }
                },
                .check_result = DY_CHECK_STATUS_NEEDS_CHECKING
            }
        };
    } else {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
    }

    return res;
}

enum dy_subtype_result dy_comp_type_is_subtype(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    size_t sequence_steps_start = dy_sequence_steps_length(ctx);

    size_t elim_id = ctx->running_id++;

    dy_core_sequence_step_t step = {
        .id = elim_id,
        .type = subtype,
        .expr = subtype_expr
    };

    dy_array_add(&ctx->sequence_steps, &step);

    dy_core_expr_t elim_id_expr = {
        .tag = DY_CORE_EXPR_REF,
        .variable = {
            .id = elim_id
        }
    };

    enum dy_subtype_transform_result did_transform_elim_id_expr = DY_SUBTYPE_TRANSFORM_RESULT_NO;
    enum dy_subtype_result res = dy_is_subtype(ctx, subtype, supertype, elim_id_expr, &elim_id_expr, &did_transform_elim_id_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        dy_array_set_end(ctx->sequence_steps, &ctx->sequence_steps[sequence_steps_start]);
        return DY_SUBTYPE_RESULT_NO;
    }

    *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;
    *new_subtype_expr = elim_id_expr;

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

enum dy_subtype_result dy_is_subtype_of_comp_type(dy_core_ctx_t *ctx, dy_core_expr_t subtype, dy_core_expr_t supertype, dy_core_expr_t subtype_expr, dy_core_expr_t *new_subtype_expr, enum dy_subtype_transform_result *did_transform_subtype_expr)
{
    dy_core_expr_t e;
    enum dy_subtype_result res = dy_is_subtype(ctx, subtype, supertype, subtype_expr, &e, did_transform_subtype_expr);

    if (res == DY_SUBTYPE_RESULT_NO) {
        return DY_SUBTYPE_RESULT_NO;
    }

    *new_subtype_expr = dy_build_id_app(ctx, subtype_expr, supertype);

    *did_transform_subtype_expr = DY_SUBTYPE_TRANSFORM_RESULT_YES;

    if (res == DY_SUBTYPE_RESULT_YES_EQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    } else {
        return res;
    }
}

void dy_compose_build_expr(dy_core_expr_t *expr, dy_core_sequence_step_t *steps, const dy_core_sequence_step_t *until, dy_core_expr_t *type)
{
    if (until == dy_array_past_end(steps)) {
        return;
    }

    dy_core_sequence_step_t step;
    dy_array_remove(steps, &step);

    *expr = (dy_core_expr_t){
        .tag = DY_CORE_EXPR_SEQUENCE,
        .compose = {
            .var = {
                .id = step.id
            },
            .first = dy_core_expr_new(step.expr),
            .first_type = dy_core_expr_new(step.type),
            .second = dy_core_expr_new(*expr),
            .second_type = type,
            .first_result = DY_CHECK_STATUS_NEEDS_CHECKING,
            .second_result = DY_CHECK_STATUS_NEEDS_CHECKING
        }
    };

    dy_compose_build_expr(expr, steps, until, type);
}

dy_core_expr_t dy_build_id_app(dy_core_ctx_t *ctx, dy_core_expr_t expr, dy_core_expr_t type)
{
    size_t id = ctx->running_id++;

    return (dy_core_expr_t){
        .tag = DY_CORE_EXPR_ELIM,
        .elim = {
            .check_result = DY_CHECK_STATUS_NEEDS_CHECKING,
            .is_implicit = false,
            .expr = dy_core_expr_new((dy_core_expr_t){
                .tag = DY_CORE_EXPR_INTRO,
                .intro = {
                    .is_implicit = false,
                    .polarity = DY_POLARITY_POSITIVE,
                    .is_complex = true,
                    .complex = {
                        .tag = DY_CORE_FUNCTION,
                        .function = {
                            .var = {
                                .id = id
                            },
                            .type = dy_core_expr_new(type),
                            .sequent = dy_core_expr_new((dy_core_expr_t){
                                .tag = DY_CORE_EXPR_REF,
                                .variable = {
                                    .id = id
                                }
                            })
                        }
                    }
                }
            }),
            .is_complex = false,
            .simple = {
                .tag = DY_CORE_MAPPING,
                .mapping = {
                    .is_computationally_irrelevant = false,
                    .expr = dy_core_expr_new(expr),
                    .sequent = dy_core_expr_new(type)
                }
            }
        }
    };
}

enum dy_subtype_result dy_join_subtype_results(enum dy_subtype_result res1, enum dy_subtype_result res2)
{
    if (res1 == DY_SUBTYPE_RESULT_NO || res2 == DY_SUBTYPE_RESULT_NO) {
        return DY_SUBTYPE_RESULT_NO;
    }

    if (res1 == DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL) {
        if (res2 == DY_SUBTYPE_RESULT_YES_INEQUAL || res2 == DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS) {
            return DY_SUBTYPE_RESULT_NO;
        }

        return DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL;
    }

    if (res2 == DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL) {
        if (res1 == DY_SUBTYPE_RESULT_YES_INEQUAL || res1 == DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS) {
            return DY_SUBTYPE_RESULT_NO;
        }

        return DY_SUBTYPE_RESULT_YES_IF_ALL_THE_REST_IS_EQUAL;
    }

    if (res1 == DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS || res2 == DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS) {
        return DY_SUBTYPE_RESULT_YES_WITH_INFERENCE_VARS;
    }

    if (res1 == DY_SUBTYPE_RESULT_YES_INEQUAL || res2 == DY_SUBTYPE_RESULT_YES_INEQUAL) {
        return DY_SUBTYPE_RESULT_YES_INEQUAL;
    }

    return DY_SUBTYPE_RESULT_YES_EQUAL;
}

enum dy_subtype_transform_result dy_join_subtype_transform_results(enum dy_subtype_transform_result res1, enum dy_subtype_transform_result res2, bool *illegal_transform)
{
    if (res1 == DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS || res2 == DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS) {
        if (res1 == DY_SUBTYPE_TRANSFORM_RESULT_YES || res2 == DY_SUBTYPE_TRANSFORM_RESULT_YES) {
            *illegal_transform = true;
            return DY_SUBTYPE_TRANSFORM_RESULT_NO;
        }

        return DY_SUBTYPE_TRANSFORM_RESULT_NO_FURTHER_TRANSFORMS;
    } else if (res1 == DY_SUBTYPE_TRANSFORM_RESULT_YES || res2 == DY_SUBTYPE_TRANSFORM_RESULT_YES) {
        return DY_SUBTYPE_TRANSFORM_RESULT_YES;
    } else if (res1 == DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED || res2 == DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED) {
        return DY_SUBTYPE_TRANSFORM_RESULT_YES_IF_ANY_OF_THE_REST_ARE_TRANSFORMED;
    }  else {
        return DY_SUBTYPE_TRANSFORM_RESULT_NO;
    }
}

void dy_remove_size_t_from_array_starting_at(size_t *array, size_t s, size_t start_index)
{
    for (size_t *x = array; x != dy_array_past_end(array);) {
        if (*x == s) {
            dy_array_remove(array, x);
        } else {
            ++x;
        }
    }
}

size_t dy_sequence_steps_length(dy_core_ctx_t *ctx)
{
    const dy_core_sequence_step_t *end = dy_array_past_end(ctx->sequence_steps);

    return (size_t)(end - ctx->sequence_steps);
}

size_t dy_size_t_array_length(size_t *array)
{
    const size_t *end = dy_array_past_end(array);

    return (size_t)(end - array);
}

size_t dy_expr_array_length(dy_core_expr_t *exprs)
{
    const dy_core_expr_t *end = dy_array_past_end(exprs);

    return (size_t)(end - exprs);
}
