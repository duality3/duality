#pragma once

#include <stdint.h>

/**
 * Implements AML. Kill me now.
 */

struct aml_def_block_header {
    uint32_t table_signature;
    uint32_t table_length;
    uint8_t spec_compliance;
    uint8_t check_sum;
    char oem_id[6];
    char oem_table_id[6];
    uint32_t oem_revision;
    uint32_t creator_id;
    uint32_t creator_revision;
};

struct aml_def_alias {
};

enum aml_name_space_mod_obj_tag {
    AML_NAME_SPACE_MOD_OBJ_DEF_ALIAS,
    AML_NAME_SPACE_MOD_OBJ_DEF_NAME,
    AML_NAME_SPACE_MOD_OBJ_DEF_SCOPE
};

struct aml_name_space_mod_obj {
    union {
    };

    enum aml_name_space_mod_obj_tag tag;
};

enum aml_object_tag {
    AML_OBJ_NAME_SPACE_MOD_OBJ,
    AML_OBJ_NAMED_OBJ
};

struct aml_object {
    union {
    };

    enum aml_object_tag tag;
};

enum aml_term_object_tag {
    AML_TERM_OBJ_OBJ,
    AML_TERM_OBJ_TYPE_1_OPCODE,
    AML_TERM_OBJ_TYPE_2_OPCODE
};

struct aml_term_object {
    union {
    };

    enum aml_term_object_tag tag;
};

struct aml_term_list {
    struct aml_term_object object;
    struct aml_term_list *next; // Can be NULL.
};

struct aml {
    struct aml_def_block_header def_block_header;
    struct aml_term_list term_list;
};
