/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "core.h"

/**
 * This file deals with collecting/resolving constraints.
 */

typedef struct dy_constraint {
    size_t index;
    dy_core_expr_t *lower_bounds;
    dy_core_expr_t *upper_bounds;
} dy_constraint_t;

static inline
size_t dy_constraint_float_offset(const dy_constraint_t *start, const dy_constraint_t *end);

static inline
void dy_constraint_get(dy_constraint_t *constraints, dy_constraint_t *start, dy_core_expr_t **lower, dy_core_expr_t **upper);

static inline
void dy_move_constraints_up(dy_constraint_t *constraints, dy_constraint_t *start);

static inline
void dy_join_constraints(dy_constraint_t *constraints, dy_constraint_t *start1, dy_constraint_t *start2);

static inline
void dy_free_constraints_from(dy_constraint_t *constraints, dy_constraint_t *start, const dy_constraint_t *end);

static inline
bool dy_constraint_promote_list(const dy_core_expr_t *first, const dy_core_expr_t *one_past_last, dy_polarity_t polarity, dy_core_expr_t *result);

static inline
size_t dy_constraints_length(const dy_constraint_t *constraints);
