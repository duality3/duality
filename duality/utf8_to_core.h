/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "core.h"

typedef bool (*dy_utf8_byte_fn)(void *env, uint8_t *byte);

typedef void (*dy_utf8_consume_fn)(void *env);

typedef struct dy_utf8_bytes_cb {
    dy_utf8_byte_fn byte;
    dy_utf8_consume_fn consume;
    void *env;
} dy_utf8_bytes_cb_t;

static inline
bool dy_utf8_non_left_recursive_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr);

bool dy_utf8_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr);

bool dy_utf8_paren_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr);

bool dy_utf8_var_to_core_expr(dy_utf8_bytes_cb_t cb, uint8_t *const **vars_in_scope, dy_core_expr_t *expr);
