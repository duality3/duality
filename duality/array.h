/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "allocator.h"

static inline
void *dy_array_create(void *env, dy_alloc_fn alloc, size_t elem_size, size_t elem_align, size_t num_elems);

static inline
void dy_array_destroy(void *env, dy_free_fn free, void *array, size_t elem_size, size_t num_elems);

static inline
void *dy_array_add_capacity(void *env, dy_alloc_fn alloc, dy_resize_fn resize, void **array_ptr, size_t elem_size, size_t num_elems, size_t offset);

static inline
void dy_array_remove_capacity(void *env, dy_resize_fn resize, void *array, size_t elem_size, size_t offset);
