/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <stddef.h>
#include <stdbool.h>

// Returns NULL if size amount of memory isn't available.
typedef void *(*dy_alloc_fn)(void *env, size_t size, size_t align);

typedef bool (*dy_resize_fn)(void *env, void *addr, size_t old_size, size_t new_size);

// Frees the memory pointed to by size.
typedef void (*dy_free_fn)(void *env, void *addr, size_t size);

typedef struct dy_allocator {
    void *env;
    dy_alloc_fn alloc;
    dy_resize_fn resize;
    dy_free_fn free;
} dy_allocator_t;
