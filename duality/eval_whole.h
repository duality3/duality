/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "core.h"

static inline bool dy_eval_whole(dy_core_expr_t expr, dy_core_expr_t *new_expr);

static inline bool dy_eval_whole_ptr(dy_core_expr_t *expr, dy_core_expr_t **result);

static inline bool dy_eval_whole_ptr_in_place(dy_core_expr_t **expr);
