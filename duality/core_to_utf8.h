/*
 * SPDX-FileCopyrightText: 2017-2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <duality/core.h>

static inline
void dy_core_expr_to_utf8(dy_core_expr_t expr, uint8_t **utf8);

static inline
void dy_core_trap_to_utf8(uint8_t **utf8);
