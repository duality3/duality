/*
 * SPDX-FileCopyrightText: 2024 Thorben Hasenpusch <mail@tpuschel.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

/**
 * This file implements the data structure that represents Core,
 * and any associated auxiliary functions.
 */

typedef struct dy_core_expr dy_core_expr_t;

typedef struct dy_core_function {
    const void *aux_data;

    dy_core_expr_t *type; // Can be NULL.
    dy_core_expr_t *sequent;
} dy_core_function_t;

typedef struct dy_core_tuple {
    const void *aux_data;

    dy_core_expr_t *sequents;
    size_t num_sequents;
} dy_core_tuple_t;

typedef struct dy_core_recursion {
    const void *aux_data;

    dy_core_expr_t *sequent;
} dy_core_recursion_t;

typedef enum __attribute((packed)) dy_core_complex_tag {
    DY_CORE_FUNCTION,
    DY_CORE_TUPLE,
    DY_CORE_RECURSION
} dy_core_complex_tag_t;

typedef struct dy_core_complex {
    dy_core_complex_tag_t tag;

    union {
        dy_core_function_t function;
        dy_core_tuple_t tuple;
        dy_core_recursion_t recursion;
    };
} dy_core_complex_t;

typedef enum __attribute((packed)) dy_core_simple_prefix_tag {
    DY_CORE_MAPPING = DY_CORE_FUNCTION,
    DY_CORE_SELECTION = DY_CORE_TUPLE,
    DY_CORE_UNFOLDING = DY_CORE_RECURSION
} dy_core_simple_prefix_tag_t;

typedef struct dy_core_simple_prefix {
    dy_core_simple_prefix_tag_t tag;

    union {
        struct {
            dy_core_expr_t *expr;
            bool is_computationally_irrelevant;
        };
        size_t index;
    };
} dy_core_simple_prefix_t;

typedef struct dy_core_simple {
    dy_core_simple_prefix_t prefix;
    dy_core_expr_t *sequent;
} dy_core_simple_t;

typedef enum __attribute((packed)) dy_polarity {
    DY_POLARITY_NEGATIVE,
    DY_POLARITY_POSITIVE
} dy_polarity_t;

typedef enum __attribute((packed)) dy_intro_tag {
    DY_INTRO_COMPLEX,
    DY_INTRO_SIMPLE
} dy_intro_tag_t;

typedef struct dy_core_intro {
    dy_intro_tag_t tag;

    union {
        dy_core_complex_t complex;
        dy_core_simple_t simple;
    };

    dy_polarity_t polarity;
    bool is_implicit;
} dy_core_intro_t;

typedef enum __attribute((packed)) dy_check_status {
    DY_CHECK_STATUS_OK,
    DY_CHECK_STATUS_FAIL,
    DY_CHECK_STATUS_NEEDS_CHECKING
} dy_check_status_t;

typedef struct dy_core_function_eliminator {
    dy_core_expr_t *type; // Can be NULL.
    dy_core_function_t cont;

    dy_check_status_t check_result;
} dy_core_function_eliminator_t;

typedef struct dy_core_cont_with_check_status {
    dy_core_function_t cont;
    dy_check_status_t status;
} dy_core_cont_with_check_status_t;

typedef struct dy_core_tuple_eliminator {
    dy_core_cont_with_check_status_t *conts_with_check_status;
    size_t num_conts_with_check_status;
} dy_core_tuple_eliminator_t;

typedef struct dy_core_recursion_eliminator {
    size_t num_inference_ctxs;
    dy_core_function_t cont;
    dy_check_status_t check_result;
} dy_core_recursion_eliminator_t;

typedef struct dy_core_complex_eliminator {
    dy_core_complex_tag_t tag;

    union {
        dy_core_function_eliminator_t function;
        dy_core_tuple_eliminator_t tuple;
        dy_core_recursion_eliminator_t recursion;
    };
} dy_core_complex_eliminator_t;

typedef struct dy_core_elim {
    const void *aux_data;

    dy_core_expr_t *expr;

    dy_intro_tag_t tag;

    union {
        dy_core_complex_eliminator_t complex;
        dy_core_simple_prefix_t simple_prefix;
    };

    bool is_implicit;

    dy_core_expr_t *result_type;

    dy_check_status_t check_result;
} dy_core_elim_t;

typedef struct dy_core_elim_ref {
    const void *aux_data;

    dy_core_expr_t *expr;
    size_t index;

    dy_check_status_t check_result;
} dy_core_elim_ref_t;

typedef struct dy_core_inference_ctx {
    const void *aux_data;

    size_t num_dependent_ctxs;

    dy_core_expr_t *expr;
} dy_core_inference_ctx_t;

typedef struct dy_core_sequence {
    const void *aux_data;

    dy_core_expr_t *expr;
    dy_core_function_t cont;
    dy_core_expr_t *type;

    dy_check_status_t expr_result;
    dy_check_status_t type_result;
} dy_core_sequence_t;

typedef struct dy_core_trap {
    const void *aux_data;

    size_t text_loc_start_index;
} dy_core_trap_t;

typedef struct dy_core_comp {
    const void *aux_data;

    dy_core_expr_t *expr;
} dy_core_comp_t;

typedef enum __attribute((packed)) dy_core_expr_tag {
    DY_CORE_EXPR_INTRO,
    DY_CORE_EXPR_ELIM,
    DY_CORE_EXPR_REF,
    DY_CORE_EXPR_ELIM_REF,
    DY_CORE_EXPR_SEQUENCE,
    DY_CORE_EXPR_COMP,
    DY_CORE_EXPR_TRAP,
    DY_CORE_EXPR_INFERENCE_CTX
} dy_core_expr_tag_t;

typedef struct dy_core_expr {
    dy_core_expr_tag_t tag;

    union {
        dy_core_intro_t intro;
        dy_core_elim_t elim;
        size_t ref;
        dy_core_elim_ref_t elim_ref;
        dy_core_sequence_t sequence;
        dy_core_comp_t comp;
        dy_core_trap_t trap;
        dy_core_inference_ctx_t inference_ctx;
    };
} dy_core_expr_t;

typedef enum __attribute((packed)) dy_binder_tag {
    DY_BINDER_FUNCTION,
    DY_BINDER_RECURSION,
    DY_BINDER_REC_ELIM,
    DY_BINDER_INFERENCE_CTX
} dy_binder_tag_t;

typedef struct dy_binder {
    dy_binder_tag_t tag;

    dy_core_expr_t type;
    bool have_type;
} dy_binder_t;


static inline const void *dy_core_get_aux_data(const dy_core_expr_t *expr);

static inline void dy_core_set_aux_data(dy_core_expr_t *expr, const void *aux_data);

static inline dy_core_expr_t *dy_core_expr_new(dy_core_expr_t expr);


/** Appends a utf8 represention of expr to 'string'. */
static inline void dy_core_expr_to_string(dy_core_expr_t expr, uint8_t **string, size_t num_indents);

/** Helper to add a string literal to a dynamic char array. */
static inline void dy_core_add_string(uint8_t **string, const uint8_t *s, size_t len_s);

static inline void dy_core_add_size_t_decimal(uint8_t **string, size_t x);
