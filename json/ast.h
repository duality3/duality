#pragma once

#include "../array/array.h"

#include <stdbool.h>
#include <stdint.h>

typedef struct dy_json dy_json_t;

typedef enum dy_json_true {
    DY_JSON_TRUE_T,
    DY_JSON_TRUE_TR,
    DY_JSON_TRUE_TRU,
    DY_JSON_TRUE_TRUE
} dy_json_true_t;

typedef enum dy_json_false {
    DY_JSON_FALSE_F,
    DY_JSON_FALSE_FA,
    DY_JSON_FALSE_FAL,
    DY_JSON_FALSE_FALS,
    DY_JSON_FALSE_FALSE,
} dy_json_false_t;

typedef enum dy_json_null {
    DY_JSON_NULL_N,
    DY_JSON_NULL_NU,
    DY_JSON_NULL_NUL,
    DY_JSON_NULL_NULL,
} dy_json_null_t;

typedef struct dy_json_member dy_json_member_t;

typedef struct dy_json_object {
    bool is_closed;
    dy_json_member_t *members;
    dy_json_member_t *current_member; // Can be NULL.
} dy_json_object_t;

typedef enum dy_json_code_point_state {
    DY_JSON_CODE_POINT_NEED_1,
    DY_JSON_CODE_POINT_NEED_2,
    DY_JSON_CODE_POINT_NEED_3,
    DY_JSON_CODE_POINT_ESCAPED,
    DY_JSON_CODE_POINT_NEED_HEX_1,
    DY_JSON_CODE_POINT_NEED_HEX_2,
    DY_JSON_CODE_POINT_NEED_HEX_3,
    DY_JSON_CODE_POINT_NEED_HEX_4,
    DY_JSON_CODE_POINT_COMPLETE
} dy_json_code_point_state_t;

typedef struct dy_json_string {
    uint32_t *code_points;
    uint32_t current_code_point;
    dy_json_code_point_state_t current_code_point_state;
    bool have_closing_quote;
} dy_json_string_t;

typedef struct dy_json_array {
    dy_json_t *values;
    dy_json_t *current_value; // Can be NULL
    bool have_closing_brace;
} dy_json_array_t;

typedef struct dy_json_number {
    bool have_minus;
    bool have_value;
    int64_t value;
} dy_json_number_t;

typedef enum dy_json_tag {
    DY_JSON_TRUE,
    DY_JSON_FALSE,
    DY_JSON_NULL,
    DY_JSON_OBJECT,
    DY_JSON_STRING,
    DY_JSON_ARRAY,
    DY_JSON_NUMBER
} dy_json_tag_t;

typedef struct dy_json {
    union {
        dy_json_true_t true_literal;
        dy_json_false_t false_literal;
        dy_json_null_t null_literal;
        dy_json_object_t object;
        dy_json_string_t string;
        dy_json_array_t array;
        dy_json_number_t number;
    };

    dy_json_tag_t tag;
} dy_json_t;

typedef struct dy_json_member {
    dy_json_string_t string;
    bool have_colon;
    dy_json_t element;
    bool have_element;
} dy_json_member_t;

static inline bool dy_json_string_equals_utf32(dy_json_string_t string, const uint32_t *s, size_t len_s);

static inline dy_json_t *dy_json_get_member(const dy_json_object_t *obj, const uint32_t *s, size_t len_s);

static inline dy_json_object_t dy_json_make_object(const dy_json_member_t *members);

static inline dy_json_member_t dy_json_make_member(const uint32_t *field, size_t field_len, dy_json_t element);

static inline dy_json_t dy_json_make_string(const uint32_t *s, size_t len);

static inline dy_json_t dy_json_make_number(int64_t value);
