#include "utf8_to_json.h"

static inline bool dy_json_is_whitespace(uint8_t c);
static inline bool dy_json_char_to_member(uint8_t c, dy_json_member_t *member);
static inline bool dy_json_member_parse(dy_json_member_t *member, uint8_t c);
static inline bool dy_json_decode_decimal_or_hex_digit(uint8_t c, uint8_t *byte);

bool dy_json_ast_parse(dy_json_t *ast, uint8_t c)
{
    switch (ast->tag) {
    case DY_JSON_TRUE:
        return dy_json_true_parse(&ast->true_literal, c);
    case DY_JSON_FALSE:
        return dy_json_false_parse(&ast->false_literal, c);
    case DY_JSON_NULL:
        return dy_json_null_parse(&ast->null_literal, c);
    case DY_JSON_OBJECT:
        return dy_json_object_parse(&ast->object, c);
    case DY_JSON_STRING:
        return dy_json_string_parse(&ast->string, c);
    case DY_JSON_ARRAY:
        return dy_json_array_parse(&ast->array, c);
    case DY_JSON_NUMBER:
        return dy_json_number_parse(&ast->number, c);
    }
}

bool dy_json_char_to_ast(uint8_t c, dy_json_t *ast)
{
    if (dy_json_is_whitespace(c)) {
        return true;
    }

    if (c == 't') {
        *ast = (dy_json_t){
            .tag = DY_JSON_TRUE,
            .true_literal = DY_JSON_TRUE_T
        };

        return true;
    }

    if (c == 'f') {
        *ast = (dy_json_t){
            .tag = DY_JSON_FALSE,
            .false_literal = DY_JSON_FALSE_F
        };

        return true;
    }

    if (c == 'n') {
        *ast = (dy_json_t){
            .tag = DY_JSON_NULL,
            .null_literal = DY_JSON_NULL_N
        };

        return true;
    }

    if (c == '{') {
        *ast = (dy_json_t){
            .tag = DY_JSON_OBJECT,
            .object = {
                .members = dy_array_create(sizeof (dy_json_member_t), 4)
            }
        };

        return true;
    }

    if (c == '\"') {
        *ast = (dy_json_t){
            .tag = DY_JSON_STRING,
            .string = {
                .code_points = dy_array_create(sizeof (uint32_t), 4)
            }
        };

        return true;
    }

    if (c == '[') {
        *ast = (dy_json_t){
            .tag = DY_JSON_ARRAY,
            .array = {
                .values = dy_array_create(sizeof (dy_json_t), 4)
            }
        };

        return true;
    }

    if (c == '-') {
        *ast = (dy_json_t){
            .tag = DY_JSON_NUMBER,
            .number = {
                .have_minus = true
            }
        };

        return true;
    }

    if ('0' <= c && c <= '9') {
        *ast = (dy_json_t){
            .tag = DY_JSON_NUMBER,
            .number = {
                .have_value = true,
                .value = c - '0'
            }
        };

        return true;
    }

    return false;
}

bool dy_json_true_parse(dy_json_true_t *true_lit, uint8_t c)
{
    switch (*true_lit) {
    case DY_JSON_TRUE_T:
        if (c == 'r') {
            *true_lit = DY_JSON_TRUE_TR;
            return true;
        } else {
            return false;
        }
    case DY_JSON_TRUE_TR:
        if (c == 'u') {
            *true_lit = DY_JSON_TRUE_TRU;
            return true;
        } else {
            return false;
        }
    case DY_JSON_TRUE_TRU:
        if (c == 'e') {
            *true_lit = DY_JSON_TRUE_TRUE;
            return true;
        } else {
            return false;
        }
    case DY_JSON_TRUE_TRUE:
        return false;
    }
}

bool dy_json_false_parse(dy_json_false_t *false_lit, uint8_t c)
{
    switch (*false_lit) {
    case DY_JSON_FALSE_F:
        if (c == 'a') {
            *false_lit = DY_JSON_FALSE_FA;
            return true;
        } else {
            return false;
        }
    case DY_JSON_FALSE_FA:
        if (c == 'l') {
            *false_lit = DY_JSON_FALSE_FAL;
            return true;
        } else {
            return false;
        }
    case DY_JSON_FALSE_FAL:
        if (c == 's') {
            *false_lit = DY_JSON_FALSE_FALS;
            return true;
        } else {
            return false;
        }
    case DY_JSON_FALSE_FALS:
        if (c == 'e') {
            *false_lit = DY_JSON_FALSE_FALSE;
            return true;
        } else {
            return false;
        }
    case DY_JSON_FALSE_FALSE:
        return false;
    }
}

bool dy_json_null_parse(dy_json_null_t *null_lit, uint8_t c)
{
    switch (*null_lit) {
    case DY_JSON_NULL_N:
        if (c == 'u') {
            *null_lit = DY_JSON_NULL_NU;
            return true;
        } else {
            return false;
        }
    case DY_JSON_NULL_NU:
        if (c == 'l') {
            *null_lit = DY_JSON_NULL_NUL;
            return true;
        } else {
            return false;
        }
    case DY_JSON_NULL_NUL:
        if (c == 'l') {
            *null_lit = DY_JSON_NULL_NULL;
            return true;
        } else {
            return false;
        }
    case DY_JSON_NULL_NULL:
        return false;
    }
}

bool dy_json_object_parse(dy_json_object_t *obj, uint8_t c)
{
    if (!obj->is_closed) {
        if (obj->current_member == NULL) {
            dy_json_member_t member;
            if (dy_json_char_to_member(c, &member)) {
                obj->current_member = dy_array_add(&obj->members, &member);
                return true;
            }

            if (dy_json_is_whitespace(c)) {
                return true;
            }

            if (obj->members == dy_array_past_end(obj->members) && c == '}') {
                obj->is_closed = true;
                return true;
            }

            return false;
        }

        if (dy_json_member_parse(obj->current_member, c)) {
            return true;
        }

        if (c == ',') {
            obj->current_member = NULL;
            return true;
        }

        if (dy_json_is_whitespace(c)) {
            return true;
        }

        if (c == '}') {
            obj->is_closed = true;
            return true;
        }

        return false;
    }

    return false;
}

bool dy_json_string_parse(dy_json_string_t *string, uint8_t c)
{
    if (string->have_closing_quote) {
        return false;
    }

    switch (string->current_code_point_state) {
    case DY_JSON_CODE_POINT_COMPLETE:
        if (c == '\"') {
            string->have_closing_quote = true;
            return true;
        }

        if (c == '\\') {
            string->current_code_point_state = DY_JSON_CODE_POINT_ESCAPED;
            return true;
        }

        if ((c >> 7) == 0) {
            dy_array_add(&string->code_points, &c);
            return true;
        }

        if ((c >> 5) == 0b110) {
            string->current_code_point_state = DY_JSON_CODE_POINT_NEED_1;
            string->current_code_point = c & 0b11111;
            return true;
        }

        if ((c >> 4) == 0b1110) {
            string->current_code_point_state = DY_JSON_CODE_POINT_NEED_2;
            string->current_code_point = c & 0b1111;
            return true;
        }

        if ((c >> 3) == 0b11110) {
            string->current_code_point_state = DY_JSON_CODE_POINT_NEED_3;
            string->current_code_point = c & 0b111;
            return true;
        }

        return false;
    case DY_JSON_CODE_POINT_NEED_1:
        if ((c >> 6) == 0b10) {
            string->current_code_point <<= 6;
            string->current_code_point |= (c & 0b00111111);
            string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
            dy_array_add(&string->code_points, &string->current_code_point);
            string->current_code_point = 0;
            return true;
        } else {
            return false;
        }
    case DY_JSON_CODE_POINT_NEED_2:
        if ((c >> 6) == 0b10) {
            string->current_code_point <<= 6;
            string->current_code_point |= (c & 0b00111111);
            string->current_code_point_state = DY_JSON_CODE_POINT_NEED_1;
            dy_array_add(&string->code_points, &string->current_code_point);
            return true;
        } else {
            return false;
        }
    case DY_JSON_CODE_POINT_NEED_3:
        if ((c >> 6) == 0b10) {
            string->current_code_point <<= 6;
            string->current_code_point |= (c & 0b00111111);
            string->current_code_point_state = DY_JSON_CODE_POINT_NEED_2;
            dy_array_add(&string->code_points, &string->current_code_point);
            return true;
        } else {
            return false;
        }
    case DY_JSON_CODE_POINT_ESCAPED:
        if (c == '"' || c == '\\' || c == '/') {
            dy_array_add(&string->code_points, &c);
            string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
            return true;
        } else if (c == 'b') {
            dy_array_add(&string->code_points, &(uint32_t){ '\b' });
            string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
            return true;
        } else if (c == 'f') {
            dy_array_add(&string->code_points, &(uint32_t){ '\f' });
            string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
            return true;
        } else if (c == 'n') {
            dy_array_add(&string->code_points, &(uint32_t){ '\n' });
            string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
            return true;
        } else if (c == 'r') {
            dy_array_add(&string->code_points, &(uint32_t){ '\r' });
            string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
            return true;
        } else if (c == 't') {
            dy_array_add(&string->code_points, &(uint32_t){ '\t' });
            string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
            return true;
        } else if (c == 'u') {
            string->current_code_point_state = DY_JSON_CODE_POINT_NEED_HEX_1;
            return true;
        } else {
            return false;
        }
    case DY_JSON_CODE_POINT_NEED_HEX_1: {
        uint8_t byte;
        if (!dy_json_decode_decimal_or_hex_digit(c, &byte)) {
            return false;
        }

        string->current_code_point = byte;

        string->current_code_point_state = DY_JSON_CODE_POINT_NEED_HEX_2;
        return true;
    }
    case DY_JSON_CODE_POINT_NEED_HEX_2: {
        uint8_t byte;
        if (!dy_json_decode_decimal_or_hex_digit(c, &byte)) {
            return false;
        }

        string->current_code_point <<= 4;
        string->current_code_point |= byte;

        string->current_code_point_state = DY_JSON_CODE_POINT_NEED_HEX_3;
        return true;
    }
    case DY_JSON_CODE_POINT_NEED_HEX_3: {
        uint8_t byte;
        if (!dy_json_decode_decimal_or_hex_digit(c, &byte)) {
            return false;
        }

        string->current_code_point <<= 4;
        string->current_code_point |= byte;

        string->current_code_point_state = DY_JSON_CODE_POINT_NEED_HEX_4;
        return true;
    }
    case DY_JSON_CODE_POINT_NEED_HEX_4: {
        uint8_t byte;
        if (!dy_json_decode_decimal_or_hex_digit(c, &byte)) {
            return false;
        }

        string->current_code_point <<= 4;
        string->current_code_point |= byte;

        dy_array_add(&string->code_points, &string->current_code_point);
        string->current_code_point = 0;

        string->current_code_point_state = DY_JSON_CODE_POINT_COMPLETE;
        return true;
    }
    }
}

bool dy_json_array_parse(dy_json_array_t *array, uint8_t c)
{
    if (array->current_value == NULL) {
        dy_json_t value;
        if (dy_json_char_to_ast(c, &value)) {
            array->current_value = dy_array_add(&array->values, &value);
            return true;
        }

        if (dy_json_is_whitespace(c)) {
            return true;
        }

        if (c == ']') {
            array->have_closing_brace = true;
            return true;
        }

        return false;
    } else {
        if (dy_json_ast_parse(array->current_value, c)) {
            return true;
        }

        if (c == ',') {
            array->current_value = NULL;
            return true;
        }

        return false;
    }
}

bool dy_json_number_parse(dy_json_number_t *number, uint8_t c)
{
    if (number->have_value && number->value == 0) {
        return false;
    }

    if (c < '0' || '9' < c) {
        return false;
    }

    uint8_t digit_value = c - '0';

    if (!number->have_value) {
        if (number->have_minus) {
            number->value = -digit_value;
        } else {
            number->value = digit_value;
        }

        return true;
    }

    if (__builtin_mul_overflow(number->value, 10, &number->value)) {
        return false;
    }

    if (__builtin_add_overflow(number->value, digit_value, &number->value)) {
        return false;
    }

    return true;
}

bool dy_json_is_whitespace(uint8_t c)
{
    if (c == ' ') {
        return true;
    }

    if (c == '\n') {
        return true;
    }

    if (c == '\r') {
        return true;
    }

    if (c == '\t') {
        return true;
    }

    return false;
}

bool dy_json_char_to_member(uint8_t c, dy_json_member_t *member)
{
    if (c == '\"') {
        *member = (dy_json_member_t){

        };

        return true;
    }

    return false;
}

bool dy_json_member_parse(dy_json_member_t *member, uint8_t c)
{
    if (!member->string.have_closing_quote) {
        if (dy_json_string_parse(&member->string, c)) {
            return true;
        }

        if (dy_json_is_whitespace(c)) {
            return true;
        }

        return false;
    }

    if (!member->have_colon) {
        if (c == ':') {
            member->have_colon = true;
            return true;
        }

        if (dy_json_is_whitespace(c)) {
            return true;
        }

        return false;
    }

    if (!member->have_element) {
        if (dy_json_char_to_ast(c, &member->element)) {
            member->have_element = true;
            return true;
        }

        if (dy_json_is_whitespace(c)) {
            return true;
        }

        return false;
    }

    return dy_json_ast_parse(&member->element, c);
}

bool dy_json_decode_decimal_or_hex_digit(uint8_t c, uint8_t *byte)
{
    if ('0' <= c && c <= '9') {
        *byte = c - '0';
        return true;
    }

    if ('a' <= c && c <= 'f') {
        *byte = c - 'a';
        return true;
    }

    if ('A' <= c && c <= 'F') {
        *byte = c - 'A';
        return true;
    }

    return false;
}
