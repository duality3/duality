#pragma once

#include "ast.h"

typedef void (*dy_json_put_utf8_byte_fn)(uint8_t c, void *user_data);

typedef struct dy_json_to_utf8_ctx {
    dy_json_put_utf8_byte_fn put_utf8_char;
    void *user_data;
} dy_json_to_utf8_ctx_t;

void dy_json_to_utf8(dy_json_to_utf8_ctx_t *ctx, const dy_json_t *json);

void dy_json_true_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_true_t true_literal);

void dy_json_false_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_false_t false_literal);

void dy_json_null_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_null_t null_literal);

void dy_json_object_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_object_t object);

void dy_json_array_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_array_t array);

void dy_json_string_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_string_t string);

void dy_json_number_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_number_t number);
