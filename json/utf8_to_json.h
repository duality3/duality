#pragma once

#include "ast.h"

static inline bool dy_json_char_to_ast(uint8_t c, dy_json_t *ast);

static inline bool dy_json_ast_parse(dy_json_t *ast, uint8_t c);

static inline bool dy_json_true_parse(dy_json_true_t *true_lit, uint8_t c);

static inline bool dy_json_false_parse(dy_json_false_t *false_lit, uint8_t c);

static inline bool dy_json_null_parse(dy_json_null_t *null_lit, uint8_t c);

static inline bool dy_json_object_parse(dy_json_object_t *obj, uint8_t c);

static inline bool dy_json_string_parse(dy_json_string_t *string, uint8_t c);

static inline bool dy_json_array_parse(dy_json_array_t *array, uint8_t c);

static inline bool dy_json_number_parse(dy_json_number_t *number, uint8_t c);
