#include "json_to_utf8.h"

void dy_json_to_utf8(dy_json_to_utf8_ctx_t *ctx, const dy_json_t *json)
{
    switch (json->tag) {
    case DY_JSON_TRUE:
        dy_json_true_to_utf8(ctx, json->true_literal);
        return;
    case DY_JSON_FALSE:
        dy_json_false_to_utf8(ctx, json->false_literal);
        return;
    case DY_JSON_NULL:
        dy_json_null_to_utf8(ctx, json->null_literal);
        return;
    case DY_JSON_OBJECT:
        dy_json_object_to_utf8(ctx, json->object);
        return;
    case DY_JSON_ARRAY:
        dy_json_array_to_utf8(ctx, json->array);
        return;
    case DY_JSON_STRING:
        dy_json_string_to_utf8(ctx, json->string);
        return;
    case DY_JSON_NUMBER:
        dy_json_number_to_utf8(ctx, json->number);
        return;
    }
}

void dy_json_true_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_true_t true_literal)
{
    switch (true_literal) {
    case DY_JSON_TRUE_T:
        ctx->put_utf8_char('t', ctx->user_data);
        return;
    case DY_JSON_TRUE_TR:
        ctx->put_utf8_char('t', ctx->user_data);
        ctx->put_utf8_char('r', ctx->user_data);
        return;
    case DY_JSON_TRUE_TRU:
        ctx->put_utf8_char('t', ctx->user_data);
        ctx->put_utf8_char('r', ctx->user_data);
        ctx->put_utf8_char('u', ctx->user_data);
        return;
    case DY_JSON_TRUE_TRUE:
        ctx->put_utf8_char('t', ctx->user_data);
        ctx->put_utf8_char('r', ctx->user_data);
        ctx->put_utf8_char('u', ctx->user_data);
        ctx->put_utf8_char('e', ctx->user_data);
        return;
    }
}

void dy_json_false_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_false_t false_literal)
{
    switch (false_literal) {
    case DY_JSON_FALSE_F:
        ctx->put_utf8_char('f', ctx->user_data);
        return;
    case DY_JSON_FALSE_FA:
        ctx->put_utf8_char('f', ctx->user_data);
        ctx->put_utf8_char('a', ctx->user_data);
        return;
    case DY_JSON_FALSE_FAL:
        ctx->put_utf8_char('f', ctx->user_data);
        ctx->put_utf8_char('a', ctx->user_data);
        ctx->put_utf8_char('l', ctx->user_data);
        return;
    case DY_JSON_FALSE_FALS:
        ctx->put_utf8_char('f', ctx->user_data);
        ctx->put_utf8_char('a', ctx->user_data);
        ctx->put_utf8_char('l', ctx->user_data);
        ctx->put_utf8_char('s', ctx->user_data);
        return;
    case DY_JSON_FALSE_FALSE:
        ctx->put_utf8_char('f', ctx->user_data);
        ctx->put_utf8_char('a', ctx->user_data);
        ctx->put_utf8_char('l', ctx->user_data);
        ctx->put_utf8_char('s', ctx->user_data);
        ctx->put_utf8_char('e', ctx->user_data);
        return;
    }
}

void dy_json_null_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_null_t null_literal)
{
    switch (null_literal) {
    case DY_JSON_NULL_N:
        ctx->put_utf8_char('n', ctx->user_data);
        return;
    case DY_JSON_NULL_NU:
        ctx->put_utf8_char('n', ctx->user_data);
        ctx->put_utf8_char('u', ctx->user_data);
        return;
    case DY_JSON_NULL_NUL:
        ctx->put_utf8_char('n', ctx->user_data);
        ctx->put_utf8_char('u', ctx->user_data);
        ctx->put_utf8_char('l', ctx->user_data);
        return;
    case DY_JSON_NULL_NULL:
        ctx->put_utf8_char('n', ctx->user_data);
        ctx->put_utf8_char('u', ctx->user_data);
        ctx->put_utf8_char('l', ctx->user_data);
        ctx->put_utf8_char('l', ctx->user_data);
        return;
    }
}

void dy_json_object_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_object_t object)
{
    ctx->put_utf8_char('{', ctx->user_data);

    if (object.members == dy_array_past_end(object.members)) {
        if (object.is_closed) {
            ctx->put_utf8_char('}', ctx->user_data);
        }
        return;
    }

    dy_json_string_to_utf8(ctx, object.members->string);
    if (object.members->have_colon) {
        ctx->put_utf8_char(':', ctx->user_data);
    }
    if (object.members->have_element) {
        dy_json_to_utf8(ctx, &object.members->element);
    }

    for (const dy_json_member_t *member = object.members + 1; member != dy_array_past_end(object.members); ++member) {
        ctx->put_utf8_char(',', ctx->user_data);

        dy_json_string_to_utf8(ctx, member->string);
        if (member->have_colon) {
            ctx->put_utf8_char(':', ctx->user_data);
        }
        if (member->have_element) {
            dy_json_to_utf8(ctx, &member->element);
        }
    }

    if (object.is_closed) {
        ctx->put_utf8_char('}', ctx->user_data);
    }
}

void dy_json_array_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_array_t array)
{
    ctx->put_utf8_char('[', ctx->user_data);

    if (array.values == dy_array_past_end(array.values)) {
        if (array.have_closing_brace) {
            ctx->put_utf8_char('}', ctx->user_data);
        }

        return;
    }

    dy_json_to_utf8(ctx, array.values);

    for (const dy_json_t *value = array.values + 1; value != dy_array_past_end(array.values); ++value) {
        ctx->put_utf8_char(',', ctx->user_data);
        dy_json_to_utf8(ctx, value);
    }

    if (array.have_closing_brace) {
        ctx->put_utf8_char('}', ctx->user_data);
    }
}

void dy_json_string_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_string_t string)
{
    ctx->put_utf8_char('"', ctx->user_data);

    const uint32_t *end = dy_array_past_end(string.code_points);

    for (const uint32_t *code_point_ptr = string.code_points; code_point_ptr != end; ++ code_point_ptr) {
        uint32_t code_point = *code_point_ptr;

        if (code_point == '"') {
            ctx->put_utf8_char('\\', ctx->user_data);
            ctx->put_utf8_char('"', ctx->user_data);
            continue;
        }

        if (code_point == '\\') {
            ctx->put_utf8_char('\\', ctx->user_data);
            ctx->put_utf8_char('\\', ctx->user_data);
            continue;
        }

        if (code_point == '\b') {
            ctx->put_utf8_char('\\', ctx->user_data);
            ctx->put_utf8_char('b', ctx->user_data);
            continue;
        }

        if (code_point == '\f') {
            ctx->put_utf8_char('\\', ctx->user_data);
            ctx->put_utf8_char('f', ctx->user_data);
            continue;
        }

        if (code_point == '\n') {
            ctx->put_utf8_char('\\', ctx->user_data);
            ctx->put_utf8_char('n', ctx->user_data);
            continue;
        }

        if (code_point == '\r') {
            ctx->put_utf8_char('\\', ctx->user_data);
            ctx->put_utf8_char('r', ctx->user_data);
            continue;
        }

        if (code_point == '\t') {
            ctx->put_utf8_char('\\', ctx->user_data);
            ctx->put_utf8_char('t', ctx->user_data);
            continue;
        }

        if (code_point < 0x20) {
            ctx->put_utf8_char('u', ctx->user_data);
            ctx->put_utf8_char('0', ctx->user_data);
            ctx->put_utf8_char('0', ctx->user_data);
            ctx->put_utf8_char((code_point & 0xF0) >> 4, ctx->user_data);
            ctx->put_utf8_char(code_point & 0xF, ctx->user_data);
            continue;
        }

        if ((code_point & ~0x7Fu) == 0) {
            ctx->put_utf8_char(code_point & 0x7F, ctx->user_data);
            continue;
        }

        if ((code_point & ~0x7FFu) == 0) {
            ctx->put_utf8_char(0xC0 | ((code_point >> 6) & 0x1F), ctx->user_data);
            ctx->put_utf8_char(0x80 | (code_point & 0x3F), ctx->user_data);
            continue;
        }

        if ((code_point & ~0xFFFFu) == 0) {
            ctx->put_utf8_char(0xE0 | ((code_point >> 12) & 0xF), ctx->user_data);
            ctx->put_utf8_char(0x80 | ((code_point >> 6) & 0x3F), ctx->user_data);
            ctx->put_utf8_char(0x80 | (code_point & 0x3F), ctx->user_data);
            continue;
        }

        if ((code_point & ~0x10FFFFu) == 0) {
            ctx->put_utf8_char(0xF0 | ((code_point >> 18) & 0x7), ctx->user_data);
            ctx->put_utf8_char(0x80 | ((code_point >> 12) & 0x3F), ctx->user_data);
            ctx->put_utf8_char(0x80 | ((code_point >> 6) & 0x3F), ctx->user_data);
            ctx->put_utf8_char(0x80 | (code_point & 0x3F), ctx->user_data);
            continue;
        }
    }

    if (string.have_closing_quote) {
        ctx->put_utf8_char('"', ctx->user_data);
    }
}

void dy_json_number_to_utf8(dy_json_to_utf8_ctx_t *ctx, dy_json_number_t number)
{
    if (number.have_minus) {
        ctx->put_utf8_char('-', ctx->user_data);
    }

    if (!number.have_value) {
        return;
    }

    if (number.value == 0) {
        ctx->put_utf8_char('0', ctx->user_data);
        return;
    }

    uint64_t value = number.value < 0 ? (uint64_t)-number.value : (uint64_t)number.value;

    while (value != 0) {
        ctx->put_utf8_char('0' + (value % 10), ctx->user_data);
        value /= 10;
    }
}
