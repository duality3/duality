#include "ast.h"

bool dy_json_string_equals_utf32(dy_json_string_t string, const uint32_t *s, size_t len_s)
{
    if (!string.have_closing_quote) {
        return false;
    }

    const uint32_t *end = dy_array_past_end(string.code_points);
    size_t num_code_points = (size_t)(end - string.code_points);

    if (num_code_points != len_s) {
        return false;
    }

    for (size_t i = 0; i < len_s; ++i) {
        if (string.code_points[i] != s[i]) {
            return false;
        }
    }

    return true;
}

dy_json_t *dy_json_get_member(const dy_json_object_t *obj, const uint32_t *s, size_t len_s)
{
    const dy_json_member_t *end = dy_array_past_end(obj->members);
    for (dy_json_member_t *member = obj->members; member != end; ++member) {
        if (!member->have_element) {
            continue;
        }

        if (dy_json_string_equals_utf32(member->string, s, len_s)) {
            return &member->element;
        }
    }

    return NULL;
}

dy_json_object_t dy_json_make_object(const dy_json_member_t *members)
{

}

dy_json_member_t dy_json_make_member(const uint32_t *field, size_t field_len, dy_json_t element)
{

}

dy_json_t dy_json_make_string(const uint32_t *s, size_t len)
{

}

dy_json_t dy_json_make_number(int64_t value)
{

}
